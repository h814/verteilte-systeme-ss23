export const SITE = {
  title: "Distributed systems - Documentation",
  description: "Documentation and stuff for the distributed systems lecture",
  defaultLanguage: "en-us",
} as const;

export const OPEN_GRAPH = {
  image: {
    src: "",
    alt: "",
  },
  twitter: "",
};

export const KNOWN_LANGUAGES = {
  English: "en",
} as const;
export const KNOWN_LANGUAGE_CODES = Object.values(KNOWN_LANGUAGES);

// See "Algolia" section of the README for more information.
export const ALGOLIA = {
  indexName: "XXXXXXXXXX",
  appId: "XXXXXXXXXX",
  apiKey: "XXXXXXXXXX",
};

export type Sidebar = Record<
  (typeof KNOWN_LANGUAGE_CODES)[number],
  Record<string, { text: string; link: string }[]>
>;

export const BASE = import.meta.env.BASE_URL;
export const BASE_NO_SLASH = BASE.substring(1);

export const SIDEBAR: Sidebar = {
  en: {
    "Getting started": [
      {
        text: "Introduction",
        link: BASE_NO_SLASH + "en/getting-started/introduction",
      },
      { text: "Readme", link: BASE_NO_SLASH + "en/getting-started/readme" },
      {
        text: "Architecture",
        link: BASE_NO_SLASH + "en/getting-started/architecture",
      },
    ],
    Protocols: [
      { text: "P1", link: BASE_NO_SLASH + "en/protocols/p1" },
      { text: "P2", link: BASE_NO_SLASH + "en/protocols/p2" },
      { text: "P3", link: BASE_NO_SLASH + "en/protocols/p3" },
      { text: "P4", link: BASE_NO_SLASH + "en/protocols/p4" },
    ],
    API: [
      { text: "Introduction", link: BASE_NO_SLASH + "en/api/introduction" },
      { text: "Bank service", link: BASE_NO_SLASH + "en/api/bank" },
      { text: "Exchange service", link: BASE_NO_SLASH + "en/api/exchange" },
    ],
  },
};
