/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
    "./src/app/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
        "vs-gradient": "linear-gradient(to top, #f12711, #f5af19)",
      },
      fontFamily: {
        "jm-roboto": "var(--font-roboto)",
        "jm-montserrat": "var(--font-montserrat)",
      },
    },
  },
  plugins: [],
};
