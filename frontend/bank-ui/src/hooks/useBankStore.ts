"use client";

import { ConsulService } from "@bank-ui/types/consul";
import { useState } from "react";

export const useBankStore = (banks: Array<ConsulService>) => {
  const [bankStore, setBankStore] = useState<Array<ConsulService>>(banks);

  return { bankStore, setBankStore };
};
