"use client";

import { BankContext } from "@bank-ui/BankContext";
import { useContext } from "react";

export const useBankContext = () => {
  const data = useContext(BankContext);

  if (data === undefined) {
    throw new Error("Context used outside of provider");
  }

  return data;
};
