"use client";

import { useBankContext } from "@bank-ui/hooks/useBankContext";
import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";

export const BankChooser = () => {
  const [activeBank, setActiveBank] = useState<string>("-1");
  const router = useRouter();
  const banks = useBankContext();

  useEffect(() => {
    if (activeBank !== "-1") {
      router.push(`/bank/${activeBank}`);
    }
  }, [activeBank]);

  return (
    <select
      className="text-xl"
      onChange={(e) => setActiveBank(e.target.value)}
      value={activeBank}
    >
      <option value="-1">Choose a bank to operate on</option>
      {banks.map((bank) => (
        <option key={bank.ID} value={bank.Service}>
          {bank.Service}
        </option>
      ))}
    </select>
  );
};
