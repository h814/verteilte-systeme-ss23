import { ConsulService } from "@bank-ui/types/consul";
import { BankContext } from "@bank-ui/BankContext";
import { useBankStore } from "@bank-ui/hooks/useBankStore";

export const BankProvider = ({
  children,
  banks,
}: {
  banks: Array<ConsulService>;
  children: React.ReactNode;
}) => {
  const { bankStore } = useBankStore(banks);

  return (
    <BankContext.Provider value={bankStore}>{children}</BankContext.Provider>
  );
};
