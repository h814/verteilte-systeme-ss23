import { createContext } from "react";
import { ConsulService } from "./types/consul";

export const BankContext = createContext<Array<ConsulService>>([]);
