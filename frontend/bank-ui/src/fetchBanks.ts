import { ConsulService, ConsulServiceRequest } from "./types/consul";

export const fetchBanks = async (
  consulAddress: string
): Promise<Array<ConsulService>> => {
  try {
    const res = await fetch(
      `http://${consulAddress}/v1/agent/services?${new URLSearchParams({
        filter: '"bank" in Tags',
      })}`,
      { cache: "no-cache" }
    );

    if (!res.ok) {
      console.error(res);
      throw new Error("Failed to fetch consul services. Fetch error");
    }

    const j: ConsulServiceRequest = await res.json();
    return Object.values(j);
  } catch (error) {
    console.error(error);
    throw error;
  }
};
