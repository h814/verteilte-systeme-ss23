export interface ConsulServiceRequest {
  [key: string]: ConsulService;
}

export interface ConsulService {
  ID: string;
  Service: string;
  Tags: Array<string>;
  Meta: {};
  Port: number;
  Address: string;
  TaggedAddresses: {
    lan_ipv4: {
      Port: number;
      Address: string;
    };
    wan_ipv4: {
      Port: number;
      Address: string;
    };
  };
  Weights: {
    Passing: number;
    Warning: number;
  };
  EnableTagOverride: boolean;
  Datacenter: string;
}

export type ConsulServiceList = Array<ConsulService>;
