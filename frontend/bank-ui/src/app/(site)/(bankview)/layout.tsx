import Link from "next/link";

const BankViewGroupLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div>
      <Link href={"/"}>Back</Link>
      {children}
    </div>
  );
};

export default BankViewGroupLayout;
