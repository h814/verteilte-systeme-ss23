"use client";

import { useBankContext } from "@bank-ui/hooks/useBankContext";
import { useEffect } from "react";

export default function Bank({ params }: { params: { name: string } }) {
  const banks = useBankContext();

  useEffect(() => {
    console.log(banks);
    console.log(banks.find((el) => el.Service == params.name));
  }, [banks]);

  return (
    <main>
      <h1>Bank: {params.name}</h1>
      <p>Address: {banks.join(",")}</p>
    </main>
  );
}
