const HomeGroupLayout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className="bg-vs-gradient h-screen w-full overflow-hidden">
      {children}
    </div>
  );
};

export default HomeGroupLayout;
