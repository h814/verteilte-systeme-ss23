import { Suspense } from "react";
import BankLoader from "../BankLoader";

export default async function Home() {
  return (
    <main className="flex flex-col justify-center items-center h-full">
      <Suspense fallback={<div>Loading banks...</div>}>
        <h1 className="text-3xl font-bold mb-10">
          Choose a bank on which the operations should be performed on
        </h1>
        {/* @ts-expect-error Async Server Component */}
        <BankLoader consulAddress="localhost:8500" />
      </Suspense>
    </main>
  );
}
