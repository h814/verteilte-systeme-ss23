import { BankChooser } from "@bank-ui/components/BankChooser";
import { fetchBanks } from "@bank-ui/fetchBanks";

type Props = {
  consulAddress: string;
};

export default async function BankLoader() {
  return <BankChooser />;
}
