"use client";

import { BankProvider } from "@bank-ui/components/BankProvider";
import { fetchBanks } from "@bank-ui/fetchBanks";

const SiteGroupLayout = async ({ children }: { children: React.ReactNode }) => {
  const data = await fetchBanks("localhost:8500");

  return <BankProvider banks={data}>{children}</BankProvider>;
};

export default SiteGroupLayout;
