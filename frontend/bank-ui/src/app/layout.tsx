import { Montserrat, Roboto } from "next/font/google";
import "./globals.sass";

export const metadata = {
  title: "VS - Bank management tool",
  description: "Built by Joel Mühlena & Lars Rüter",
};

const roboto = Roboto({
  weight: ["300", "400", "700"],
  subsets: ["latin"],
  fallback: ["sans-serif"],
  variable: "--font-roboto",
});

const montserrat = Montserrat({
  weight: ["300", "400", "700"],
  subsets: ["latin"],
  fallback: ["Roboto", "sans-serif"],
  variable: "--font-montserrat",
});

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <link
          rel="stylesheet"
          href="https://maxst.icons8.com/vue-static/landings/line-awesome/line-awesome/1.3.0/css/line-awesome.min.css"
        />
      </head>
      <body
        className={`${roboto.className} ${montserrat.variable} ${roboto.variable}`}
      >
        <div className="app">{children}</div>
      </body>
    </html>
  );
}
