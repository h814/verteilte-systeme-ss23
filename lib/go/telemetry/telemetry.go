package telemetry

import (
	"context"
	"fmt"
	"sync"
	"time"

	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"

	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace"
	"go.opentelemetry.io/otel/exporters/otlp/otlptrace/otlptracegrpc"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.12.0"
	"go.opentelemetry.io/otel/trace"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
)

// TODO try re-init if fails and enabled via go routine

type Telemetry struct {
	logger   *zap.Logger
	config   Config
	Shutdown func(context.Context) error
	Provider *sdktrace.TracerProvider
}

var telLock sync.RWMutex

func New(cfg Config, logger *zap.Logger) *Telemetry {
	logger = logger.Named("telemetry")
	tel := new(Telemetry)

	logger = logger.Named("otel")

	tel.logger = logger
	tel.config = cfg
	tel.Shutdown = func(ctx context.Context) error { return nil }

	return tel
}

func NewTestProvider(logger *zap.Logger) *Telemetry {
	tel := new(Telemetry)
	tel.logger = logger

	traceExporter, _ := tel.setupEmptyCollector(context.Background())
	res, _ := resource.New(context.Background(),
		resource.WithAttributes(
			attribute.Bool("test", true),
		),
	)
	tel.setupProvider(traceExporter, res)

	return tel
}

// Initializes an OTLP exporter, and configures the corresponding trace and
// metric providers.
func (tel *Telemetry) initProvider(timeout time.Duration) error {
	ctx, cancel := context.WithTimeout(context.Background(), timeout) // TODO
	defer cancel()

	res, err := resource.New(ctx,
		resource.WithAttributes(
			// the service name used to display traces in backends
			semconv.ServiceNameKey.String(tel.config.Name),
			semconv.ServiceInstanceIDKey.String(tel.config.ID),
			semconv.ServiceNamespaceKey.String(tel.config.Namespace),
			semconv.ServiceVersionKey.String(tel.config.Version),
		),
		resource.WithContainer(),
		resource.WithFromEnv(),
		resource.WithHost(),
		resource.WithOS(),
		resource.WithProcess(),
		resource.WithTelemetrySDK(),
	)
	if err != nil {
		return fmt.Errorf("failed to create resource: %w", err)
	}

	traceExporter, err := tel.setupOTELCollectorGRPC(ctx)
	if err != nil {
		if tel.config.BackupCollectorConfig.Enabled {
			tel.logger.Error("Failed to init collector connection", zap.Error(err))

			traceExporter, err = tel.setupBackupCollector(ctx)

			if err != nil {
				return fmt.Errorf("failed to setup backup collector: %w", err)
			}
		} else {
			return fmt.Errorf("failed to setup collector connection: %w", err)
		}
	}

	tel.setupProvider(traceExporter, res)

	return nil
}

func (tel *Telemetry) setupProvider(traceExporter *otlptrace.Exporter, res *resource.Resource) {
	// Register the trace exporter with a TracerProvider, using a batch
	// span processor to aggregate spans before export.
	bsp := sdktrace.NewBatchSpanProcessor(traceExporter)
	tracerProvider := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithResource(res),
		sdktrace.WithSpanProcessor(bsp),
	)
	otel.SetTracerProvider(tracerProvider)

	// set global propagator to tracecontext (the default is no-op).
	otel.SetTextMapPropagator(propagation.TraceContext{})

	tel.Provider = tracerProvider
	// Shutdown will flush any remaining spans and shut down the exporter.
	tel.Shutdown = tracerProvider.Shutdown
}

func (tel *Telemetry) setupOTELCollectorGRPC(ctx context.Context) (*otlptrace.Exporter, error) {
	tel.logger.Debug("OTEL Collector gRPC connection", zap.String("url", fmt.Sprintf("%s:%d", tel.config.CollectorConfig.URL, tel.config.CollectorConfig.Port)))
	conn, err := grpc.DialContext(ctx, fmt.Sprintf("%s:%d", tel.config.CollectorConfig.URL, tel.config.CollectorConfig.Port), grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, fmt.Errorf("failed to create gRPC connection to collector: %w", err)
	}

	// Set up a trace exporter
	traceExporter, err := otlptracegrpc.New(ctx, otlptracegrpc.WithGRPCConn(conn))
	if err != nil {
		return nil, fmt.Errorf("failed to create trace exporter: %w", err)
	}

	return traceExporter, nil
}

func (tel *Telemetry) setupBackupCollector(ctx context.Context) (*otlptrace.Exporter, error) {
	switch tel.config.BackupCollectorConfig.Type {
	case "file":
		tel.setupFileCollector(ctx)
	case "console":
		return tel.setupConsoleCollector(ctx)
	case "empty":
		return tel.setupEmptyCollector(ctx)
	}

	return nil, fmt.Errorf("unsupported type: %s", tel.config.BackupCollectorConfig.Type)
}

func (tel *Telemetry) setupFileCollector(ctx context.Context) (*otlptrace.Exporter, error) {
	col := FileExporter{
		logger: tel.logger,
	}
	col.SetFile(tel.config.BackupCollectorConfig.FilePath, true)

	return otlptrace.New(ctx, &col)
}

func (tel *Telemetry) setupConsoleCollector(ctx context.Context) (*otlptrace.Exporter, error) {
	col := ConsoleExporter{
		logger: tel.logger,
	}

	return otlptrace.New(ctx, &col)
}

func (tel *Telemetry) setupEmptyCollector(ctx context.Context) (*otlptrace.Exporter, error) {
	col := EmptyExporter{
		logger: tel.logger,
	}

	return otlptrace.New(ctx, &col)
}

func (tel *Telemetry) Init(timeout time.Duration) error {
	if !tel.config.Enabled {
		tel.logger.Info("Telemetry is not enabled")
		return nil
	}

	tel.logger.Info("Initing telemetry")

	err := tel.validate()
	if err != nil {
		tel.logger.Error("Failed to init telemetry", zap.Error(err))
		return err
	}

	err = tel.initProvider(timeout)
	if err != nil {
		tel.logger.Error("Failed to init telemetry", zap.Error(err))
	} else {
		tel.logger.Info("Telemetry init completed")
	}

	return err
}

func (tel *Telemetry) Tracer() trace.Tracer {
	return otel.Tracer(tel.config.Name)
}

func (tel *Telemetry) Enabled() bool {
	return tel.config.Enabled
}

func (tel *Telemetry) validate() error {
	cfg := tel.config

	if cfg.Name == "" {
		return fmt.Errorf("invalid name set: a name is required")
	}

	if cfg.Name == "" {
		return fmt.Errorf("invalid ID set: an id is required")
	}

	if cfg.BackupCollectorConfig.Enabled {
		if !util.Contains(GetAllowedBackupTypes(), cfg.BackupCollectorConfig.Type) {
			return fmt.Errorf("this backup collector type is not allowed: %s", cfg.BackupCollectorConfig.Type)
		}
	}

	return nil
}

func GetAllowedBackupTypes() []string {
	return []string{
		"console",
		"file",
		"empty",
	}
}

func GetGRPCClientInterceptors() (grpc.UnaryClientInterceptor, grpc.StreamClientInterceptor) {
	return otelgrpc.UnaryClientInterceptor(), otelgrpc.StreamClientInterceptor()
}

func GetGRPCServerInterceptors() (grpc.UnaryServerInterceptor, grpc.StreamServerInterceptor) {
	return otelgrpc.UnaryServerInterceptor(), otelgrpc.StreamServerInterceptor()
}
