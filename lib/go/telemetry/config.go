package telemetry

type Config struct {
	Enabled               bool                  `yaml:"enabled" toml:"enabled"`
	Name                  string                `yaml:"name" toml:"name"`
	ID                    string                `yaml:"id" toml:"id"`
	Namespace             string                `yaml:"namespace" toml:"namespace"`
	Version               string                `yaml:"version" toml:"version"`
	CollectorConfig       CollectorConfig       `yaml:"collector" toml:"collector"`
	BackupCollectorConfig BackupCollectorConfig `yaml:"backupCollector" toml:"backup_collector"`
}

type CollectorConfig struct {
	URL  string `yaml:"url" toml:"url"`
	Port uint16 `yaml:"port" toml:"port"`
}

type BackupCollectorConfig struct {
	Enabled  bool   `yaml:"enabled" toml:"enabled"`
	Type     string `yaml:"type" toml:"type"`
	FilePath string `yaml:"filePath" toml:"file_path"`
}
