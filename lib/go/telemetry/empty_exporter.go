package telemetry

import (
	"context"
	"fmt"

	tracepb "go.opentelemetry.io/proto/otlp/trace/v1"
	"go.uber.org/zap"
)

type EmptyExporter struct {
	logger    *zap.Logger
	isStarted bool
}

func (ee *EmptyExporter) Start(ctx context.Context) error {
	ee.logger.Info("Starting EmptyExporter")
	ee.isStarted = true
	return nil
}

func (ee *EmptyExporter) Stop(ctx context.Context) error {
	ee.logger.Info("Stopping EmptyExporter")
	ee.isStarted = false
	return nil
}

func (ee *EmptyExporter) UploadTraces(ctx context.Context, protoSpans []*tracepb.ResourceSpans) error {
	if !ee.isStarted {
		return fmt.Errorf("exporter not started")
	}

	return nil
}
