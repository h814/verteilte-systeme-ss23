package telemetry

import (
	"context"
	"fmt"
	"os"
	"sync"

	tracepb "go.opentelemetry.io/proto/otlp/trace/v1"
	"go.uber.org/zap"
)

type FileExporter struct {
	logger    *zap.Logger
	isStarted bool
	file      *os.File

	writeLock sync.Mutex
}

func (fe *FileExporter) SetFile(path string, create bool) error {
	file, err := os.Open(path)

	if os.IsNotExist(err) && create {
		file, err = os.Create(path)

		if err != nil {
			return fmt.Errorf("failed to create file: %w", err)
		}

		err = file.Chmod(os.ModePerm)

		if err != nil {
			return fmt.Errorf("failed to change file permissions: %w", err)
		}
	} else if err != nil {
		return fmt.Errorf("failed to open file: %w", err)
	}

	fe.file = file
	return nil
}

func (fe *FileExporter) Start(ctx context.Context) error {
	if fe.file == nil {
		return fmt.Errorf("no file set")
	}

	fe.logger.Info("Starting FileExporter")

	fe.isStarted = true
	return nil
}

func (fe *FileExporter) Stop(ctx context.Context) error {
	fe.logger.Info("Stopping FileExporter")
	fe.isStarted = false
	return nil
}

func (fe *FileExporter) UploadTraces(ctx context.Context, protoSpans []*tracepb.ResourceSpans) error {
	if !fe.isStarted {
		return fmt.Errorf("exporter not started")
	}

	fe.writeLock.Lock()
	defer fe.writeLock.Unlock()
	for _, span := range protoSpans {
		fe.file.WriteString(fmt.Sprintf("TRACE: %s", span.String()))
	}

	return nil
}
