package telemetry

import (
	"context"
	"fmt"
	"os"

	tracepb "go.opentelemetry.io/proto/otlp/trace/v1"
	"go.uber.org/zap"
)

type ConsoleExporter struct {
	logger    *zap.Logger
	isStarted bool
	xp        FileExporter
}

func (ce *ConsoleExporter) Start(ctx context.Context) error {
	ce.logger.Info("Starting ConsoleExporter")
	ce.isStarted = true

	ce.xp = FileExporter{
		file:   os.Stdout,
		logger: ce.logger,
	}

	return ce.xp.Start(ctx)
}

func (ce *ConsoleExporter) Stop(ctx context.Context) error {
	ce.logger.Info("Stopping ConsoleExporter")
	ce.isStarted = false
	return ce.xp.Stop(ctx)
}

func (ce *ConsoleExporter) UploadTraces(ctx context.Context, protoSpans []*tracepb.ResourceSpans) error {
	if !ce.isStarted {
		return fmt.Errorf("exporter not started")
	}

	return ce.xp.UploadTraces(ctx, protoSpans)
}
