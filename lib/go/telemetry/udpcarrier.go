package telemetry

import "github.com/vmihailenco/msgpack/v5"

type udpCarrier struct {
	internalMap map[string]string
}

func NewUDPCarrier() *udpCarrier {
	return &udpCarrier{
		internalMap: make(map[string]string, 0),
	}
}

func (udpC *udpCarrier) Pack() ([]byte, error) {
	return msgpack.Marshal(udpC.internalMap)
}

func (udpC *udpCarrier) Unpack(b []byte) error {
	return msgpack.Unmarshal(b, &udpC.internalMap)
}

// Get returns the value associated with the passed key.
func (udpC *udpCarrier) Get(key string) string {
	return udpC.internalMap[key]
}

// Set stores the key-value pair.
func (udpC *udpCarrier) Set(key string, value string) {
	udpC.internalMap[key] = value
}

// Keys lists the keys stored in this carrier.
func (udpC *udpCarrier) Keys() []string {
	keys := make([]string, 0, len(udpC.internalMap))
	for k := range udpC.internalMap {
		keys = append(keys, k)
	}
	return keys
}
