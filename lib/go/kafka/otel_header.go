package kafka

import (
	"context"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"github.com/samber/lo"
	"github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/protocol"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
)

func PackOTELHeader(ctx context.Context) (protocol.Header, error) {
	propagator := otel.GetTextMapPropagator()
	kafkaCarrier := telemetry.NewUDPCarrier()
	propagator.Inject(ctx, kafkaCarrier)

	kafkaCarrierPacked, err := kafkaCarrier.Pack()
	if err != nil {
		return protocol.Header{}, err
	}

	return protocol.Header{Key: "otel", Value: kafkaCarrierPacked}, nil
}

func UnpackOTELHeader(ctx context.Context, msg kafka.Message, tracer trace.Tracer, spanName string) (context.Context, trace.Span) {
	val := GetHeader(msg, "X-Content-Type")

	propagator := otel.GetTextMapPropagator()
	ctx, span := tracer.Start(ctx, spanName)
	otelHeader, ok := lo.Find(msg.Headers, func(item kafka.Header) bool {
		return item.Key == "otel"
	})

	if ok {
		kafkaCarrier := telemetry.NewUDPCarrier()
		err := kafkaCarrier.Unpack(otelHeader.Value)
		if err != nil {
			return ctx, span
		}
		ctx = propagator.Extract(context.Background(), kafkaCarrier)
		ctx, span = tracer.Start(ctx, spanName)

		if len(val) > 0 {
			span.SetAttributes(attribute.String("content-type", string(val)))
		}
	}

	return ctx, span
}
