package kafka

import (
	"net"
	"strconv"
	"strings"

	"github.com/samber/lo"
	kafka "github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/protocol"
	"go.uber.org/zap"
)

type Kafka struct {
	logger  *zap.Logger
	brokers []string
}

func New(logger *zap.Logger, cfg Config) *Kafka {
	kafkaClient := &Kafka{
		logger:  logger.Named("kafka"),
		brokers: strings.Split(cfg.Brokers, ","),
	}

	return kafkaClient
}

func (k *Kafka) NewReader(topic string, consumerID string) *kafka.Reader {
	reader := kafka.NewReader(kafka.ReaderConfig{
		Brokers:  k.brokers,
		GroupID:  consumerID,
		Topic:    topic,
		MaxBytes: 10e6, // 10MB
	})

	return reader
}

func (k *Kafka) NewWriter(topic string) *kafka.Writer {
	writer := &kafka.Writer{
		Addr:                   kafka.TCP(k.brokers...),
		Topic:                  topic,
		Balancer:               &kafka.Hash{},
		AllowAutoTopicCreation: true,
	}

	return writer
}

func (k *Kafka) ConnectToLeaderFromAny() (*kafka.Conn, error) {
	conn, err := kafka.Dial("tcp", k.brokers[0])
	if err != nil {
		return nil, err
	}
	defer conn.Close()

	controller, err := conn.Controller()
	if err != nil {
		return nil, err
	}

	var controllerConn *kafka.Conn
	controllerConn, err = kafka.Dial("tcp", net.JoinHostPort(controller.Host, strconv.Itoa(controller.Port)))
	if err != nil {
		return nil, err
	}

	return controllerConn, nil
}

func (k *Kafka) CreateTopic(cfg ...kafka.TopicConfig) error {
	controllerConn, err := k.ConnectToLeaderFromAny()
	if err != nil {
		return err
	}

	defer controllerConn.Close()

	err = controllerConn.CreateTopics(cfg...)
	if err != nil {
		return err
	}

	return nil
}

func GetHeader(msg kafka.Message, key string) []byte {
	hd := lo.FindOrElse(msg.Headers, protocol.Header{Value: make([]byte, 0)}, func(item protocol.Header) bool {
		return item.Key == key
	})

	return hd.Value
}
