package util

import (
	"sync"

	"github.com/jaevor/go-nanoid"
)

var (
	nano_id_generator      func() string
	nano_id_generator_init sync.Once
)

func GenerateNanoID() string {
	nano_id_generator_init.Do(func() {
		nano_id_generator, _ = nanoid.Standard(21)
	})

	return nano_id_generator()
}
