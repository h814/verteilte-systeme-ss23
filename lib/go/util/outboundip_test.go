package util_test

import (
	"errors"
	"net"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
)

type mockDialer struct{}

func (m mockDialer) Dial(network, address string) (net.Conn, error) {
	return nil, errors.New("mock dial error")
}

func TestGetOutboundIP(t *testing.T) {
	ifaces, err := net.Interfaces()
	assert.Nil(t, err)

	t.Run("get ip", func(t *testing.T) {
		result, err := util.GetOutboundIP()
		assert.Nil(t, err)

		ips := make([]string, 0)

		for _, iface := range ifaces {
			addrs, err := iface.Addrs()
			assert.Nil(t, err)

			for _, addr := range addrs {
				assert.Nil(t, err)
				ips = append(ips, strings.Split(addr.String(), "/")[0])
			}
		}

		assert.Contains(t, ips, result.String())
	})
}
