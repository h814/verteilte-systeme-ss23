package util

import (
	"fmt"
	"sync"
	"sync/atomic"
)

var ErrKeyExistsNoOverride = fmt.Errorf("failed to add value, because this key already exists and overriding is not enabled")

type ConcurrentMap[Key comparable, Value any] struct {
	lock        sync.RWMutex
	internalMap map[Key]Value
	isOverride  atomic.Bool
}

func NewConcurrentMap[Key comparable, Value any]() *ConcurrentMap[Key, Value] {
	cMap := &ConcurrentMap[Key, Value]{
		internalMap: make(map[Key]Value, 0),
	}

	return cMap
}

func NewConcurrentMapFromRaw[Key comparable, Value any](raw map[Key]Value) *ConcurrentMap[Key, Value] {
	cMap := NewConcurrentMap[Key, Value]()

	for k, v := range raw {
		cMap.internalMap[k] = v
	}

	return cMap
}

func (cMap *ConcurrentMap[Key, Value]) Set(key Key, value Value) error {
	cMap.lock.Lock()
	defer cMap.lock.Unlock()

	return cMap.set(key, value)
}

// IMPORTANT only use if lock is set otherwise race conditions can occur
// Is a helper for setting entries
func (cMap *ConcurrentMap[Key, Value]) set(key Key, value Value) error {
	_, ok := cMap.internalMap[key]
	if ok && !cMap.isOverride.Load() {
		return ErrKeyExistsNoOverride
	}

	cMap.internalMap[key] = value

	return nil
}

// IMPORTANT only use if lock is set otherwise race conditions can occur
// Is a helper for undo actions on error
func (cMap *ConcurrentMap[Key, Value]) undo(keys []Key) {
	for _, k := range keys {
		delete(cMap.internalMap, k)
	}
}

func (cMap *ConcurrentMap[Key, Value]) Get(key Key) Value {
	cMap.lock.RLock()
	defer cMap.lock.RUnlock()

	mw, ok := cMap.internalMap[key]

	var empty Value
	if !ok {
		return empty
	}

	return mw
}

func (cMap *ConcurrentMap[Key, Value]) Remove(key Key) {
	cMap.lock.Lock()
	delete(cMap.internalMap, key)
	cMap.lock.Unlock()
}

func (cMap *ConcurrentMap[Key, Value]) SetOverride(enable bool) {
	cMap.isOverride.Store(true)
}

func (cMap *ConcurrentMap[Key, Value]) Override() bool {
	return cMap.isOverride.Load()
}

func (cMap *ConcurrentMap[Key, Value]) ApplyMap(m map[Key]Value) error {
	cMap.lock.Lock()
	defer cMap.lock.Unlock()

	keys := make([]Key, 0)

	for k, v := range m {
		err := cMap.set(k, v)
		if err != nil {
			cMap.undo(keys)
			return err
		}
		keys = append(keys, k)
	}

	return nil
}

func (cMap *ConcurrentMap[Key, Value]) GetMap() map[Key]Value {
	cMap.lock.RLock()
	defer cMap.lock.RUnlock()

	mapCpy := make(map[Key]Value, len(cMap.internalMap))

	for k, v := range cMap.internalMap {
		mapCpy[k] = v
	}

	return mapCpy
}
