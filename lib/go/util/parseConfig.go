package util

import (
	"fmt"
	"os"
	"path"

	"github.com/BurntSushi/toml"
	"gopkg.in/yaml.v3"
)

func ParseConfig[T any](configPath string) (*T, error) {
	fileData, err := os.ReadFile(configPath)
	if err != nil {
		return nil, fmt.Errorf("failed to read file: %w", err)
	}

	ext := path.Ext(configPath)

	switch ext {
	case ".toml":
		return ParseTOMLConfig[T](fileData)
	case ".yaml", ".yml":
		return ParseYAMLConfig[T](fileData)
	}

	return nil, fmt.Errorf("no valid config file")
}

func ParseYAMLConfig[T any](data []byte) (*T, error) {
	cfg := new(T)
	err := yaml.Unmarshal(data, cfg)
	if err != nil {
		return nil, fmt.Errorf("failed to parse config: %w", err)
	}

	return cfg, nil
}

func ParseTOMLConfig[T any](data []byte) (*T, error) {
	cfg := new(T)
	err := toml.Unmarshal(data, cfg)
	if err != nil {
		return nil, fmt.Errorf("failed to parse config: %w", err)
	}

	return cfg, nil
}
