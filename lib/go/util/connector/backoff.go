package connector

import (
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
)

func GetBackoffDuration(backoff time.Duration, multiplier float64, maxBackoff time.Duration) time.Duration {
	jitter := time.Duration(util.RandFloat64Secure() * float64(backoff/4))
	if jitter < 0 {
		jitter = -jitter
	}
	backoff = time.Duration(float64(backoff) * multiplier)
	backoff = backoff + jitter
	if backoff > maxBackoff {
		backoff = maxBackoff
	}
	return backoff
}
