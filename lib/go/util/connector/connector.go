package connector

import (
	"context"
	"fmt"
	"sync/atomic"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"go.uber.org/zap"
)

type ConnectionEstablisher struct {
	Connector         Connector
	MaxRetries        int
	InitialBackoff    time.Duration
	MaxBackoff        time.Duration
	BackoffMultiplier float64
	Logger            *zap.Logger
}

type BaseConnector struct {
	State atomic.Uint32
}

type ConnectorState uint32

const (
	NOOP         ConnectorState = iota
	QUIT         ConnectorState = iota
	DISCONNECTED ConnectorState = iota
	RETRYING     ConnectorState = iota
	CONNECTED    ConnectorState = iota

	EVENT_CHAN_SIZE = 100
)

type Connector interface {
	Connect(context.Context) error
	Check(context.Context) error
	State(context.Context) ConnectorState
	Disconnect(context.Context) error
}

type ConnectionEvent struct {
	State ConnectorState
	Err   error
}

func (e *ConnectionEstablisher) EstablishConnection(ctx context.Context, checkIntervalSeconds int64) <-chan ConnectionEvent {
	result := make(chan ConnectionEvent, EVENT_CHAN_SIZE)

	go func(checkIntervalSeconds int64) {
		defer close(result)
		ticker := time.NewTicker(time.Duration(checkIntervalSeconds) * time.Second)
		tickerWorker := time.NewTicker(1 * time.Second)

		internalState := RETRYING

		for {
			ctx := context.Background()

			select {
			case <-ctx.Done():
				var err error
				err = e.Connector.Disconnect(ctx)

				if err != nil {
					err = fmt.Errorf("connection establisher canceled: %w", err)
				} else {
					err = fmt.Errorf("connection establisher canceled")
				}

				util.CheckAndClearChan(e.Logger, result, EVENT_CHAN_SIZE)
				result <- ConnectionEvent{Err: err, State: QUIT}
				close(result)
				return
			case <-ticker.C:
				e.Logger.Debug("Checking connector connection")
				// Check connection
				err := e.Connector.Check(ctx)

				util.CheckAndClearChan(e.Logger, result, EVENT_CHAN_SIZE)

				// Error with connection
				if err != nil {
					e.Logger.Error("Failed to check connector connection", zap.Error(err))
					result <- ConnectionEvent{Err: err, State: RETRYING}
					internalState = RETRYING
				} else {
					result <- ConnectionEvent{Err: nil, State: NOOP}
				}
			case <-tickerWorker.C:

				if e.Connector.State(ctx) == DISCONNECTED || internalState != RETRYING {
					continue
				}

				backoff := e.InitialBackoff
				retries := 0

				util.CheckAndClearChan(e.Logger, result, EVENT_CHAN_SIZE)

				// Connect state if not initial connected or in retry state
				for {
					e.Logger.Debug("Try connecting connector", zap.Duration("backoff", backoff), zap.Int("retryCount", retries))
					err := e.Connector.Connect(ctx)

					if err == nil {
						e.Logger.Info("Connector connected")
						result <- ConnectionEvent{Err: nil, State: CONNECTED}
						internalState = CONNECTED
						break
					}

					if retries >= e.MaxRetries && e.MaxRetries > 0 {
						result <- ConnectionEvent{Err: fmt.Errorf("connection establisher exceeded max retries (%d): %w", e.MaxRetries, err), State: QUIT}
						return
					}

					retries++
					backoff = GetBackoffDuration(backoff, e.BackoffMultiplier, e.MaxBackoff)

					if backoff > e.MaxBackoff {
						backoff = e.MaxBackoff
					}

					time.Sleep(backoff)
				}
			}
		}
	}(checkIntervalSeconds)

	return result
}
