package util_test

import (
	"testing"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"github.com/stretchr/testify/assert"
)

func TestRandBool(t *testing.T) {
	for i := 0; i < 10; i++ {
		result := util.RandBool()
		assert.Contains(t, []bool{true, false}, result, "RandBool should return true or false")
	}
}

func TestRandFloat(t *testing.T) {
	tests := []struct {
		name   string
		input  struct{ min, max float64 }
		output float64
	}{
		{"min=0 max=1", struct{ min, max float64 }{0, 1}, 0.5},
		{"min=10 max=20", struct{ min, max float64 }{10, 20}, 15},
		{"min=-10 max=10", struct{ min, max float64 }{-10, 10}, 0},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			result := util.RandFloat(tt.input.min, tt.input.max)
			assert.True(t, result >= tt.input.min && result <= tt.input.max,
				"Expected RandFloat(%v, %v) to return a value between %v and %v, but got %v",
				tt.input.min, tt.input.max, tt.input.min, tt.input.max, result)
		})
	}
}
