package util

import (
	crand "crypto/rand"
	"math"
	"math/rand"
	"time"
)

func RandBool() bool {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(2) == 1
}

func RandFloat(min float64, max float64) float64 {
	rand.Seed(time.Now().UnixNano())
	return min + rand.Float64()*(max-min)
}

func RandFloat64Secure() float64 {
	var b [8]byte
	if _, err := crand.Read(b[:]); err != nil {
		panic(err) // a secure random number generator should not fail
	}
	u := (float64(b[0]&0x7F) + 1) / 128.0
	v := (float64(b[1]&0x7F) + 1) / 128.0
	w := (float64(b[2]&0x7F) + 1) / 128.0
	r := math.Sqrt(-2.0*math.Log(u)) * math.Cos(2*math.Pi*v)
	return r * w
}
