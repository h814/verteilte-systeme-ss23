package util

import "go.uber.org/zap"

// Hack to prevent blocking on full channel
func CheckAndClearChan[T any](logger *zap.Logger, ch <-chan T, size int) {
	if len(ch) >= size {
		logger.Debug("Channel is full. Events are not being consumed... Clearing")
		for i := 0; i < len(ch); i++ {
			<-ch
		}
	}
}
