package httpserver

import (
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/log"
	"go.uber.org/zap"
)

var discardLogger *zap.Logger = log.DiscardLogger()
