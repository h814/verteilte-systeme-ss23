package httptracer

import (
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
	"go.opentelemetry.io/otel/attribute"
)

func httpRequestAttrs(req *httpserver.Request) []attribute.KeyValue {
	attrs := make([]attribute.KeyValue, 0)

	attrs = append(attrs, attribute.String("method", string(req.Method())))
	attrs = append(attrs, attribute.String("path", req.Path()))

	if req.ContentType() != "" {
		attrs = append(attrs, attribute.String("content", req.ContentType()))
	}

	return attrs
}
