package httptracer

import (
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

const (
	instrumentationName = "code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver/httptracer"
)

type HTTPTracer struct {
	handler     httpserver.Handler
	logger      *zap.Logger
	propagators propagation.TextMapPropagator
	tracer      trace.Tracer
}

var _ httpserver.Handler = &HTTPTracer{}

func NewHandler(logger *zap.Logger, handler httpserver.Handler) *HTTPTracer {
	tracer := &HTTPTracer{
		handler:     handler,
		logger:      logger.Named("httptracer"),
		propagators: otel.GetTextMapPropagator(),
	}

	return tracer
}

func (tr *HTTPTracer) ServerHTTP(req *httpserver.Request, res httpserver.Response) {
	requestStartTime := time.Now()
	tr.logger.Debug("Trace request")

	ctx := tr.propagators.Extract(req.Context(), &httpserver.HeaderCarrier{Headers: &req.Headers})
	opts := []trace.SpanStartOption{
		trace.WithAttributes(httpRequestAttrs(req)...),
		trace.WithSpanKind(trace.SpanKindServer),
	}

	tracer := tr.tracer

	if tracer == nil {
		if span := trace.SpanFromContext(req.Context()); span.SpanContext().IsValid() {
			tracer = newTracer(span.TracerProvider())
		} else {
			tracer = newTracer(otel.GetTracerProvider())
		}
	}

	ctx, span := tracer.Start(ctx, "httpserver", opts...)
	defer span.End()

	tr.handler.ServerHTTP(req.WithContext(ctx), res)
	elapsedTime := float64(time.Since(requestStartTime)) / float64(time.Millisecond)
	tr.logger.Debug("Trace request done", zap.Float64("elapsed", elapsedTime))
}

func newTracer(tp trace.TracerProvider) trace.Tracer {
	return tp.Tracer(instrumentationName, trace.WithInstrumentationVersion(SemVersion()))
}

func SemVersion() string {
	return "semver:" + Version()
}

func Version() string {
	return "0.0.1"
}
