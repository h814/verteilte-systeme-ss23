package httpserver

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"strconv"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"go.uber.org/zap"
)

type Response interface {
	Header() *util.ConcurrentMap[string, string]
	WriteHeader(int) error
	Json(interface{})
	io.Writer
}

var _ Response = &HTTPResponse{}

type HTTPResponse struct {
	conn           net.Conn
	req            *Request
	logger         *zap.Logger
	Headers        util.ConcurrentMap[string, string]
	headersWritten bool
	ctx            context.Context
}

func NewHTTPResponse(ctx context.Context, logger *zap.Logger, conn net.Conn, req *Request) *HTTPResponse {
	ctx, span := tracer.Start(ctx, "new response")
	defer span.End()

	res := &HTTPResponse{
		conn:    conn,
		req:     req,
		logger:  logger.Named("response"),
		Headers: *util.NewConcurrentMap[string, string](),
		ctx:     ctx,
	}

	res.Headers.SetOverride(true)

	return res
}

func (res *HTTPResponse) Json(data interface{}) {
	b, err := json.Marshal(data)
	if err != nil {
		res.WriteHeader(500)
		res.Write([]byte("Internal Error marshalling response"))
		return
	}

	res.Header().Set("Content-Type", "application/json")
	res.Write(b)
}

func (res *HTTPResponse) Header() *util.ConcurrentMap[string, string] {
	return &res.Headers
}

func (res *HTTPResponse) WriteHeader(code int) error {
	ctx, span := tracer.Start(res.ctx, "write headers")
	defer span.End()

	err := res.writeResponseLine(ctx, code)
	if err != nil {
		return err
	}

	err = res.writeHeaders(ctx)
	if err != nil {
		return err
	}

	res.headersWritten = true
	return nil
}

func (res *HTTPResponse) writeResponseLine(ctx context.Context, code int) error {
	ctx, span := tracer.Start(ctx, "write response line")
	defer span.End()

	statusText := http.StatusText(code)

	_, err := res.conn.Write([]byte("HTTP/1.1 " + strconv.Itoa(code) + " " + statusText + REQUEST_DELIM))
	if err != nil {
		return err
	}
	return nil
}

func (res *HTTPResponse) writeHeaders(ctx context.Context) error {
	ctx, span := tracer.Start(ctx, "write headers internal")
	defer span.End()
	headers := res.Headers.GetMap()

	for k, v := range headers {
		line := fmt.Sprintf("%s: %s%s", k, v, REQUEST_DELIM)
		_, err := res.conn.Write([]byte(line))
		if err != nil {
			return err
		}
	}

	_, err := res.conn.Write([]byte(REQUEST_DELIM))
	if err != nil {
		return err
	}

	return nil
}

func (res *HTTPResponse) Write(data []byte) (int, error) {
	ctx, span := tracer.Start(res.ctx, "write")
	defer span.End()

	res.ctx = ctx

	if !res.headersWritten {
		if res.Headers.Get("Content-Type") == "" {
			res.Headers.Set("Content-Type", "text/plain")
		}
		res.WriteHeader(200)
	}

	return res.conn.Write(data)
}
