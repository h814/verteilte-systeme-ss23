package httpserver

type Method string

const (
	NotAllowed Method = "NOT_ALLOWED"
	GET        Method = "GET"
	POST       Method = "POST"
	DELETE     Method = "DELETE"
)

func parseMethod(m string) (pm Method) {
	switch m {
	case "GET":
		pm = GET
	case "POST":
		pm = POST
	case "DELETE":
		pm = DELETE
	default:
		pm = NotAllowed
	}

	return
}
