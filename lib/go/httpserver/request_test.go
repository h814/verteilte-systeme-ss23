package httpserver

import (
	"context"
	"fmt"
	"io"
	"net"
	"strconv"
	"testing"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"github.com/stretchr/testify/assert"
)

func TestNewRequest(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)
	raw := "GET /test HTTP/1.1\r\nContent-Length: 4\r\nContent-Type: text\\plain\r\n\r\ntest"

	t.Run("Should return ErrEmpty if request is empty", func(t *testing.T) {
		l, err := net.Listen("tcp", ":56844")
		assert.Nil(err)
		go func() {
			time.Sleep(500 * time.Millisecond)
			conn, err := net.DialTCP("tcp", nil, &net.TCPAddr{
				IP:   net.IPv4(127, 0, 0, 1),
				Port: 56844,
			})
			assert.Nil(err)
			defer conn.Close()
			_, err = conn.Write([]byte(""))
			assert.Nil(err)
		}()

		conn, err := l.Accept()
		assert.Nil(err)

		req, err := NewRequest(context.Background(), discardLogger, conn)
		assert.NotNil(err)
		assert.Nil(req)
		assert.ErrorIs(err, ErrEmptyRequest)
	})

	l, err := net.Listen("tcp", ":56843")
	assert.Nil(err)

	go func() {
		time.Sleep(500 * time.Millisecond)
		conn, err := net.DialTCP("tcp", nil, &net.TCPAddr{
			IP:   net.IPv4(127, 0, 0, 1),
			Port: 56843,
		})
		defer conn.Close()
		assert.Nil(err)
		w, err := conn.Write([]byte(raw))
		assert.Nil(err)
		assert.Equal(len(raw), w)
	}()

	conn, err := l.Accept()
	assert.Nil(err)

	req, err := NewRequest(context.Background(), discardLogger, conn)
	assert.Nil(err)
	assert.Equal("GET", string(req.method))
	assert.Equal("HTTP/1.1", req.version)
	assert.Equal("/test", req.path)

	for k, v := range map[string]string{"Content-Length": "4", "Content-Type": "text\\plain"} {
		assert.Equal(v, req.Headers.Get(k))
	}

	data, err := io.ReadAll(req.Body)
	assert.Nil(err)
	assert.Equal("test", string(data))
}

func TestParseRequestLine(t *testing.T) {
	t.Parallel()
	ctx := context.Background()
	assert := assert.New(t)

	t.Run("Should return error if wrong part count is detected", func(t *testing.T) {
		t.Parallel()
		req := Request{}

		err := req.parseRequestLine(ctx, "POST HTTP/1.1")
		assert.NotNil(err)
		assert.Contains(err.Error(), "wrong request line size")

		err = req.parseRequestLine(ctx, "POST / HTTP/1.1 test")
		assert.NotNil(err)
		assert.Contains(err.Error(), "wrong request line size")
	})

	t.Run("Should return error if not allowed method is detected", func(t *testing.T) {
		t.Parallel()
		req := Request{}

		err := req.parseRequestLine(ctx, "WRONG / HTTP/1.1")
		assert.NotNil(err)
		assert.Contains(err.Error(), "http method is not allowed")
	})

	t.Run("Should set request line parts", func(t *testing.T) {
		t.Parallel()

		data := []struct {
			HTTP_V string
			METHOD string
			PATH   string
		}{
			{HTTP_V: "HTTP/1.1", METHOD: "GET", PATH: "/"},
			{HTTP_V: "HTTP/1.1", METHOD: "POST", PATH: "/1234"},
			{HTTP_V: "HTTP/2", METHOD: "POST", PATH: "/3434/ds?da565=342"},
			{HTTP_V: "HTTP/3", METHOD: "GET", PATH: "/abc/de/erf/df"},
		}

		for _, dd := range data {
			req := &Request{}
			err := req.parseRequestLine(ctx, fmt.Sprintf("%s %s %s", dd.METHOD, dd.PATH, dd.HTTP_V))
			assert.Nil(err)
			assert.Equal(dd.HTTP_V, req.version)
			assert.Equal(dd.METHOD, string(req.method))
			assert.Equal(dd.PATH, req.path)
		}
	})
}

func TestParseHeaders(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)
	data := []struct {
		expected map[string]string
		data     string
		rest     string
		finish   bool
	}{
		{
			data: "Content-Type: test\r\nData: 123\r\n",
			rest: "",
			expected: map[string]string{
				"Content-Type": "test",
				"Data":         "123",
			},
			finish: false,
		},
		{
			data: "Content-Type: test\r\nData: 12",
			rest: "Data: 12",
			expected: map[string]string{
				"Content-Type": "test",
			},
			finish: false,
		},
		{
			data: "Content-Type: test\r\nData: 123\r\n\r\n",
			rest: "",
			expected: map[string]string{
				"Content-Type": "test",
				"Data":         "123",
			},
			finish: true,
		},
	}

	ctx := context.Background()

	for _, dd := range data {
		req := &Request{
			Headers: *util.NewConcurrentMap[string, string](),
		}
		finish, err := req.parseHeaders(ctx, &dd.data)
		assert.Nil(err)
		assert.Equal(dd.finish, finish)

		for k, v := range dd.expected {
			assert.Equal(v, req.Headers.Get(k))
		}

		assert.Equal(dd.rest, dd.data)

	}
}

func TestBody(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)
	ctx := context.Background()

	t.Run("Should be be empty body if no content length header", func(t *testing.T) {
		req := &Request{
			Headers: *util.NewConcurrentMap[string, string](),
			logger:  discardLogger,
		}
		err := req.parseBody(ctx, "")
		assert.Nil(err)
		data, err := io.ReadAll(req.Body)
		assert.Nil(err)
		assert.Equal(0, len(data))
	})

	t.Run("Should error if content type is not parseable", func(t *testing.T) {
		req := &Request{
			Headers: *util.NewConcurrentMap[string, string](),
		}
		req.Headers.Set("Content-Length", "abc")
		err := req.parseBody(ctx, "")
		assert.NotNil(err)
		assert.ErrorContains(err, "strconv.Atoi")
	})

	t.Run("Should read body from raw", func(t *testing.T) {
		data := []struct {
			body   string
			length int
			bodyEx string
		}{
			{
				body:   "abcdefghijll",
				length: 10,
				bodyEx: "abcdefghij",
			},
			{
				body:   "test",
				length: 4,
				bodyEx: "test",
			},
			{
				body:   "",
				length: 0,
				bodyEx: "",
			},
			{
				body:   "test34,,ner,wnr3424,n",
				length: 4,
				bodyEx: "test",
			},
		}

		for _, dd := range data {
			req := &Request{
				Headers: *util.NewConcurrentMap[string, string](),
				logger:  discardLogger,
			}
			req.Headers.Set("Content-Length", strconv.Itoa(dd.length))
			err := req.parseBody(ctx, dd.body)
			assert.Nil(err)
			data, err := io.ReadAll(req.Body)
			assert.Nil(err)
			assert.Equal(dd.length, len(data))
			assert.Equal(dd.bodyEx, string(data))
		}
	})
}
