package httpserver

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"strconv"
	"strings"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"go.opentelemetry.io/otel/codes"
	"go.uber.org/zap"
)

const REQUEST_DELIM = "\r\n"

var ErrEmptyRequest = fmt.Errorf("empty request")

type HeaderCarrier struct {
	Headers *util.ConcurrentMap[string, string]
}

// Get returns the value associated with the passed key.
func (hc *HeaderCarrier) Get(key string) string {
	return hc.Headers.Get(key)
}

// Set stores the key-value pair.
func (hc *HeaderCarrier) Set(key string, value string) {
	hc.Headers.Set(key, value)
}

// Keys lists the keys stored in this carrier.
func (hc *HeaderCarrier) Keys() []string {
	m := hc.Headers.GetMap()
	keys := make([]string, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	return keys
}

type Request struct {
	conn        net.Conn
	Headers     util.ConcurrentMap[string, string]
	Body        io.ReadCloser
	method      Method
	path        string
	version     string
	contentType string
	host        net.Addr
	logger      *zap.Logger
	Params      util.ConcurrentMap[string, string]
	ctx         context.Context
	traceCtx    context.Context
}

func NewRequest(ctx context.Context, logger *zap.Logger, conn net.Conn) (*Request, error) {
	ctx, span := tracer.Start(ctx, "new request")
	defer span.End()

	req := &Request{
		conn:     conn,
		Headers:  *util.NewConcurrentMap[string, string](),
		Params:   *util.NewConcurrentMap[string, string](),
		logger:   logger.Named("request"),
		ctx:      context.Background(),
		traceCtx: ctx,
	}

	req.host = conn.RemoteAddr()

	reqLineParsed := false
	reqHeadersParsed := false

	empty := true
	requestRaw := ""
	for {
		buf := make([]byte, 128)
		readBytes, err := conn.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}

		empty = false

		requestRaw += string(buf[:readBytes])

		if !reqLineParsed {
			reqLineEnd := strings.Index(requestRaw, REQUEST_DELIM)

			if reqLineEnd != -1 {
				reqLine := requestRaw[:reqLineEnd]
				requestRaw = requestRaw[reqLineEnd+2:]

				err := req.parseRequestLine(ctx, reqLine)
				if err != nil {
					return nil, err
				}

				reqLineParsed = true
			} else {
				continue
			}
		}

		if !reqHeadersParsed {
			reqHeadersParsed, err = req.parseHeaders(ctx, &requestRaw)
			if err != nil {
				return nil, err
			}
		}

		if reqHeadersParsed {
			err = req.parseBody(ctx, requestRaw)
			if err != nil {
				return nil, err
			}

			break
		}
	}

	if empty {
		return nil, ErrEmptyRequest
	}

	return req, nil
}

func (req *Request) parseRequestLine(ctx context.Context, line string) error {
	ctx, span := tracer.Start(ctx, "parseRequestLine")
	defer span.End()

	line = strings.TrimSpace(line)
	parts := strings.Split(line, " ")

	if len(parts) != 3 {
		return fmt.Errorf("wrong request line size")
	}

	method := parseMethod(parts[0])
	if method == NotAllowed {
		return fmt.Errorf("this http method is not allowed: %s", method)
	}

	req.method = method
	req.path = parts[1]
	req.version = parts[2]

	return nil
}

func (req *Request) parseHeaders(ctx context.Context, requestRaw *string) (bool, error) {
	ctx, span := tracer.Start(ctx, "parseHeaders")
	defer span.End()
	for {
		index := strings.Index(*requestRaw, REQUEST_DELIM)

		if index != -1 {
			headerLine := (*requestRaw)[:index]
			*requestRaw = (*requestRaw)[index+2:]

			if headerLine == "" {
				return true, nil
			}

			headerParts := strings.Split(headerLine, ":")
			key := strings.TrimSpace(headerParts[0])
			value := strings.TrimSpace(headerParts[1])

			err := req.Headers.Set(key, value)
			if err != nil {
				return false, err
			}

		} else {
			break
		}
	}

	return false, nil
}

func (req *Request) parseBody(ctx context.Context, requestRaw string) error {
	ctx, span := tracer.Start(ctx, "parseBody")
	defer span.End()
	contentLengthStr := req.Headers.Get("Content-Length")

	if contentLengthStr == "" {
		req.logger.Debug("Body has no content")
		req.Body = io.NopCloser(strings.NewReader(""))
		return nil
	}

	contentLength, err := strconv.Atoi(contentLengthStr)
	if err != nil {
		return err
	}

	readCum := len(requestRaw)
	bodyRaw := requestRaw

	if len(requestRaw) > contentLength {
		bodyRaw = requestRaw[:contentLength]
	}

	for {
		if readCum >= contentLength {
			break
		}

		buf := make([]byte, 1024)
		readBytes, err := req.conn.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
		}

		bodyRaw += string(buf[:readBytes])

		readCum += readBytes
	}

	req.Body = io.NopCloser(strings.NewReader(bodyRaw))

	return nil
}

func (req *Request) Method() Method {
	return req.method
}

func (req *Request) Path() string {
	return req.path
}

func (req *Request) Version() string {
	return req.version
}

func (req *Request) ContentType() string {
	return req.contentType
}

func (req *Request) Host() net.Addr {
	return req.host
}

func (req *Request) Context() context.Context {
	return req.ctx
}

func (req *Request) WithContext(ctx context.Context) *Request {
	req.ctx = ctx
	return req
}

func DecodeJson[T any](ctx context.Context, req *Request, res Response) (T, bool) {
	ctx, span := tracer.Start(ctx, "DecodeJson")
	defer span.End()

	var data T
	if req.Headers.Get("Content-Type") == "application/json" {
		dec := json.NewDecoder(req.Body)

		err := dec.Decode(&data)
		if err != nil {
			res.WriteHeader(500)
			res.Write([]byte("Internal Error failed to decode body"))
			span.RecordError(err)
			span.SetStatus(codes.Error, "Decoding failed")
			return data, false
		}
	} else {
		res.WriteHeader(400)
		res.Write([]byte("Wrong content type... Only JSON allowed"))
		span.SetStatus(codes.Error, "Wrong content type")
		return data, false
	}

	return data, true
}
