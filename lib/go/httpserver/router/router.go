package router

import (
	"context"
	"net/http"
	"strings"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

var tracer trace.Tracer = otel.Tracer("httprouter")

type Router struct {
	logger     *zap.Logger
	routes     []Route
	middleware []Middleware
}

type Route struct {
	Method  httpserver.Method
	Path    string
	Handler httpserver.HandlerFunc
}

type Middleware struct{}

func New(logger *zap.Logger) *Router {
	r := &Router{
		logger:     logger.Named("router"),
		middleware: make([]Middleware, 0),
		routes:     make([]Route, 0),
	}

	return r
}

func (r *Router) GET(path string, handler httpserver.HandlerFunc) *Router {
	return r.Handle(path, httpserver.GET, handler)
}

func (r *Router) GETHandler(path string, handler httpserver.Handler) *Router {
	return r.GET(path, handler.ServerHTTP)
}

func (r *Router) POST(path string, handler httpserver.HandlerFunc) *Router {
	return r.Handle(path, httpserver.POST, handler)
}

func (r *Router) POSTHandler(path string, handler httpserver.Handler) *Router {
	return r.POST(path, handler.ServerHTTP)
}

func (r *Router) DELETE(path string, handler httpserver.HandlerFunc) *Router {
	return r.Handle(path, httpserver.DELETE, handler)
}

func (r *Router) DELETEHandler(path string, handler httpserver.Handler) *Router {
	return r.DELETE(path, handler.ServerHTTP)
}

func (r *Router) Handle(path string, method httpserver.Method, handler httpserver.HandlerFunc) *Router {
	r.routes = append(r.routes, Route{
		Path:    path,
		Method:  method,
		Handler: handler,
	})

	return r
}

func (r *Router) HandleHandler(path string, method httpserver.Method, handler httpserver.Handler) *Router {
	return r.Handle(path, method, handler.ServerHTTP)
}

// TODO add methods for setting middleware

func (r *Router) ServerHTTP(req *httpserver.Request, res httpserver.Response) {
	ctx, span := tracer.Start(req.Context(), "HTTPRouter/ServeHTTP")
	defer span.End()
	req = req.WithContext(ctx)

	span.SetAttributes(attribute.String("path", req.Path()))

	routeIndex, noMethodMatch := checkRouteMatch(ctx, r.logger, req, r.routes)

	if routeIndex != -1 {
		r.logger.Debug("Matched route", zap.String("path", req.Path()), zap.String("method", string(r.routes[routeIndex].Method)))
		r.routes[routeIndex].Handler(req, res)
		res.Write([]byte{})
		return
	}

	r.logger.Debug("No routes matched", zap.Bool("methodNotAllowed", noMethodMatch))

	if noMethodMatch {
		span.SetStatus(codes.Error, "Method not allowed for this route")
		res.WriteHeader(http.StatusMethodNotAllowed)
		res.Write([]byte("405 - Method not allowed"))
		return
	}

	span.SetStatus(codes.Error, "Route is not existing")
	res.WriteHeader(http.StatusNotFound)
	res.Write([]byte("404 - Not found"))
}

func checkRouteMatch(ctx context.Context, logger *zap.Logger, req *httpserver.Request, routes []Route) (int, bool) {
	ctx, span := tracer.Start(ctx, "HTTPRouter/checkRouteMatch")
	defer span.End()

	reqPathParts := strings.Split(req.Path()[1:], "/")
	routeMatchNoMethod := false

routeLoop:
	for i, route := range routes {
		routePathParts := strings.Split(route.Path[1:], "/")
		tmpParams := make(map[string]string, 0)

		for i, part := range routePathParts {
			// Out of bounds
			if i > len(reqPathParts)-1 {
				// println("Route no match out of bounds")
				continue routeLoop
			}

			if strings.HasPrefix(part, "{") && strings.HasSuffix(part, "}") {
				paramName := part[1 : len(part)-1]
				tmpParams[paramName] = reqPathParts[i]
				continue
			}

			// Match
			if i == len(routePathParts) && routePathParts[i] == "*" {
				// println("Match wildcard")
				break
			}

			// No match
			if routePathParts[i] != "*" && reqPathParts[i] != part {
				// println("Route no match unequal")
				continue routeLoop
			}
		}

		if len(reqPathParts) != len(routePathParts) && !strings.HasSuffix(strings.TrimRight(route.Path, "/"), "*") {
			// println("Route no match length diff")
			continue
		}

		err := req.Params.ApplyMap(tmpParams)
		if err != nil {
			logger.Error("Failed to set param", zap.Error(err))
		}

		if route.Method != req.Method() {
			// println("Route no match method")
			routeMatchNoMethod = true
			continue
		}

		// First match is returned
		return i, false
	}

	return -1, routeMatchNoMethod
}
