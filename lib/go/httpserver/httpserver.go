package httpserver

import (
	"context"
	"errors"
	"net"
	"strconv"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type Handler interface {
	ServerHTTP(*Request, Response)
}

type HandlerFunc func(*Request, Response)

type HTTPServer struct {
	Addr    string
	Logger  *zap.Logger
	conn    *net.TCPListener
	Handler Handler
}

var tracer trace.Tracer = otel.Tracer("httpserver")

func (httpServer *HTTPServer) Shutdown(ctx context.Context) error {
	err := httpServer.conn.Close()
	return err
}

func (httpServer *HTTPServer) ListenAndServe() error {
	if httpServer.Handler == nil {
		//return fmt.Errorf("no handler set")
	}

	if httpServer.Addr == "" {
		httpServer.Addr = ":http"
	}

	host, port, err := net.SplitHostPort(httpServer.Addr)
	if err != nil {
		return err
	}

	portParsed, err := strconv.Atoi(port)
	if err != nil {
		return err
	}

	conn, err := net.ListenTCP("tcp", &net.TCPAddr{Port: portParsed, IP: net.ParseIP(host)})
	if err != nil {
		return err
	}

	go func() {
		defer conn.Close()
		for {
			newConn, err := conn.Accept()
			if err != nil {
				if errors.Is(err, net.ErrClosed) {
					httpServer.Logger.Warn("TCP socket closed ending accepting routine")
					return
				}
				httpServer.Logger.Error("Error accepting TCP connection", zap.Error(err))
			}

			go handleTCP(context.Background(), httpServer.Logger, newConn, httpServer.Handler)
		}
	}()

	return nil
}

func handleTCP(ctx context.Context, logger *zap.Logger, conn net.Conn, handler Handler) {
	ctx, span := tracer.Start(ctx, "handleTCP")
	defer span.End()

	defer conn.Close()

	req, err := NewRequest(ctx, logger, conn)
	if err != nil {

		if err == ErrEmptyRequest {
			logger.Debug("Skipping empty request")
			return
		}

		logger.Error("Failed to parse tcp request to http request", zap.Error(err))
		return
	}

	req = req.WithContext(ctx)

	res := NewHTTPResponse(ctx, logger, conn, req)
	handler.ServerHTTP(req, res)
}
