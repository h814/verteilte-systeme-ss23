package raft

import (
	"github.com/shaj13/raft/raftlog"
	"go.uber.org/zap"
)

type RaftZap struct {
	*zap.SugaredLogger
}

func NewRaftZap(logger *zap.SugaredLogger) *RaftZap {
	return &RaftZap{
		SugaredLogger: logger,
	}
}

// Not expecting to use this.
func (r *RaftZap) V(l int) raftlog.Verbose {
	return r
}

// This is a hack and does not actually work.
// Not expecting to use this.
func (r *RaftZap) Enabled() bool {
	return true
}

func (r *RaftZap) Warning(args ...interface{}) {
	r.SugaredLogger.Warn(args)
}

func (r *RaftZap) Warningf(str string, args ...interface{}) {
	r.SugaredLogger.Warnf(str, args)
}
