package raft

type Config struct {
	BindAddr          string   `toml:"bindAddr" yaml:"bindAddr"`
	Port              uint16   `toml:"port" yaml:"port"`
	BootstrapClusters []string `toml:"bootstrapClusters" yaml:"bootstrapClusters"`
	Hostname          *string  `toml:"hostname" yaml:"hostname"`
}
