package raft

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"strings"
	"sync"

	"go.uber.org/zap"
)

type stateMachine struct {
	logger *zap.Logger
	mu     sync.Mutex
	kv     map[string][]byte
}

type StateMachineEntry struct {
	Key   string
	Value []byte
}

func NewStateMachine(logger *zap.Logger) *stateMachine {
	return &stateMachine{
		kv:     make(map[string][]byte),
		logger: logger.Named("state-machine"),
	}
}

func (s *stateMachine) Apply(data []byte) {
	var e StateMachineEntry
	if err := json.Unmarshal(data, &e); err != nil {
		s.logger.Error("Unable to Unmarshal entry", zap.Error(err))
		return
	}

	s.mu.Lock()
	defer s.mu.Unlock()
	s.kv[e.Key] = e.Value
}

func (s *stateMachine) Snapshot() (io.ReadCloser, error) {
	s.mu.Lock()
	defer s.mu.Unlock()
	buf, err := json.Marshal(&s.kv)
	if err != nil {
		return nil, err
	}
	return io.NopCloser(strings.NewReader(string(buf))), nil
}

func (s *stateMachine) Restore(r io.ReadCloser) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	buf, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}

	err = json.Unmarshal(buf, &s.kv)
	if err != nil {
		return err
	}

	return r.Close()
}

func (s *stateMachine) Read(key string) []byte {
	s.mu.Lock()
	defer s.mu.Unlock()
	return s.kv[key]
}
