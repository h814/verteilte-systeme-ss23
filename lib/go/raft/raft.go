package raft

import (
	"context"
	"fmt"
	"net"
	"os"
	"strings"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"github.com/samber/lo"
	"github.com/shaj13/raft"
	"github.com/shaj13/raft/transport"
	"github.com/shaj13/raft/transport/raftgrpc"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

type Raft struct {
	raftServer *grpc.Server
	logger     *zap.Logger
	name       string
	members    []raft.RawMember
	node       *raft.Node
	cfg        Config
}

func remove[T any](s []T, i int) []T {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func New(logger *zap.Logger, name string, cfg Config) *Raft {
	var err error
	hostname := ""

	if cfg.Hostname == nil {
		hostname, err = os.Hostname()
		if err != nil {
			panic(err)
		}
	} else {
		hostname = *cfg.Hostname
	}

	logger.Info("Detected hostname", zap.String("hostname", hostname))

	members := make([]raft.RawMember, len(cfg.BootstrapClusters))
	for i, addr := range cfg.BootstrapClusters {
		members[i] = raft.RawMember{ID: uint64(i + 1), Address: addr}
	}

	members_sort := make([]raft.RawMember, 0, len(cfg.BootstrapClusters))

	item, self_index, ok := lo.FindIndexOf(members, func(i raft.RawMember) bool {
		return strings.Contains(i.Address, fmt.Sprintf(":%d", cfg.Port)) && strings.Contains(i.Address, hostname)
	})

	if ok {
		members_sort = append(members_sort, item)
		members = remove(members, self_index)
		members_sort = append(members_sort, members...)
	}

	unaryC, streamIC := telemetry.GetGRPCClientInterceptors()
	raftgrpc.Register(
		raftgrpc.WithDialOptions(grpc.WithInsecure(), grpc.WithUnaryInterceptor(unaryC), grpc.WithStreamInterceptor(streamIC)),
	)

	var opts []grpc.ServerOption
	unary, stream := telemetry.GetGRPCServerInterceptors()

	opts = append(opts, grpc.UnaryInterceptor(unary), grpc.StreamInterceptor(stream))

	return &Raft{
		logger:     logger.Named("raft"),
		members:    members_sort,
		name:       name,
		cfg:        cfg,
		raftServer: grpc.NewServer(opts...),
	}
}

func (r *Raft) Start() error {
	r.logger.Info("Starting raft", zap.Uint16("port", r.cfg.Port), zap.String("bindAddr", r.cfg.BindAddr), zap.Any("members", r.members))
	fsm := NewStateMachine(r.logger)

	logger := NewRaftZap(r.logger.Named("internal").Sugar())

	r.node = raft.NewNode(fsm, transport.GRPC, raft.WithLogger(logger), raft.WithStateDIR("/tmp/raft/"+r.name))
	raftgrpc.RegisterHandler(r.raftServer, r.node.Handler())

	go func() {
		lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", r.cfg.BindAddr, r.cfg.Port))
		if err != nil {
			r.logger.Error("Failed to listen to tcp", zap.Error(err))
		}

		err = r.raftServer.Serve(lis)
		if err != nil {
			r.logger.Error("Failed to server grpc server", zap.Error(err))
		}
	}()

	go func() {
		for {
			r.logger.Info("Trying to start raft node", zap.Any("members", r.members))
			err := r.node.Start(raft.WithFallback(raft.WithInitCluster(), raft.WithRestart()), raft.WithMembers(r.members...))
			if err != nil && err != raft.ErrNodeStopped {
				r.logger.Error("Error starting raft node", zap.Error(err))
				time.Sleep(time.Second * 5)
			}
			break
		}
	}()

	// TODO
	/*
		1. Check if node is leader
		2. if rpc gets rescue request, replicate and coordinate with other nodes
		3. anyways subscribe to rescue-coordination kafka topic
		4. leader must collect all rescue-offers for in interval x
		5. leader must decide which offers to accept with wich amount
		6. leader must send rescue-accept to all affected nodes with its amount
		7. nodes which are subscribed to rescue-accept must transfer the amount to the requesting bank
	*/

	return nil
}

func (r *Raft) Node() *raft.Node {
	return r.node
}

func (r *Raft) Shutdown(ctx context.Context) {
	r.raftServer.GracefulStop()
	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()
	r.node.Shutdown(ctx)
}
