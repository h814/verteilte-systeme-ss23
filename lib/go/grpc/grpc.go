package grpc

import (
	"fmt"
	"net"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type Grpc struct {
	log          *zap.Logger
	server       *grpc.Server
	cfg          GRPCServerConfig
	isRegistered bool
}

func New(logger *zap.Logger, cfg GRPCServerConfig) *Grpc {
	grpcServer := &Grpc{
		log:          logger.Named("grpc"),
		cfg:          cfg,
		isRegistered: false,
	}

	var opts []grpc.ServerOption
	unary, stream := telemetry.GetGRPCServerInterceptors()

	opts = append(opts, grpc.UnaryInterceptor(unary), grpc.StreamInterceptor(stream))

	grpcServer.server = grpc.NewServer(opts...)

	reflection.Register(grpcServer.server)

	return grpcServer
}

func RegisterGrpc[T any](grpcSvc *Grpc, regFn func(*grpc.Server, T), regSvc T) {
	grpcSvc.isRegistered = true
	regFn(grpcSvc.server, regSvc)
}

func (grpcSvc *Grpc) Run() {
	if !grpcSvc.isRegistered {
		grpcSvc.log.Fatal("no grpc service registered yet")
	}

	lis, err := net.Listen("tcp", fmt.Sprintf("%s:%d", grpcSvc.cfg.BindAddr, grpcSvc.cfg.Port))
	if err != nil {
		grpcSvc.log.Fatal("failed to listen ", zap.String("bindAddr", grpcSvc.cfg.BindAddr), zap.Uint16("port", grpcSvc.cfg.Port), zap.Error(err))
	}

	grpcSvc.log.Info("Started tcp listener for grpc", zap.String("bindAddr", grpcSvc.cfg.BindAddr), zap.Uint16("port", grpcSvc.cfg.Port))

	grpcSvc.log.Info("Try starting grpc server", zap.String("bindAddr", grpcSvc.cfg.BindAddr), zap.Uint16("port", grpcSvc.cfg.Port))
	err = grpcSvc.server.Serve(lis)

	if err != nil {
		grpcSvc.log.Fatal("Failed to listen ", zap.String("bindAddr", grpcSvc.cfg.BindAddr), zap.Uint16("port", grpcSvc.cfg.Port), zap.Error(err))
	}
}
