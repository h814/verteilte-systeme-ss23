package grpc

type GRPCServerConfig struct {
	BindAddr string `yaml:"bindAddr" toml:"bindAddr"`
	Port     uint16 `yaml:"port" toml:"port"`
}
