package log

import (
	"io"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

func DiscardLogger() *zap.Logger {
	priority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return true
	})

	encoder := zapcore.NewConsoleEncoder(defaultConsoleEncoderConfig)
	discardCore := zapcore.NewCore(encoder, zapcore.AddSync(io.Discard), priority)
	return zap.New(discardCore)
}
