package log

import (
	"go.uber.org/zap/zapcore"
)

type Config struct {
	Name    string              `yaml:"name" toml:"name"`
	Console ConsoleLoggerConfig `yaml:"console" toml:"console"`
	File    FileLoggerConfig    `yaml:"file" toml:"file"`
	Loki    LokiLoggerConfig    `yaml:"loki" toml:"loki"`
}

type ConsoleLoggerConfig struct {
	LogLevel string `yaml:"logLevel" toml:"logLevel"`
	Enabled  bool   `yaml:"enabled" toml:"enabled"`
}

type FileLoggerConfig struct {
	LogLevel string `yaml:"logLevel" toml:"logLevel"`
	Enabled  bool   `yaml:"enabled" toml:"enabled"`
	Json     bool   `yaml:"json" toml:"json"`
	FilePath string `yaml:"path" toml:"path"`
	Create   *bool  `yaml:"create" toml:"create"`
}

type LokiLoggerConfig struct {
	LogLevel string `yaml:"logLevel" toml:"logLevel"`
	Enabled  bool   `yaml:"enabled" toml:"enabled"`
	URL      string `yaml:"url" toml:"url"`
	Username string `yaml:"username" toml:"username"`
	Password string `yaml:"password" toml:"password"`
}

var defaultConsoleEncoderConfig zapcore.EncoderConfig = zapcore.EncoderConfig{
	MessageKey:    "message",
	LevelKey:      "level",
	EncodeLevel:   zapcore.CapitalColorLevelEncoder,
	TimeKey:       "time",
	EncodeTime:    CustomTimeEncoder,
	CallerKey:     "caller",
	EncodeCaller:  zapcore.FullCallerEncoder,
	NameKey:       "name",
	EncodeName:    zapcore.FullNameEncoder,
	StacktraceKey: "stacktrace",
	FunctionKey:   "function",
}

var defaultJsonEncoderConfig zapcore.EncoderConfig = zapcore.EncoderConfig{
	MessageKey:    "message",
	LevelKey:      "level",
	EncodeLevel:   zapcore.LowercaseLevelEncoder,
	TimeKey:       "time",
	EncodeTime:    CustomTimeEncoder,
	CallerKey:     "caller",
	EncodeCaller:  zapcore.FullCallerEncoder,
	NameKey:       "name",
	EncodeName:    zapcore.FullNameEncoder,
	StacktraceKey: "stacktrace",
	FunctionKey:   "function",
}
