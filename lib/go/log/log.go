package log

import (
	"context"
	"os"
	"path/filepath"
	"time"

	zaploki "github.com/paul-milne/zap-loki"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

const RFC3339_TIME_FORMAT string = "2006-01-02T15:04:05-0700"

func New(config Config) (*zap.Logger, error) {
	teeLoggers := make([]zapcore.Core, 0)

	priority := zap.LevelEnablerFunc(func(lvl zapcore.Level) bool {
		return true
	})

	if config.Console.Enabled {
		encoder := zapcore.NewConsoleEncoder(defaultConsoleEncoderConfig)

		zapcore.NewCore(encoder, zapcore.Lock(os.Stderr), priority)
		teeLoggers = append(teeLoggers, zapcore.NewCore(encoder, zapcore.Lock(os.Stderr), priority))
	}

	if config.File.Enabled {

		if config.File.Create == nil {
			config.File.Create = new(bool)
			*config.File.Create = true
		}

		encoder := zapcore.NewJSONEncoder(defaultJsonEncoderConfig)

		path := os.ExpandEnv(config.File.FilePath)

		if *config.File.Create {
			_, err := checkAndCreate(path)
			if err != nil {
				return nil, err
			}
		}

		writer, _, err := zap.Open(path)
		if err != nil {
			return nil, err
		}

		teeLoggers = append(teeLoggers, zapcore.NewCore(encoder, writer, priority))
	}

	if config.Loki.Enabled {
		encoder := zapcore.NewJSONEncoder(defaultJsonEncoderConfig)

		zl, err := zaploki.New(context.Background(), zaploki.Config{Url: config.Loki.URL, Labels: map[string]string{"test": "123"}}).Sink(nil)
		if err != nil {
			return nil, err
		}

		writer := zapcore.AddSync(zl)

		teeLoggers = append(teeLoggers, zapcore.NewCore(encoder, writer, priority))
	}

	combinedLoggerCore := zapcore.NewTee(teeLoggers...)
	combinedLogger := zap.New(combinedLoggerCore).Named(config.Name)
	combinedLogger.Sync()

	return combinedLogger, nil
}

func CustomTimeEncoder(t time.Time, enc zapcore.PrimitiveArrayEncoder) {
	enc.AppendString(t.Format(RFC3339_TIME_FORMAT))
}

func checkAndCreate(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return false, nil
	}

	if os.IsNotExist(err) {
		dir := filepath.Dir(path)
		if err = os.MkdirAll(dir, os.ModePerm); err != nil {
			return false, err
		}

		return true, nil
	}

	return false, err
}
