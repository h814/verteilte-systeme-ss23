package probes

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"sync/atomic"

	"go.uber.org/zap"
)

type Probes struct {
	ready   atomic.Bool
	startup atomic.Bool
	logger  *zap.Logger
	server  *http.Server
}

type probeResponse struct {
	Code      uint16 `json:"code"`
	Msg       string `json:"msg"`
	ProbeType string `json:"probeType"`
}

func New(config Config, logger *zap.Logger) *Probes {
	probes := &Probes{}

	if config.Port == 0 {
		logger.Fatal("The provided port is not possible or was not set")
	}

	probes.server = probes.initServer(config.BindAddr, config.Port)
	probes.logger = logger.Named("probes")

	return probes
}

func (p *Probes) initServer(bindAddr string, port uint16) *http.Server {
	mux := http.NewServeMux()
	mux.HandleFunc("/ready", p.readyAndLiveHandler)
	mux.HandleFunc("/live", p.readyAndLiveHandler)
	mux.HandleFunc("/startup", p.startupHandler)

	server := &http.Server{
		Addr:    fmt.Sprintf("%s:%d", bindAddr, port),
		Handler: mux,
	}

	return server
}

func (p *Probes) startupHandler(res http.ResponseWriter, req *http.Request) {
	probeRes := probeResponse{ProbeType: "startup", Code: 200, Msg: "Service startup probe successful"}

	if !p.Startup() {
		probeRes.Code = 400
		probeRes.Msg = "Service startup probe failed"
	}

	sendProbeResponse(p.logger, res, probeRes)
}

func (p *Probes) readyAndLiveHandler(res http.ResponseWriter, req *http.Request) {
	probeRes := probeResponse{ProbeType: "read or live", Code: 200, Msg: "Ready or liveness probe successful"}

	if !p.LiveOrReady() {
		probeRes.Code = 400
		probeRes.Msg = "Service startup probe failed"
	}

	sendProbeResponse(p.logger, res, probeRes)
}

func sendProbeResponse(logger *zap.Logger, res http.ResponseWriter, probeRes probeResponse) {
	res.Header().Set("Content-Type", "application/json")
	res.WriteHeader(int(probeRes.Code))

	bytes, err := json.Marshal(probeRes)
	if err != nil {
		logger.Error("Failed to encode probe json response", zap.Error(err))
	}

	res.Write(bytes)
}

func (p *Probes) Shutdown(ctx context.Context) error {
	return p.server.Shutdown(ctx)
}

func (p *Probes) LiveOrReady() bool {
	return p.ready.Load()
}

func (p *Probes) Startup() bool {
	return p.startup.Load()
}

func (p *Probes) SetLiveOrReady(val bool) {
	p.logger.Debug("Live or ready was set", zap.Bool("value", val))
	p.ready.Store(val)
}

func (p *Probes) SetStartup(val bool) {
	p.logger.Debug("Startup was set", zap.Bool("value", val))
	p.startup.Store(val)
}

func (p *Probes) ListenAndServe() {
	host, port, err := net.SplitHostPort(p.server.Addr)
	if err != nil {
		p.logger.Error("Failed to split host and port", zap.Error(err))
	}

	go func() {
		p.logger.Info("Starting probe server", zap.String("port", port), zap.String("host", host))
		err := p.server.ListenAndServe()
		if err != nil {
			p.logger.Fatal("Failed to start server", zap.Error(err))
		}
	}()
}
