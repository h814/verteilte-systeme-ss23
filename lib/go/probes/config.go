package probes

type Config struct {
	Port     uint16 `yaml:"port" toml:"port"`
	BindAddr string `yaml:"bindAddr" toml:"bindAddr"`
}
