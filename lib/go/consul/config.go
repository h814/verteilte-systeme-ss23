package consul

type Config struct {
	Port           uint16 `yaml:"port" toml:"port"`
	Host           string `yaml:"host" toml:"host"`
	ExtraCheckHost string `yaml:"extraCheckHost" toml:"extraCheckHost"`
}
