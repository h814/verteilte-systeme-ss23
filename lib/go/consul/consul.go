package consul

import (
	"fmt"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	consulapi "github.com/hashicorp/consul/api"
	"go.uber.org/zap"
)

type Consul struct {
	client         *consulapi.Client
	logger         *zap.Logger
	id             string
	extraCheckHost string
}

func New(logger *zap.Logger, config Config) (*Consul, error) {
	consul := &Consul{
		logger:         logger,
		extraCheckHost: config.ExtraCheckHost,
	}

	consulCfg := consulapi.DefaultConfig()
	consulCfg.Address = fmt.Sprintf("%s:%d", config.Host, config.Port)
	consulClient, err := consulapi.NewClient(consulCfg)
	if err != nil {
		return nil, fmt.Errorf("failed to create consul client: %w", err)
	}
	consul.client = consulClient

	return consul, nil
}

func (consul *Consul) RegisterService(id string, name string, healthPort uint16, servicePort uint16, tags []string) error {
	consul.id = id
	registration := new(consulapi.AgentServiceRegistration)
	registration.ID = id
	registration.Name = name

	ip, err := util.GetOutboundIP()
	if err != nil {
		return fmt.Errorf("failed to get outbound ip: %w", err)
	}

	registration.Address = ip.String()
	registration.Port = int(servicePort)
	registration.Check = new(consulapi.AgentServiceCheck)
	registration.Tags = tags

	checkHost := ip.String()

	if consul.extraCheckHost != "" {
		checkHost = consul.extraCheckHost
	}

	registration.Check.HTTP = fmt.Sprintf("http://%s:%v/ready", checkHost, healthPort)
	registration.Check.Interval = "5s"
	registration.Check.Timeout = "3s"

	err = consul.client.Agent().ServiceRegister(registration)
	if err != nil {
		return fmt.Errorf("failed to register service: %w", err)
	}

	return nil
}

func (consul *Consul) DeregisterService() error {
	err := consul.client.Agent().ServiceDeregister(consul.id)
	if err != nil {
		return fmt.Errorf("failed to deregister service: %w", err)
	}

	return nil
}

func (consul *Consul) GetBanks() ([]*consulapi.CatalogService, error) {
	serviceNames, _, err := consul.client.Catalog().Services(&consulapi.QueryOptions{Filter: "bank in ServiceTags"})
	if err != nil {
		return nil, fmt.Errorf("failed to fetch bank service names: %w", err)
	}

	services := make([]*consulapi.CatalogService, 0, len(serviceNames))

	for name := range serviceNames {
		svc, _, err := consul.client.Catalog().Service(name, "bank", &consulapi.QueryOptions{})
		if err != nil {
			return nil, fmt.Errorf("failed to fetch bank service (%s): %w", name, err)
		}
		services = append(services, svc[0])
	}

	return services, nil
}
