## Prerequisites

- docker
- [bazelisk](https://github.com/bazelbuild/bazelisk)
- [volta](https://docs.volta.sh/guide/getting-started)

## Usage

```bash
# Build exchange
bazel build --@io_bazel_rules_go//go/config:pure //exchange/cmd:cmd_amd64
bazel build --@io_bazel_rules_go//go/config:pure //exchange/cmd:cmd_amd64

# Local use
bazel build //exchange/cmd:svc_image_tar_amd64
cat $(bazel cquery --output=files //exchange/cmd:svc_image_tar_amd64) | docker load

# Multi arch amd64 & arm64
bazel build //exchange/cmd:svc_image_multi

# Push
# bazel run --@io_bazel_rules_go//go/config:pure //exchange/cmd:push_local
bazel run --@io_bazel_rules_go//go/config:pure //exchange/cmd:push_gitlab

# Build bank
bazel build --@io_bazel_rules_go//go/config:pure //bank/cmd:cmd_amd64
bazel build --@io_bazel_rules_go//go/config:pure //bank/cmd:cmd_amd64

# Local use
bazel build //bank/cmd:svc_image_tar_amd64
cat $(bazel cquery --output=files //bank/cmd:svc_image_tar_amd64) | docker load

# Multi arch amd64 & arm64
bazel build //bank/cmd:svc_image_multi

# Push
# bazel run --@io_bazel_rules_go//go/config:pure //bank/cmd:push_local
bazel run --@io_bazel_rules_go//go/config:pure //bank/cmd:push_gitlab


# Frotend dev
# Install node modules
yarn

# Run dev
yarn workspace @mue-rue-vs/bank-ui dev

# Docker

docker-compose -f deployment/local/compose/docker-compose.yaml up

# Runner
mkdir -p /tmp/gitlab-runner/vs/cache
docker run -d --name gitlab-runner --restart always \
  -v $PWD/ci/gitlab-runner:/etc/gitlab-runner \
  -v /tmp/gitlab-runner/vs/cache:/cache \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest

docker run --rm -it -v $PWD/ci/gitlab-runner:/etc/gitlab-runner gitlab/gitlab-runner register 
```
