package types

import "net"

type Client struct {
	IP   net.IP
	Port int
}
