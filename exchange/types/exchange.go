package types

import "code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"

type Exchange struct {
	Shares *util.ConcurrentMap[string, Share]
}

type Share struct {
	Name  string  `json:"name"`
	Code  string  `json:"code"`
	Value float64 `json:"value"`
}
