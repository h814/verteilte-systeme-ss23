package types

import (
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/log"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/probes"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
)

type Config struct {
	Name      string           `yaml:"name" toml:"name"`
	LogShares bool             `yaml:"logShares" toml:"log_shares"`
	Server    ServerConfig     `yaml:"server" toml:"server"`
	Logger    log.Config       `yaml:"logger" toml:"logger"`
	Probes    probes.Config    `yaml:"probes" toml:"probes"`
	Telemetry telemetry.Config `yaml:"telemetry" toml:"telemetry"`
}

type ServerConfig struct {
	BindAddr string          `yaml:"bindAddr" toml:"bindAddr"`
	Port     uint16          `yaml:"port" toml:"port"`
	UDP      UDPServerConfig `yaml:"udp" toml:"udp"`
}

type UDPServerConfig struct {
	BindAddr string `yaml:"bindAddr" toml:"bindAddr"`
	Port     uint16 `yaml:"port" toml:"port"`
}
