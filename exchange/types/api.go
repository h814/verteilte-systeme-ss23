package types

type ApiShareResponse struct {
	Code uint16
	Data []Share
}
