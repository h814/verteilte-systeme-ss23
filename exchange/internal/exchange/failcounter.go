package exchange

import (
	"sync"
	"sync/atomic"
)

type FailCounter struct {
	lock sync.Mutex
	cMap map[string]*atomic.Uint32
}

func NewFailCounter() FailCounter {
	return FailCounter{
		cMap: make(map[string]*atomic.Uint32, 0),
	}
}

func (fc *FailCounter) Inc(key string) uint32 {
	val := fc.getAndNewIfNotSet(key).Add(1)
	return val
}

func (fc *FailCounter) Remove(key string) {
	fc.lock.Lock()
	delete(fc.cMap, key)
	fc.lock.Unlock()
}

func (fc *FailCounter) getAndNewIfNotSet(key string) *atomic.Uint32 {
	fc.lock.Lock()
	defer fc.lock.Unlock()
	val, ok := fc.cMap[key]

	if !ok {
		val = &atomic.Uint32{}
		fc.cMap[key] = val
	}

	return val
}

func (fc *FailCounter) Get(key string) uint32 {
	return fc.getAndNewIfNotSet(key).Load()
}
