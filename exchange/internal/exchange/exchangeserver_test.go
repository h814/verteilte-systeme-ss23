package exchange

import (
	"context"
	"fmt"
	"net"
	"os"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/log"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"github.com/stretchr/testify/assert"
	"go.opentelemetry.io/otel"
	"go.uber.org/zap"
)

var discardLogger *zap.Logger = log.DiscardLogger()

func setup() {
	telemetry.NewTestProvider(discardLogger)
}

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}

func TestInitUDP(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	l, _ := zap.NewDevelopment()

	server := &ExchangeServer{
		exchange:        types.Exchange{Shares: util.NewConcurrentMap[string, types.Share]()},
		updateShareChan: make(chan UpdateShare, chanSize),
		logger:          l,
		logShares:       false,
		clientFail:      NewFailCounter(),
		clients:         make(map[string]types.Client, 0),
	}

	udpPort := 53034

	t.Run("should err on listen error", func(t *testing.T) {
		t.Parallel()
		conn, err := net.ListenUDP("udp", &net.UDPAddr{IP: nil, Port: int(8080)})
		assert.Nil(err)
		defer conn.Close()
		err = server.initUDPServer(types.UDPServerConfig{BindAddr: "", Port: 8080})
		assert.NotNil(err)
	})

	err := server.initUDPServer(types.UDPServerConfig{BindAddr: "", Port: uint16(udpPort)})
	assert.Nil(err)

	t.Run("Nothing should happen if msg len == 0", func(t *testing.T) {
		t.Parallel()
		conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: nil, Port: udpPort})
		assert.Nil(err, "Failed to dial")
		defer conn.Close()

		i, err := conn.Write(nil)
		assert.Nil(err, "Failed to write to conn")
		assert.Equal(0, i, "Wrote more than 0 bytes")
	})

	t.Run("Nothing should happen if msg not contain valid prefix", func(t *testing.T) {
		t.Parallel()
		conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: nil, Port: udpPort})
		assert.Nil(err, "Failed to dial")
		defer conn.Close()

		text := "Not meaningful messages prefix"

		i, err := conn.Write([]byte("Not meaningful messages prefix"))
		assert.Nil(err, "Failed to write to conn")
		assert.Equal(len(text), i, "Wrote wrong count of message")

		conn.SetReadDeadline(time.Now().Add(4 * time.Second))
		for {
			udpMessageRaw := make([]byte, 512)
			readBytes, err := conn.Read(udpMessageRaw)
			assert.NotNil(err)

			if err != nil {
				break
			}

			assert.Equal(0, readBytes, "Read an unexpected message %d", readBytes)
			break
		}
	})

	propagator := otel.GetTextMapPropagator()
	udpCarrier := telemetry.NewUDPCarrier()
	udpCarrier.Set("traceparent", "700-87be396ee69480f3ae6ee99724ac96ed-e08b3cdd7a25ce04-01")
	propagator.Inject(context.Background(), udpCarrier)
	udpCarrierData, err := udpCarrier.Pack()
	assert.Nil(err, "Failed to pack udp carrier dumm data")

	t.Run("Should pong on ping", func(t *testing.T) {
		t.Parallel()
		conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: nil, Port: udpPort})
		assert.Nil(err, "Failed to dial")
		defer conn.Close()

		host, port, err := net.SplitHostPort(conn.LocalAddr().String())
		message := "PING_EX;HOST:" + host + ";PORT:" + port + ";" + string(udpCarrierData)

		wg := sync.WaitGroup{}
		wg.Add(1)

		go func() {
			defer wg.Done()

			conn.SetReadDeadline(time.Now().Add(4 * time.Second))
			for {
				udpMessageRaw := make([]byte, 512)
				readBytes, err := conn.Read(udpMessageRaw)
				assert.Nil(err, "Failed to read from udp: %v", err)

				if err != nil {
					break
				}

				if readBytes <= 0 {
					continue
				}

				assert.Equal("PONG", strings.TrimSpace(string(udpMessageRaw[:readBytes])), "Wrong answer received")
				break

			}
		}()

		i, err := conn.Write([]byte(message))
		assert.Nil(err, "Failed to write to conn")
		assert.Equal(len(message), i, "Wrote wrong count of message")

		wg.Wait()
	})

	t.Run("Should Quit and remove client on quit (validate server ack)", func(t *testing.T) {
		t.Parallel()
		t.Run("should remove successful", func(t *testing.T) {
			t.Parallel()
			conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: nil, Port: udpPort})
			assert.Nil(err, "Failed to dial")
			defer conn.Close()
			host, port, err := net.SplitHostPort(conn.LocalAddr().String())
			assert.Nil(err)
			portParsed, err := strconv.Atoi(port)
			assert.Nil(err)

			server.clientLock.Lock()
			server.clients[fmt.Sprintf("%s:%s", host, port)] = types.Client{IP: net.ParseIP(host), Port: portParsed}
			server.clientLock.Unlock()
			message := "QUIT_EX;HOST:" + host + ";PORT:" + port + ";" + string(udpCarrierData)

			wg := sync.WaitGroup{}
			wg.Add(1)

			go func() {
				defer wg.Done()

				conn.SetReadDeadline(time.Now().Add(4 * time.Second))
				for {
					udpMessageRaw := make([]byte, 512)
					readBytes, err := conn.Read(udpMessageRaw)
					assert.Nil(err, "Failed to read from udp: %v", err)

					if err != nil {
						break
					}

					if readBytes <= 0 {
						continue
					}

					assert.Equal("SUCCESS", strings.TrimSpace(string(udpMessageRaw[:readBytes])), "Wrong answer received")
					break
				}
			}()

			i, err := conn.Write([]byte(message))
			assert.Nil(err, "Failed to write to conn")
			assert.Equal(len(message), i, "Wrote wrong count of message")

			wg.Wait()

			key := fmt.Sprintf("%s:%s", host, port)

			server.clientLock.RLock()
			_, ok := server.clients[key]
			server.clientLock.RUnlock()
			assert.False(ok, "Client was not removed")
		})

		t.Run("Should not remove on unproper protocol", func(t *testing.T) {
			t.Parallel()
			conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: nil, Port: udpPort})
			assert.Nil(err, "Failed to dial")
			defer conn.Close()
			host, port, err := net.SplitHostPort(conn.LocalAddr().String())
			assert.Nil(err)
			portParsed, err := strconv.Atoi(port)
			assert.Nil(err)

			server.clientLock.Lock()
			server.clients[fmt.Sprintf("%s:%s", host, port)] = types.Client{IP: net.ParseIP(host), Port: portParsed}
			server.clientLock.Unlock()
			message := "QUIT_EX;" + host + ";" + port + ";" + string(udpCarrierData)

			wg := sync.WaitGroup{}
			wg.Add(1)

			go func() {
				defer wg.Done()

				conn.SetReadDeadline(time.Now().Add(4 * time.Second))
				for {
					udpMessageRaw := make([]byte, 512)
					readBytes, err := conn.Read(udpMessageRaw)
					assert.NotNil(err)

					if err != nil {
						break
					}

					assert.Equal(0, readBytes, "Unexpected read message")
				}
			}()

			i, err := conn.Write([]byte(message))
			assert.Nil(err, "Failed to write to conn")
			assert.Equal(len(message), i, "Wrote wrong count of message")

			wg.Wait()

			key := fmt.Sprintf("%s:%s", host, port)

			server.clientLock.RLock()
			_, ok := server.clients[key]
			server.clientLock.RUnlock()
			assert.True(ok, "Client was falsy removed")
		})
	})

	t.Run("Should Join and add client on join (validate server ack)", func(t *testing.T) {
		t.Parallel()
		t.Run("should add successful", func(t *testing.T) {
			t.Parallel()
			conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: nil, Port: udpPort})
			assert.Nil(err, "Failed to dial")
			defer conn.Close()
			host, port, err := net.SplitHostPort(conn.LocalAddr().String())
			assert.Nil(err)
			portParsed, err := strconv.Atoi(port)
			assert.Nil(err)

			message := "JOIN_EX;HOST:" + host + ";PORT:" + port + ";" + string(udpCarrierData)

			wg := sync.WaitGroup{}
			wg.Add(1)

			go func() {
				defer wg.Done()

				conn.SetReadDeadline(time.Now().Add(4 * time.Second))
				for {
					udpMessageRaw := make([]byte, 512)
					readBytes, err := conn.Read(udpMessageRaw)
					assert.Nil(err, "Failed to read from udp: %v", err)

					if err != nil {
						break
					}

					if readBytes <= 0 {
						continue
					}

					assert.Equal("SUCCESS", strings.TrimSpace(string(udpMessageRaw[:readBytes])), "Wrong answer received")
					break
				}
			}()

			i, err := conn.Write([]byte(message))
			assert.Nil(err, "Failed to write to conn")
			assert.Equal(len(message), i, "Wrote wrong count of message")

			wg.Wait()

			key := fmt.Sprintf("%s:%s", host, port)

			server.clientLock.RLock()
			c, ok := server.clients[key]
			server.clientLock.RUnlock()
			assert.True(ok, "Client was not added")
			assert.Equal(host, c.IP.String(), "Got wrong ip")
			assert.Equal(portParsed, c.Port, "Got wrong port")
		})

		t.Run("Should not add on unproper protocol", func(t *testing.T) {
			t.Parallel()
			conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: nil, Port: udpPort})
			assert.Nil(err, "Failed to dial")
			defer conn.Close()
			host, port, err := net.SplitHostPort(conn.LocalAddr().String())
			assert.Nil(err)

			message := "JOIN_EX;" + host + ";" + port + ";" + string(udpCarrierData)

			wg := sync.WaitGroup{}
			wg.Add(1)

			go func() {
				defer wg.Done()

				conn.SetReadDeadline(time.Now().Add(4 * time.Second))
				for {
					udpMessageRaw := make([]byte, 512)
					readBytes, err := conn.Read(udpMessageRaw)
					assert.NotNil(err)

					if err != nil {
						break
					}

					assert.Equal(0, readBytes, "Unexpected read message")
				}
			}()

			i, err := conn.Write([]byte(message))
			assert.Nil(err, "Failed to write to conn")
			assert.Equal(len(message), i, "Wrote wrong count of message")

			wg.Wait()

			key := fmt.Sprintf("%s:%s", host, port)

			server.clientLock.RLock()
			_, ok := server.clients[key]
			server.clientLock.RUnlock()
			assert.False(ok, "Client was falsy added")
		})
	})
}

func TestValueDistributor(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	l, _ := zap.NewDevelopment()

	udpPort := 53035
	mockClientConn, err := net.ListenUDP("udp", &net.UDPAddr{Port: udpPort})
	assert.Nil(err)
	defer mockClientConn.Close()
	host, port, err := net.SplitHostPort(mockClientConn.LocalAddr().String())
	assert.Nil(err)
	key := fmt.Sprintf("%s:%s", host, port)

	server := &ExchangeServer{
		exchange:        types.Exchange{Shares: util.NewConcurrentMap[string, types.Share]()},
		updateShareChan: make(chan UpdateShare, chanSize),
		logger:          l,
		logShares:       false,
		clientFail:      NewFailCounter(),
		clients: map[string]types.Client{
			key: {IP: net.ParseIP(host), Port: udpPort},
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go server.middleman(ctx)

	testValues := []UpdateShare{
		{Ctx: context.Background(), Share: types.Share{Name: "Test", Code: "TST", Value: 23.0}},
		{Ctx: context.Background(), Share: types.Share{Name: "Test 1", Code: "TST1", Value: 28.23}},
		{Ctx: context.Background(), Share: types.Share{Name: "Test 2", Code: "TST2", Value: 21233.0}},
		{Ctx: context.Background(), Share: types.Share{Name: "Test 3", Code: "TST3", Value: 424.2343}},
	}

	for _, v := range testValues {
		wg := sync.WaitGroup{}
		wg.Add(1)

		go func() {
			defer wg.Done()

			mockClientConn.SetReadDeadline(time.Now().Add(4 * time.Second))
			for {
				udpMessageRaw := make([]byte, 512)
				readBytes, remote, err := mockClientConn.ReadFromUDP(udpMessageRaw)
				assert.Nil(err, "UDP read error: %v", err)

				if err != nil {
					break
				}

				if readBytes <= 0 {
					continue
				}

				msg := strings.TrimSpace(string(udpMessageRaw[:readBytes]))
				assert.True(strings.HasPrefix(msg, SHARE_VALUE_PREFIX), "Not a valid share change")
				msg = strings.TrimPrefix(msg, SHARE_VALUE_PREFIX)
				dataS := strings.Split(msg, ";")
				assert.Len(dataS, 4)
				assert.Equal(v.Share.Code, dataS[1], "Code not equal")

				value, err := strconv.ParseFloat(dataS[2], 64)
				assert.Nil(err)

				assert.Equal(v.Share.Value, value, "Value not equal")
				mockClientConn.WriteToUDP([]byte("ACK"), remote)
				break
			}
		}()

		server.updateShareChan <- v
		wg.Wait()
	}
}
