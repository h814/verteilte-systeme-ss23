package exchange

import (
	"context"
	"fmt"
	"math/rand"
	"net"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

const (
	JOIN_PREFIX        = "JOIN_EX;"
	PING_PREFIX        = "PING_EX;"
	QUIT_PREFIX        = "QUIT_EX;"
	HOST_PREFIX        = "HOST:"
	PORT_PREFIX        = "PORT:"
	SHARE_VALUE_PREFIX = "SHARE_VAL;"
	MAX_CLIENT_FAIL    = 4
	chanSize           = 10
)

type UpdateShare struct {
	Share types.Share
	Ctx   context.Context
}

type ExchangeServer struct {
	exchange        types.Exchange
	updateShareChan chan UpdateShare
	logger          *zap.Logger
	logShares       bool
	server          *http.Server
	conn            *net.UDPConn
	clientLock      sync.RWMutex
	clients         map[string]types.Client
	cancelMiddleman context.CancelFunc
	clientFail      FailCounter
}

func New(logger *zap.Logger, ex types.Exchange, config types.ServerConfig, logShares bool) (*ExchangeServer, context.CancelFunc, error) {
	server := &ExchangeServer{
		exchange:        ex,
		updateShareChan: make(chan UpdateShare, chanSize),
		logger:          logger.Named("ex-server"),
		logShares:       logShares,
		clientFail:      NewFailCounter(),
		clients:         make(map[string]types.Client, 0),
	}

	if config.Port == 0 {
		server.logger.Fatal("The provided http server port is not possible or was not set")
	}

	if config.UDP.Port == 0 {
		server.logger.Fatal("The provided udp server port is not possible or was not set")
	}

	server.exchange.Shares.SetOverride(true)

	ctx, cancel := context.WithCancel(context.Background())
	server.cancelMiddleman = cancel

	server.initHttpServer(config)
	err := server.initUDPServer(config.UDP)
	if err != nil {
		return nil, nil, err
	}

	for i := 0; i < chanSize; i++ {
		go server.middleman(ctx)
	}

	return server, cancel, nil
}

func (es *ExchangeServer) initHttpServer(config types.ServerConfig) {
	mux := http.NewServeMux()

	mux.HandleFunc("/api/share", es.handleHttpGetShares)

	es.server = &http.Server{
		Addr:    fmt.Sprintf("%s:%d", config.BindAddr, config.Port),
		Handler: mux,
	}
}

func (es *ExchangeServer) initUDPServer(config types.UDPServerConfig) error {
	es.logger.Info("Starting udp server", zap.String("bindAddr", config.BindAddr), zap.Uint16("port", config.Port))
	conn, err := net.ListenUDP("udp", &net.UDPAddr{IP: net.ParseIP(config.BindAddr), Port: int(config.Port)})
	if err != nil {
		return fmt.Errorf("UDP server failed to listen: %w", err)
	}

	es.conn = conn

	// Handle connection and message loop
	go func() {
		propagator := otel.GetTextMapPropagator()
	main_loop:
		for {
			udpMessageRaw := make([]byte, 128)
			readBytes, remote, err := conn.ReadFromUDP(udpMessageRaw)
			if err != nil {
				es.logger.Error("Failed to read UDP message", zap.Error(err))
				continue
			}
			es.logger.Debug("New UDP message for exchange server", zap.Any("remote", remote), zap.ByteString("data", udpMessageRaw[:readBytes]))

			if readBytes == 0 {
				continue
			}

			var rHost string
			var rPort int
			data := strings.TrimSpace(string(udpMessageRaw[:readBytes]))

			if !strings.HasPrefix(data, JOIN_PREFIX) && !strings.HasPrefix(data, QUIT_PREFIX) && !strings.HasPrefix(data, PING_PREFIX) {
				continue
			}

			isQuit := false
			isPing := false

			if strings.HasPrefix(data, QUIT_PREFIX) {
				isQuit = true
				data = strings.TrimPrefix(data, QUIT_PREFIX)
			}

			if strings.HasPrefix(data, PING_PREFIX) {
				isPing = true
				data = strings.TrimPrefix(data, PING_PREFIX)
			}

			data = strings.TrimPrefix(data, JOIN_PREFIX)

			dataS := strings.Split(data, ";")

			if len(dataS) != 3 {
				es.logger.Error("Received false formatted message", zap.Int("length", len(dataS)))
				continue
			}

			udpCarrier := telemetry.NewUDPCarrier()
			err = udpCarrier.Unpack([]byte(dataS[len(dataS)-1]))
			if err != nil {
				es.logger.Error("Failed to unpack udp carrier", zap.Error(err))
				continue
			}
			ctx := context.Background()
			ctx = propagator.Extract(ctx, udpCarrier)
			ctx, span := internal.Tracer().Start(ctx, "ex-client-op")

			for i, d := range dataS {
				dataS[i] = strings.TrimSpace(d)

				if strings.HasPrefix(dataS[i], HOST_PREFIX) {
					parsed := strings.TrimPrefix(dataS[i], HOST_PREFIX)
					rHost = parsed
				} else if strings.HasPrefix(dataS[i], PORT_PREFIX) {
					parsed, err := strconv.Atoi(strings.TrimPrefix(dataS[i], PORT_PREFIX))
					if err != nil {
						es.logger.Error("Failed to parse port", zap.Error(err))
						span.End()
						continue main_loop
					}
					rPort = parsed
				}
			}

			if rHost == "" {
				es.logger.Error("No host found")
				continue
			}

			if rPort == 0 {
				es.logger.Error("No port found")
				continue
			}

			addr, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%s:%d", rHost, rPort))
			if err != nil {
				es.logger.Error("Failed to resolve UDP Addr", zap.Error(err), zap.String("host", rHost), zap.Int("port", rPort))
			}

			client := types.Client{
				IP:   addr.IP,
				Port: addr.Port,
			}

			key := fmt.Sprintf("%s:%d", addr.IP.String(), addr.Port)

			if isPing {
				_, err = conn.WriteToUDP([]byte("PONG"), remote)
				if err != nil {
					es.logger.Error("Failed to react to client ping", zap.Any("client", client), zap.Error(err))
				} else {
					es.logger.Info("Got client ping", zap.Any("client", client))
				}
			} else {
				es.clientLock.Lock()
				if !isQuit {
					es.clients[key] = client
					es.logger.Info("Added new bank client", zap.Any("client", client), zap.Int("clientsLength", len(es.clients)))
				} else {
					delete(es.clients, key)
					es.logger.Info("Removed bank client", zap.Any("client", client), zap.Int("clientsLength", len(es.clients)))
				}
				es.clientLock.Unlock()

				conn.WriteToUDP([]byte("SUCCESS"), remote)

				if !isQuit {
					udpCarrier = telemetry.NewUDPCarrier()
					propagator.Inject(ctx, udpCarrier)

					udpCarrierPacked, err := udpCarrier.Pack()
					if err != nil {
						es.logger.Error("Failed to pack udp carrier", zap.Error(err))
					}

					for _, share := range es.exchange.Shares.GetMap() {
						conn.WriteToUDP([]byte(fmt.Sprintf("%s%d;%s;%f;%s;%s", SHARE_VALUE_PREFIX, 0, share.Code, share.Value, share.Name, string(udpCarrierPacked))), addr)
					}
				}
			}

			span.End()
		}
	}()

	return nil
}

func (es *ExchangeServer) ListenAndServe() {
	host, port, err := net.SplitHostPort(es.server.Addr)
	if err != nil {
		es.logger.Error("Failed to split host and port", zap.Error(err))
	}

	go func() {
		es.logger.Info("Starting exchange server", zap.String("port", port), zap.String("host", host))
		err := es.server.ListenAndServe()
		if err != nil {
			es.logger.Fatal("Failed to start server")
		}
	}()
}

func (es *ExchangeServer) Shutdown(ctx context.Context) error {
	es.cancelMiddleman()
	return es.server.Shutdown(ctx)
}

func (es *ExchangeServer) UpdateChan() chan<- UpdateShare {
	return es.updateShareChan
}

func (es *ExchangeServer) Shares() *util.ConcurrentMap[string, types.Share] {
	return es.exchange.Shares
}

func (es *ExchangeServer) middleman(mmCtx context.Context) {
	propagator := otel.GetTextMapPropagator()

	for {
		select {
		case updatedShare := <-es.updateShareChan:
			share := updatedShare.Share
			ctx := updatedShare.Ctx

			if share.Code == "" {
				continue
			}

			ctx, span := internal.Tracer().Start(ctx, "shareValueDistributor", trace.WithSpanKind(trace.SpanKindProducer))

			span.SetAttributes(attribute.String("share-code", share.Code), attribute.Float64("share-value", share.Value))

			if es.logShares {
				es.logger.Debug("Updated share", zap.String("code", share.Code), zap.Float64("value", share.Value))
			}

			es.exchange.Shares.Set(share.Code, share)

			es.clientLock.RLock()
			clients := es.clients
			es.clientLock.RUnlock()

			for _, client := range clients {
				span.AddEvent("Iterating client")
				go func(ctx context.Context, client types.Client, fc *FailCounter) {
					ctx, span := internal.Tracer().Start(ctx, "shareValueDistributor-client-pusher")
					span.SetAttributes(attribute.String("client-ip", client.IP.String()), attribute.Int("client-port", int(client.Port)))
					defer span.End()

					conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: client.IP, Port: client.Port})
					if err != nil {
						span.SetStatus(codes.Error, "failed to dial client")
						span.RecordError(err)
						es.logger.Error("Failed to dial client", zap.Error(err), zap.Any("client", client))
						return
					}

					udpCarrier := telemetry.NewUDPCarrier()
					propagator.Inject(ctx, udpCarrier)

					data, err := udpCarrier.Pack()
					if err != nil {
						span.SetStatus(codes.Error, "failed to pack tracing data")
						span.RecordError(err)
						es.logger.Error("Failed to pack udp carrier", zap.Error(err))
					}

					conn.Write([]byte(fmt.Sprintf("%s%d;%s;%f;%s", SHARE_VALUE_PREFIX, rand.Intn(20 - -20)+-20, share.Code, share.Value, string(data))))
					conn.SetReadDeadline(time.Now().Add(15 * time.Second))

					clientKey := fmt.Sprintf("%s:%d", client.IP.String(), client.Port)

					for {
						message := make([]byte, 32)
						n, err := conn.Read(message)
						if err != nil {
							concreteClientFail := fc.Inc(clientKey)
							es.logger.Error("Failed to read ack", zap.Error(err), zap.Any("client", client))
							span.RecordError(fmt.Errorf("failed to read ack: %w", err))

							if concreteClientFail > MAX_CLIENT_FAIL {
								es.logger.Info("Client not reachable to much ACK dropped. Removing it", zap.Any("client", client))
								span.RecordError(fmt.Errorf("client not reachable. Too many failures"))
								span.SetStatus(codes.Error, "Client not reachable to much ACK dropped")
								span.AddEvent("Removing client")
								es.clientLock.Lock()
								delete(es.clients, clientKey)
								es.clientLock.Unlock()
								fc.Remove(clientKey)
								span.AddEvent("Removing client done")
							}

							return
						}

						if strings.TrimSpace(string(message[:n])) == "ACK" {
							span.AddEvent("Received ack")
							return
						}
					}
				}(ctx, client, &es.clientFail)
			}
			span.End()
		case <-mmCtx.Done():
			return
		}
	}
}
