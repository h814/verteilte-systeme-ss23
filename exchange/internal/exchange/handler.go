package exchange

import (
	"encoding/json"
	"net/http"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/types"
)

func (es *ExchangeServer) handleHttpGetShares(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	resp := types.ApiShareResponse{Code: 200, Data: make([]types.Share, 0)}

	byCode := r.URL.Query().Get("code")

	if byCode != "" {
		data := es.exchange.Shares.Get(byCode)

		if data.Code == "" {
			w.WriteHeader(http.StatusNotFound)
			return
		}

		resp.Data = append(resp.Data, data)
	} else {
		data := es.exchange.Shares.GetMap()

		for _, v := range data {
			resp.Data = append(resp.Data, v)
		}
	}

	w.Header().Add("Content-Type", "application/json")

	enc := json.NewEncoder(w)
	_ = enc.Encode(resp)
}
