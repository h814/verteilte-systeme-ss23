package main

import (
	"context"
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/internal/exchange"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/log"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/probes"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

func main() {
	configFile := flag.String("configFile", "", "The config file path for the service")
	shareFile := flag.String("shareFile", "", "The file path for the share names")
	flag.Parse()

	if *configFile == "" {
		panic("No config file provided")
	}

	if *shareFile == "" {
		panic("No share file provided")
	}

	cfg, err := util.ParseConfig[types.Config](*configFile)
	if err != nil {
		panic(err)
	}

	logger, err := log.New(cfg.Logger)
	if err != nil {
		panic(err)
	}

	logger.Debug("Loaded configuration", zap.Any("config", cfg))

	probeServer := probes.New(cfg.Probes, logger)
	probeServer.SetStartup(true)

	probeServer.ListenAndServe()

	tel, err := internal.InitTelemetry(logger, cfg.Telemetry, cfg.Name, "0")
	if err != nil {
		logger.Error("Failed to init telemetry", zap.Error(err))
	}
	fmt.Printf("tel: %v\n", tel)

	shares, err := createInitialExchangeShares(logger, *shareFile)
	logger.Debug("Created initial share list", zap.Any("shares", shares))
	if err != nil {
		logger.Fatal("Failed to create initial shares", zap.Error(err))
	}

	ex := types.Exchange{
		Shares: util.NewConcurrentMapFromRaw(shares),
	}

	exServer, cancelMiddleman, err := exchange.New(logger, ex, cfg.Server, cfg.LogShares)
	if err != nil {
		logger.Fatal("Failed to create exchange server", zap.Error(err))
	}
	fmt.Printf("cancelMiddleman: %v\n", cancelMiddleman)

	quitValueTicker := make(chan struct{})
	valueChanger(logger, quitValueTicker, exServer)

	exServer.ListenAndServe()

	probeServer.SetLiveOrReady(true)

	// Wait for CTRL+C to quit
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	sig := <-c

	logger.Info("Shutting down", zap.String("signal", sig.String()))
	exServer.Shutdown(context.Background())

	logger.Info("Exiting exchange server application")
}

func createInitialExchangeShares(logger *zap.Logger, initFile string) (map[string]types.Share, error) {
	values, err := readCsvFile(initFile)
	if err != nil {
		return nil, err
	}

	shares := make(map[string]types.Share, len(values))

	for _, value := range values {
		name := value[0]
		code := value[1]

		share := types.Share{
			Name:  name,
			Code:  code,
			Value: generateInitialRndValue(),
		}

		shares[code] = share
	}

	return shares, nil
}

func readCsvFile(filePath string) ([][]string, error) {
	f, err := os.Open(filePath)
	if err != nil {
		return nil, fmt.Errorf("unable to read input file: %w", err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	records, err := csvReader.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("failed to parse csv: %w", err)
	}

	return records, nil
}

func generateInitialRndValue() float64 {
	max := 350.0
	min := 102.0

	return util.RandFloat(min, max)
}

func valueChanger(logger *zap.Logger, quit chan struct{}, exServer *exchange.ExchangeServer) {
	logger = logger.Named("value-changer")
	ticker := time.NewTicker(3 * time.Second)
	maxAllowedRange := 10.0

	go func() {
		for {
			select {
			case <-ticker.C:
				ctx, span := internal.Tracer().Start(context.Background(), "valueChanger", trace.WithSpanKind(trace.SpanKindServer))
				shares := exServer.Shares().GetMap()
				for i := range shares {
					// Don't change this share
					if !util.RandBool() {
						continue
					}

					share := shares[i]

					changeValue := util.RandFloat(0.0, maxAllowedRange)

					if util.RandBool() {
						share.Value += changeValue
					} else {
						share.Value -= changeValue
					}

					exServer.UpdateChan() <- exchange.UpdateShare{Share: share, Ctx: ctx}
				}
				span.End()
			case <-quit:
				ticker.Stop()
				return
			}
		}
	}()
}
