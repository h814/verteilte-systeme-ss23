#!/bin/sh

function log() {
    echo -e "[$(date -Iseconds)] $1"
}

function info() {
    log "INFO $1"
}

info "Mounting the desired paths as shared"

mount --make-shared /
mount --make-shared /run
mount --make-shared /var/lib/rancher/k3s

info "Setting fs inotify"

sysctl -n -w fs.inotify.max_user_instances=512 > /dev/null 2>&1
sysctl -n -w fs.inotify.max_user_watches=524288 > /dev/null 2>&1