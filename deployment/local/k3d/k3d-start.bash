#!/bin/bash

set -e

K3D_FIX_MOUNTS=1

baseDir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P  )"
k3dConfigName="k3d-config.yaml"
k3sImage="rancher/k3s:v1.27.2-k3s1"

export DOCKER_BUILDKIT=1

function log() {
    echo -e "[$(date -Iseconds)] $1"
}

function info() {
    log "\e[32mINFO\e[0m $1"
}

function error() {
    log "\e[31mERROR\e[0m $1"
}


function build_local_k3s() {
    docker build -f $baseDir/Dockerfile.K3s -t custom-$k3sImage $baseDir
}

build_local_k3s

info "Setting up local k3d cluster."
info "Base dir is ${baseDir}"

k3d cluster create --config ${baseDir}/${k3dConfigName} \
        --volume ${baseDir}/manifests:/var/lib/rancher/k3s/server/manifests/locald@server:* \
        --image custom-$k3sImage --kubeconfig-update-default || exit 1