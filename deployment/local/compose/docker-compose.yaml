version: '3.8'

name: vs-praktikum

x-common_service_cfg:
  &common_service_cfg
  labels:
    - "logging=promtail"
  restart: always
  networks:
    - vsnet
  healthcheck:
    test: [ "CMD", "/healthcheck", "-port", "82" ]
    interval: 2s
    timeout: 10s
    retries: 10
    start_period: 5s
  depends_on:
    consul-server:
      condition: service_healthy

x-exchange-image: &exchange_image registry.code.fbi.h-da.de/distributed-systems/praktika/lab-for-distributed-systems-2023-sose/rueden/group_d_5/exchange
x-bank-image: &bank_image registry.code.fbi.h-da.de/distributed-systems/praktika/lab-for-distributed-systems-2023-sose/rueden/group_d_5/bank

services:
  # Collector
  otel-collector:
    image: otel/opentelemetry-collector-contrib:latest
    command: [ "--config=/etc/otel-collector-config.yaml" ]
    volumes:
      - ./../config/otel-collector-config.yaml:/etc/otel-collector-config.yaml
      - /var/run/docker.sock:/var/run/docker.sock:ro
    ports:
      - "1888:1888" # pprof extension
      - "8888:8888" # Prometheus metrics exposed by the collector
      - "8889:8889" # Prometheus exporter metrics
      - "13133:13133" # health_check extension
      - "4317:4317" # OTLP gRPC receiver
      - "4318:4318" # OTLP http receiver
      - "55679:55679" # zpages extension
    networks:
      - vsnet
    depends_on:
      - jaeger
    labels:
      - "logging=promtail"

  jaeger:
    image: jaegertracing/all-in-one:latest
    volumes:
      - vs-jaeger-data:/badger
    networks:
      - vsnet
    ports:
      - 6831:6831/udp
      - 6832:6832/udp
      - 5778:5778
      - 14250:14250
      - 14268:14268
      - 14269:14269
      - 9411:9411
    environment:
      - LOG_LEVEL=info
      - COLLECTOR_OTLP_ENABLED
      - SPAN_STORAGE_TYPE=badger
      - BADGER_EPHEMERAL=true
      - BADGER_DIRECTORY_VALUE=/badger/data
      - BADGER_DIRECTORY_KEY=/badger/key
      - METRICS_STORAGE_TYPE=prometheus
      - PROMETHEUS_SERVER_URL=http://prometheus:9090
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.jaeger.tls=false"
      - "traefik.http.routers.jaeger.entrypoints=web"
      - "traefik.http.routers.jaeger.rule=Host(`jaeger.vs.localhost`)"
      - "traefik.http.services.jaeger.loadbalancer.server.port=16686"
      - "logging=promtail"

  prometheus:
    networks:
      - vsnet
    image: prom/prometheus:latest
    volumes:
      - "./../config/prometheus.yml:/etc/prometheus/prometheus.yml"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.prometheus.tls=false"
      - "traefik.http.routers.prometheus.entrypoints=web"
      - "traefik.http.routers.prometheus.rule=Host(`prometheus.vs.localhost`)"
      - "traefik.http.services.prometheus.loadbalancer.server.port=9090"
      - "logging=promtail"

  promtail:
    image: grafana/promtail:2.8.0
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./../config/promtail-config.yaml:/etc/promtail/config.yml
    command: -config.file=/etc/promtail/config.yml
    labels:
      - "logging=promtail"
    networks:
      - vsnet

  loki:
    image: grafana/loki:2.8.0
    volumes:
      - vs-loki-data:/loki
      - ./../config/loki-config.yaml:/etc/loki/local-config.yaml
    command: -config.file=/etc/loki/local-config.yaml
    ports:
      - "3100:3100"
    networks:
      - vsnet
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.loki.tls=false"
      - "traefik.http.routers.loki.entrypoints=web"
      - "traefik.http.routers.loki.rule=Host(`loki.vs.localhost`)"
      - "traefik.http.services.loki.loadbalancer.server.port=3100"
      - "logging=promtail"

  grafana:
    environment:
      GF_AUTH_ANONYMOUS_ENABLED: "true"
      GF_AUTH_DISABLE_LOGIN_FORM: "true"
      GF_AUTH_ANONYMOUS_ORG_ROLE: "Admin"
    entrypoint:
      - sh
      - -euc
      - |
        mkdir -p /etc/grafana/provisioning/datasources
        cat <<EOF > /etc/grafana/provisioning/datasources/ds.yaml
        apiVersion: 1
        datasources:
        - name: Loki
          type: loki
          access: proxy 
          orgId: 1
          url: http://loki:3100
          basicAuth: false
          version: 1
          editable: false
        - name: Jaeger
          type: jaeger
          url: http://jaeger:16686
          editable: true
        - name: Prometheus
          type: prometheus
          url: http://prometheus:9090
          isDefault: true
          access: proxy
          editable: true
        EOF
        /run.sh
    image: grafana/grafana:latest
    networks:
      - vsnet
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.grafana.tls=false"
      - "traefik.http.routers.grafana.entrypoints=web"
      - "traefik.http.routers.grafana.rule=Host(`grafana.vs.localhost`)"
      - "traefik.http.services.grafana.loadbalancer.server.port=3000"
      - "logging=promtail"
    volumes:
      - vs-grafana-data:/var/lib/grafana

  registry:
    image: registry:2
    networks:
      - vsnet
    environment:
      REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY: /data
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.registry.tls=false"
      - "traefik.http.routers.registry.entrypoints=web"
      - "traefik.http.routers.registry.rule=Host(`registry.vs.localhost`)"
      - "traefik.http.services.registry.loadbalancer.server.port=5000"
      - "logging=promtail"
    volumes:
      - vs-registry-data:/data

  traefik:
    image: traefik:v2.9
    ports:
      - 80:80
      - 443:443
    networks:
      - vsnet
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./../config/traefik/traefik.toml:/traefik.toml
      - ./../config/traefik/traefik_dynamic.toml:/traefik_dynamic.toml
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.api.tls=false"
      - "traefik.http.routers.api.rule=Host(`traefik.vs.localhost`) && PathPrefix(`/dashboard`, `/api`)"
      - "traefik.http.routers.api.entrypoints=web"
      - "traefik.http.routers.api.service=api@internal"
      - "logging=promtail"

  consul-server:
    image: hashicorp/consul:1.15.2
    volumes:
      - vs-consul-data:/consul/data
      - ./../config/consul-server.json:/consul/config/server.json:ro
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.consul.tls=false"
      - "traefik.http.routers.consul.entrypoints=web"
      - "traefik.http.routers.consul.rule=Host(`consul.vs.localhost`)"
      - "traefik.http.services.consul.loadbalancer.server.port=8500"
      - "logging=promtail"
    networks:
      - vsnet
    ports:
      - "8500:8500"
      - "8600:8600/tcp"
      - "8600:8600/udp"
    command: "agent -bootstrap-expect=1"
    extra_hosts:
      - "host.docker.internal:host-gateway"
    healthcheck:
      test:
        [
          "CMD",
          "curl",
          "-X",
          "GET",
          "localhost:8500/v1/status/leader"
        ]
      interval: 3s
      timeout: 15s
      retries: 10
      start_period: 10s

  kafka-broker-0:
    image: confluentinc/cp-kafka:7.3.3
    hostname: kafka-broker-0
    ports:
      - "9092:9092"
      - "9101:9101"
    environment:
      KAFKA_BROKER_ID: 1
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: 'CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT'
      KAFKA_ADVERTISED_LISTENERS: 'PLAINTEXT://kafka-broker-0:29092,PLAINTEXT_HOST://localhost:9092'
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS: 0
      KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 1
      KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 1
      KAFKA_JMX_PORT: 9101
      KAFKA_JMX_OPTS: -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=kafka-broker-0 -Dcom.sun.management.jmxremote.rmi.port=9101
      KAFKA_PROCESS_ROLES: 'broker,controller'
      KAFKA_NODE_ID: 1
      KAFKA_CONTROLLER_QUORUM_VOTERS: '1@kafka-broker-0:29093'
      KAFKA_LISTENERS: 'PLAINTEXT://kafka-broker-0:29092,CONTROLLER://kafka-broker-0:29093,PLAINTEXT_HOST://0.0.0.0:9092'
      KAFKA_INTER_BROKER_LISTENER_NAME: 'PLAINTEXT'
      KAFKA_CONTROLLER_LISTENER_NAMES: 'CONTROLLER'
      KAFKA_LOG_DIRS: '/tmp/kraft-combined-logs'
      KAFKA_AUTO_CREATE_TOPICS_ENABLE: true
      KAKFA_DELETE_TOPIC_ENABLE: true
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.kafka-broker-0.tls=false"
      - "traefik.http.routers.kafka-broker-0.entrypoints=web"
      - "traefik.http.routers.kafka-broker-0.rule=Host(`kafka-b-0.vs.localhost`)"
      - "traefik.http.services.kafka-broker-0.loadbalancer.server.port=9092"
      - "logging=promtail"
    networks:
      - vsnet
    volumes:
      - ./update-kafka-raft-run.sh:/tmp/update_run.sh
    command: "bash -c 'if [ ! -f /tmp/update_run.sh ]; then echo \"ERROR: Did you forget the update_run.sh file that came with this docker-compose.yml file?\" && exit 1 ; else /tmp/update_run.sh && /etc/confluent/docker/run ; fi'"
    healthcheck:
      test: nc -z localhost 9092 || exit -1
      start_period: 15s
      interval: 5s
      timeout: 10s
      retries: 10

  kafka-ui:
    container_name: awp-compose-kafka-ui
    image: provectuslabs/kafka-ui:latest
    depends_on:
      kafka-broker-0:
        condition: service_healthy
    environment:
      DYNAMIC_CONFIG_ENABLED: true
      LOGGING_LEVEL_COM_PROVECTUS: "info"
      KAFKA_CLUSTERS_0_NAME: docker-local
      KAFKA_CLUSTERS_0_BOOTSTRAPSERVERS: kafka-broker-0:29092
      KAFKA_CLUSTERS_0_METRICS_PORT: 9101
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.kafka-ui.tls=false"
      - "traefik.http.routers.kafka-ui.entrypoints=web"
      - "traefik.http.routers.kafka-ui.rule=Host(`kafka-ui.vs.localhost`)"
      - "traefik.http.services.kafka-ui.loadbalancer.server.port=8080"
      - "logging=promtail"
    networks:
      - vsnet

  #################################
  #           Exchange            #
  #################################
  liliput-main-exchange:
    image: *exchange_image
    volumes:
      - ./config/exchange/liliput_main_exchange.toml:/etc/config/exchange.toml:ro
      - ./../../../shares.csv:/etc/config/shares.csv:ro
    <<: *common_service_cfg

  lilliput-vr-bank-stock-exchange:
    image: *exchange_image
    volumes:
      - ./config/exchange/vr_bank_liliput_exchange.toml:/etc/config/exchange.toml:ro
      - ./../../../shares.csv:/etc/config/shares.csv:ro
    <<: *common_service_cfg

  #################################
  #             Bank              #
  #################################

  lilliput-central-bank:
    hostname: lilliput-central-bank
    ports:
      - "8083:80"
    image: *bank_image
    volumes:
      - ./config/bank/liliput-central-bank.toml:/etc/config/bank.toml:ro
      - ./../../../profiling:/etc/profiling:rw
    command:
      [
        "-configFile",
        "/etc/config/bank.toml",
        "-mom",
        "-manualConsul",
        "-profile"
      ]
    <<: *common_service_cfg

  lilliput-n64:
    hostname: lilliput-n64
    ports:
      - "8082:80"
    image: *bank_image
    volumes:
      - ./config/bank/liliput-n64.toml:/etc/config/bank.toml:ro
    command:
      [
        "-configFile",
        "/etc/config/bank.toml",
        "-mom",
        "-manualConsul"
      ]
    <<: *common_service_cfg

  lilliput-santander:
    hostname: lilliput-santander
    ports:
      - "8081:80"
    image: *bank_image
    volumes:
      - ./config/bank/liliput-santander.toml:/etc/config/bank.toml:ro
    command:
      [
        "-configFile",
        "/etc/config/bank.toml",
        "-mom",
        "-manualConsul"
      ]
    <<: *common_service_cfg

  lilliput-vr-bank:
    hostname: lilliput-vr-bank
    ports:
      - "8080:80"
    image: *bank_image
    volumes:
      - ./config/bank/liliput-vr.toml:/etc/config/bank.toml:ro
    command:
      [
        "-configFile",
        "/etc/config/bank.toml",
        "-mom",
        "-manualConsul"
      ]
    <<: *common_service_cfg
  #################################
  #              UI               #
  #################################
  # lilliput-bank-frontend:
  #   image: registry.code.fbi.h-da.de/distributed-systems/praktika/lab-for-distributed-systems-2023-sose/rueden/group_d_5/ui
  #   labels:
  #     - "logging=promtail"
  #   networks:
  #     - vsnet

volumes:
  vs-registry-data:
    driver: local
  vs-jaeger-data:
    driver: local
  vs-loki-data:
    driver: local
  vs-grafana-data:
    driver: local
  vs-consul-data:
    driver: local
  vs-kafka-data:
    driver: local

networks:
  vsnet:
    driver: "bridge"
    ipam:
      config:
        - subnet: 10.10.0.0/24
