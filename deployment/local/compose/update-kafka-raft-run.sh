#!/bin/sh

# Docker workaround: Remove check for KAFKA_ZOOKEEPER_CONNECT parameter
sed -i '/KAFKA_ZOOKEEPER_CONNECT/d' /etc/confluent/docker/configure

# Docker workaround: Ignore cub zk-ready
sed -i 's/cub zk-ready/echo ignore zk-ready/' /etc/confluent/docker/ensure

cluster_id=$(kafka-storage random-uuid)
export KAFKA_CLUSTER_ID=$cluster_id

echo "export KAFKA_CLUSTER_ID=$cluster_id" >> /etc/confluent/docker/ensure

# KRaft required step: Format the storage directory with a new cluster ID
echo "kafka-storage format --ignore-formatted --cluster-id=$cluster_id -c /etc/kafka/kafka.properties" >> /etc/confluent/docker/ensure
