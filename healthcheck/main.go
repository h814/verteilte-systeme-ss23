package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
)

func main() {
	host := flag.String("host", "localhost", "The host of the endpoint to check")
	port := flag.Uint("port", 80, "The port of the host of the endpoint to check")
	endpoint := flag.String("endpoint", "/ready", "The path of the endpoint to check")
	flag.Parse()

	_, err := http.Get(fmt.Sprintf("http://%s:%d%s", *host, *port, *endpoint))
	if err != nil {
		os.Exit(1)
	}
}
