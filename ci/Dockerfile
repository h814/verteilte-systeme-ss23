FROM ubuntu:22.04

ENV GOPATH /etc/go
ENV VOLTA_HOME $HOME/.volta
ENV PATH $GOPATH/bin:$VOLTA_HOME/bin:/usr/local/go/bin:$PATH

RUN mkdir -p ${GOPATH} && mkdir /tmp/dl

RUN apt update && apt install -y \
  ca-certificates \
  curl \
  gnupg \
  curl \
  git \
  bash \
  make \
  gcc \
  g++ \
  clang

RUN curl -L https://go.dev/dl/go1.20.3.linux-amd64.tar.gz -o /tmp/dl/go1.20.3.linux-amd64.tar.gz && \
  tar -C /usr/local -xzf /tmp/dl/go1.20.3.linux-amd64.tar.gz

RUN curl -fsSL https://d2lang.com/install.sh | sh -s --

RUN curl https://get.volta.sh | bash && volta install node@18 && volta install yarn@3

RUN install -m 0755 -d /etc/apt/keyrings && \
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
  chmod a+r /etc/apt/keyrings/docker.gpg && \
  echo \
  "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null && \
  apt update && apt install -y docker-ce docker-ce-cli

RUN go install github.com/bazelbuild/bazelisk@latest && ln -s  ${GOPATH}/bin/bazelisk ${GOPATH}/bin/bazel

RUN rm -rf /tmp/dl

WORKDIR /vs-build