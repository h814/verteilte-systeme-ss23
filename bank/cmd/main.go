package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"runtime/pprof"
	"syscall"
	"time"

	netpprof "net/http/pprof"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/glob"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/consul"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/log"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/probes"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/raft"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"github.com/jaevor/go-nanoid"
	"go.uber.org/zap"
)

func main() {
	configFile := flag.String("configFile", "", "The config file path for the service")
	allowExplicitRescueOperations := flag.Bool("allowExplicitRescue", true, "Indicates if explicit rescue operations should be allowed")
	momEnabled := flag.Bool("mom", false, "If MOM rescue should be enabled")
	manualConsulEnabled := flag.Bool("manualConsul", false, "If the manual consul registration should be enabled")
	profilingEnabled := flag.Bool("profile", false, "If the profiling should be enabled")
	profilingFile := flag.Bool("profileFile", false, "If the profiling should be written to a file")
	profilingHTTPPort := flag.Uint("profilePort", 0, "The port for the profiling server")
	profilingHTTPAddr := flag.String("profileAddr", "0.0.0.0", "The bind address for the profiling server")
	flag.Parse()

	glob.MOM_ENABLED = *momEnabled

	if *configFile == "" {
		panic("No config file provided")
	}

	cfg, err := util.ParseConfig[types.Config](*configFile)
	if err != nil {
		panic(err)
	}

	logger, err := log.New(cfg.Logger)
	if err != nil {
		panic(err)
	}

	if allowExplicitRescueOperations != nil {
		cfg.AllowExplicitRescueOperations = *allowExplicitRescueOperations
	}

	logger.Debug("Loaded configuration", zap.Any("config", cfg))

	if glob.MOM_ENABLED {
		logger.Info("MOM is enabled")
	} else {
		logger.Info("MOM is disabled")
	}

	if *profilingEnabled {
		if *profilingFile {
			f, err := os.Create(fmt.Sprintf("/etc/profiling/bank_%s_%s_cpu.prof", cfg.Name, time.Now().Format("2006-01-02T15:04:05Z07:00")))
			if err != nil {
				f.Close()
				logger.Fatal("Failed to create profiling file", zap.Error(err))
			}

			err = pprof.StartCPUProfile(f)
			if err != nil {
				f.Close()
				logger.Fatal("Failed to start profiling", zap.Error(err))
			}
		}

		if *profilingHTTPPort != 0 {
			pprofMux := http.NewServeMux()

			pprofMux.HandleFunc("/debug/pprof/", netpprof.Index)
			pprofMux.HandleFunc("/debug/pprof/cmdline", netpprof.Cmdline)
			pprofMux.HandleFunc("/debug/pprof/profile", netpprof.Profile)
			pprofMux.HandleFunc("/debug/pprof/symbol", netpprof.Symbol)
			pprofMux.HandleFunc("/debug/pprof/trace", netpprof.Trace)

			server := &http.Server{
				Addr:    fmt.Sprintf("%s:%d", *profilingHTTPAddr, *profilingHTTPPort),
				Handler: pprofMux,
			}

			go func() {
				err := server.ListenAndServe()
				if err != nil {
					logger.Error("Failed to start profiling server", zap.Error(err))
				}
			}()
		}
	}

	probeServer := probes.New(cfg.Probes, logger)
	probeServer.SetStartup(true)

	probeServer.ListenAndServe()

	tel, err := internal.InitTelemetry(logger, cfg.Telemetry, cfg.Name, "0")
	if err != nil {
		logger.Error("Failed to init telemetry", zap.Error(err))
	}

	consulClient, err := consul.New(logger, cfg.Consul)
	if err != nil {
		logger.Fatal("Failed to create consul", zap.Error(err))
	}

	nid, err := nanoid.Standard(21)
	if err != nil {
		logger.Fatal("Failed to create nanoid generator", zap.Error(err))
	}

	raft := raft.New(logger, cfg.Name, cfg.Raft)
	err = raft.Start()
	if err != nil {
		logger.Fatal("Failed to start raft", zap.Error(err))
	}

	if *manualConsulEnabled {
		err = consulClient.RegisterService(nid(), cfg.Name, cfg.Probes.Port, cfg.Server.GRPCServer.Port, []string{"bank"})
		if err != nil {
			logger.Fatal("Failed to register service", zap.Error(err))
		}
	}

	server, cancelMiddleman, err := bank.New(cfg.Name, logger, cfg.Server, cfg.ExchangeConfig, cfg.Kafka, raft, cfg.LogShares, cfg.AllowExplicitRescueOperations, probeServer, consulClient)
	if err != nil {
		logger.Fatal("Failed to create server", zap.Error(err))
	}
	defer cancelMiddleman()

	// Set random balance at start up
	val := server.BalanceService.ChangeBalance(context.Background(), util.RandFloat(1000, 20000))
	logger.Info("Set initial random balance", zap.Float64("balance", val))

	server.ListenAndServe()

	probeServer.SetLiveOrReady(true)

	// Wait for CTRL+C, Kill or TERM (Docker) to quit
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTERM)
	sig := <-c

	logger.Info("Shutting down", zap.String("signal", sig.String()))

	if *manualConsulEnabled {
		err = consulClient.DeregisterService()
		if err != nil {
			logger.Fatal("Failed to deregister service from consul", zap.Error(err))
		} else {
			logger.Info("Deregistered from consul")
		}
	}

	raft.Shutdown(context.Background())

	server.Shutdown(context.Background())

	tel.Shutdown(context.Background())

	if *profilingEnabled && *profilingFile {
		pprof.StopCPUProfile()
	}

	logger.Info("Exiting bank server application")
}
