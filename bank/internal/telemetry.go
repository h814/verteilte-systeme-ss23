package internal

import (
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

var tracer trace.Tracer = otel.Tracer("code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/internal")

func Tracer() trace.Tracer {
	return tracer
}

func InitTelemetry(logger *zap.Logger, cfg telemetry.Config, name string, id string) (*telemetry.Telemetry, error) {
	cfg.Name = name
	cfg.ID = id

	tel := telemetry.New(cfg, logger)

	err := tel.Init(15 * time.Second)

	tracer = tel.Provider.Tracer(cfg.Name)

	return tel, err
}
