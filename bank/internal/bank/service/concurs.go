package service

import (
	"context"
	"sync/atomic"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"go.opentelemetry.io/otel/attribute"
	"go.uber.org/zap"
)

var _ Service = &ConcursService{}

type ConcursService struct {
	logger *zap.Logger

	isConcurs atomic.Bool
}

func NewConcursService(logger *zap.Logger) *ConcursService {
	rs := &ConcursService{
		logger: logger.Named("concurs-service"),
	}

	return rs
}

func (cs *ConcursService) Shutdown() {}

func (cs *ConcursService) GoIntoConcurs(ctx context.Context) {
	_, span := internal.Tracer().Start(ctx, "ConcursService/GoIntoConcurs")
	defer span.End()

	cs.logger.Debug("Going into concurs mode")
	cs.isConcurs.Store(true)
}

func (cs *ConcursService) GoIntoNormal(ctx context.Context) {
	_, span := internal.Tracer().Start(ctx, "ConcursService/GoIntoNormal")
	defer span.End()

	cs.logger.Debug("Going into normal mode")
	cs.isConcurs.Store(false)
}

func (cs *ConcursService) IsConcurs(ctx context.Context) bool {
	_, span := internal.Tracer().Start(ctx, "ConcursService/IsConcurs")
	defer span.End()

	isConcurs := cs.isConcurs.Load()

	span.SetAttributes(attribute.Bool("isConcurs", isConcurs))

	return isConcurs
}
