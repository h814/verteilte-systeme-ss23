package service

import (
	"context"
	"fmt"
	"sync"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"github.com/jaevor/go-nanoid"
	"go.uber.org/zap"
)

var _ Service = &CreditService{}

type CreditState = string

const (
	CREDIT_PAYED   CreditState = "PAYED"
	CREDIT_PENDING CreditState = "PENDING"
)

type Credit struct {
	State      CreditState
	ID         string
	CustomerID string
	Amount     float64
}

type CreditService struct {
	logger    *zap.Logger
	idFactory func() string

	// Credits still open
	pendingCreditsLock sync.RWMutex
	pendingCredits     map[string]Credit

	// Credit already payed back
	payedCreditsLock sync.RWMutex
	payedCredits     map[string]Credit
}

func NewCreditService(logger *zap.Logger) *CreditService {
	cs := &CreditService{
		logger:         logger.Named("credit-service"),
		pendingCredits: make(map[string]Credit, 0),
		payedCredits:   make(map[string]Credit, 0),
	}

	idFactory, _ := nanoid.Standard(21)
	cs.idFactory = idFactory

	return cs
}

func (cs *CreditService) PayCredit(ctx context.Context, id string) error {
	ctx, span := internal.Tracer().Start(ctx, "CreditService/PayCredits")
	defer span.End()

	cs.pendingCreditsLock.Lock()
	defer cs.pendingCreditsLock.Unlock()

	credit, ok := cs.pendingCredits[id]
	if !ok {
		return fmt.Errorf("No credit found")
	}

	delete(cs.pendingCredits, id)

	credit.State = CREDIT_PAYED

	cs.payedCreditsLock.Lock()
	cs.payedCredits[id] = credit
	cs.payedCreditsLock.Unlock()

	return nil
}

func (cs *CreditService) GetNewCredit(ctx context.Context, customerID string, amount float64) string {
	ctx, span := internal.Tracer().Start(ctx, "CreditService/GetNewCredit")
	defer span.End()

	id := cs.idFactory()
	cs.pendingCreditsLock.Lock()
	defer cs.pendingCreditsLock.Unlock()

	cs.pendingCredits[id] = Credit{
		CustomerID: customerID,
		Amount:     amount,
		State:      CREDIT_PENDING,
		ID:         id,
	}

	return id
}

func (cs *CreditService) GetFullCreditValue(ctx context.Context) float64 {
	ctx, span := internal.Tracer().Start(ctx, "CreditService/GetFullCreditValue")
	defer span.End()

	val := 0.0
	credits := cs.GetAllOpenCredits(ctx)
	for _, credit := range credits {
		val += credit.Amount
	}

	return val
}

func (cs *CreditService) GetAllOpenCredits(ctx context.Context) []Credit {
	ctx, span := internal.Tracer().Start(ctx, "CreditService/GetAllOpenCredits")
	defer span.End()

	cs.pendingCreditsLock.RLock()
	pendingCredits := make([]Credit, 0, len(cs.pendingCredits))

	for _, credit := range cs.pendingCredits {
		pendingCredits = append(pendingCredits, credit)
	}

	cs.pendingCreditsLock.RUnlock()

	return pendingCredits
}

func (cs *CreditService) GetAllPayedCredits(ctx context.Context) []Credit {
	ctx, span := internal.Tracer().Start(ctx, "CreditService/GetAllPayedCredits")
	defer span.End()

	cs.payedCreditsLock.RLock()
	payedCredits := make([]Credit, 0, len(cs.payedCredits))

	for _, credit := range cs.payedCredits {
		payedCredits = append(payedCredits, credit)
	}

	cs.payedCreditsLock.RUnlock()

	return payedCredits
}

func (cs *CreditService) GetAllCredits(ctx context.Context) []Credit {
	ctx, span := internal.Tracer().Start(ctx, "CreditService/GetAllCredits")
	defer span.End()

	cs.pendingCreditsLock.RLock()
	cs.payedCreditsLock.RLock()
	mergedCredits := make([]Credit, 0, len(cs.payedCredits)+len(cs.pendingCredits))

	for _, credit := range cs.pendingCredits {
		mergedCredits = append(mergedCredits, credit)
	}

	for _, credit := range cs.payedCredits {
		mergedCredits = append(mergedCredits, credit)
	}

	cs.payedCreditsLock.RUnlock()
	cs.pendingCreditsLock.RUnlock()

	return mergedCredits
}

func (cs *CreditService) GetCreditById(ctx context.Context, id string) Credit {
	ctx, span := internal.Tracer().Start(ctx, "CreditService/GetCreditById")
	defer span.End()

	cs.pendingCreditsLock.RLock()
	cs.payedCreditsLock.RLock()
	defer cs.payedCreditsLock.RUnlock()
	defer cs.pendingCreditsLock.RUnlock()

	for cId, credit := range cs.pendingCredits {
		if cId == id {
			return credit
		}
	}

	for cId, credit := range cs.payedCredits {
		if cId == id {
			return credit
		}
	}

	return Credit{}
}

func (cs *CreditService) Shutdown() {
}
