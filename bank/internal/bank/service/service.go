package service

type Service interface {
	Shutdown()
}
