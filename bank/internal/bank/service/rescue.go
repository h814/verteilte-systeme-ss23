package service

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"math"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	kafka_go "github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/protocol"
	raftlib "github.com/shaj13/raft"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/grpc_client"
	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/consul"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/kafka"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/raft"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"github.com/hashicorp/consul/api"
	"github.com/samber/lo"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

var _ Service = &RescueService{}

var ErrRescueNotEnoughResources = fmt.Errorf("this bank has not enough resources to rescue the requesting bank")

type RescueService struct {
	ownName                       string
	logger                        *zap.Logger
	cancelWatcher                 context.CancelCauseFunc
	balanceService                *BalanceService
	transferService               *TransferService
	concursService                *ConcursService
	creditService                 *CreditService
	allowExplicitRescueOperations bool
	kafkaClient                   *kafka.Kafka
	raft                          *raft.Raft

	definitivRescue atomic.Bool
	definitivFail   atomic.Bool

	rescueInProcess atomic.Bool

	consulClient *consul.Consul
}

func NewRescueService(logger *zap.Logger, name string, balanceService *BalanceService, concursService *ConcursService, transferService *TransferService, creditService *CreditService, consulClient *consul.Consul, kafkaClient *kafka.Kafka, raft *raft.Raft, allowExplicitRescueOperations bool) *RescueService {
	rs := &RescueService{
		logger:                        logger.Named("rescue-service"),
		ownName:                       name,
		balanceService:                balanceService,
		concursService:                concursService,
		transferService:               transferService,
		consulClient:                  consulClient,
		creditService:                 creditService,
		allowExplicitRescueOperations: allowExplicitRescueOperations,
		kafkaClient:                   kafkaClient,
		raft:                          raft,
	}

	ctx, cancel := context.WithCancelCause(context.Background())
	rs.cancelWatcher = cancel

	go rs.rescueWatcher(ctx)
	go rs.rescueCoordinatorWatcher(ctx)
	go rs.rescueRequestWatcher(ctx)

	return rs
}

func (rs *RescueService) SetDefinitivRescue(val bool) {
	if !rs.allowExplicitRescueOperations {
		rs.logger.Info("Explicit rescue operations are not allowed. Use the `-allowExplicitRescue` CLI flag or the equivalent config tag")
		return
	}

	rs.definitivRescue.Store(val)
}

func (rs *RescueService) GetDefinitivRescue() bool {
	return rs.definitivRescue.Load()
}

func (rs *RescueService) SetDefinitivFail(val bool) {
	if !rs.allowExplicitRescueOperations {
		rs.logger.Info("Explicit rescue operations are not allowed. Use the `-allowExplicitRescue` CLI flag or the equivalent config tag")
		return
	}

	rs.definitivFail.Store(val)
}

func (rs *RescueService) GetDefinitivFail() bool {
	return rs.definitivFail.Load()
}

func (rs *RescueService) rescueWatcher(ctx context.Context) {
	const rescueOffset = 500
	const activelySkipRescueIfAlreadyInConcurs = true

	rs.rescueInProcess.Store(false)

	rs.logger.Info("Start rescue watcher", zap.Int("rescueOffset", rescueOffset))

	conn, err := rs.kafkaClient.ConnectToLeaderFromAny()
	if err != nil {
		rs.logger.Error("Failed to connect to kafka leader", zap.Error(err))
		return
	}

	err = conn.CreateTopics(
		kafka_go.TopicConfig{
			Topic:             types.KAFKA_BALANCE_PUBLISHER_TOPIC,
			NumPartitions:     1,
			ReplicationFactor: 1,
		},
	)

	if err != nil {
		rs.logger.Error("Failed to create topics", zap.Error(err))
		return
	}

	data := make(chan types.BalancePublisherBalance, 1)

	go func() {
		reader := rs.kafkaClient.NewReader(types.KAFKA_BALANCE_PUBLISHER_TOPIC, "rescue-watcher_"+rs.ownName)
		defer reader.Close()

		for {
			select {
			case <-ctx.Done():
				close(data)
				return
			default:
				msg, err := reader.ReadMessage(ctx)

				if err == context.DeadlineExceeded || err == context.Canceled {
					continue
				}

				if err == io.EOF {
					rs.logger.Error("Kafka reader was closed", zap.Error(err))
					return
				}

				if err != nil {
					rs.logger.Error("Failed to read message from kafka", zap.Error(err))
					continue
				}

				var op types.BalancePublisherBalance
				err = json.Unmarshal(msg.Value, &op)
				if err != nil {
					rs.logger.Error("Failed to unmarshal message", zap.Error(err))
					continue
				}

				if op.Bank != rs.ownName {
					continue
				}

				if rs.rescueInProcess.Load() {
					rs.logger.Debug("Rescue is already in process, skipping")
					continue
				}

				select {
				case data <- op:
					rs.logger.Debug("Pushed new balance value", zap.Any("op", op))
				default:
					rs.logger.Debug("Failed to push new balance value, because channel is full", zap.Any("op", op))
				}
			}
		}
	}()

	for {
		select {
		case <-ctx.Done():
			return
		case op, ok := <-data:
			if !ok {
				rs.logger.Error("Failed to read from data channel")
				return
			}

			if op.Bank == "" {
				continue
			}

			ctx, span := internal.Tracer().Start(ctx, "RescueService/check for rescue")

			balance := op.Balance
			rs.logger.Debug("Checking for rescue", zap.Float64("currentCombinedValue", balance))

			if balance > rescueOffset {
				if rs.concursService.IsConcurs(ctx) {
					rs.logger.Info("Bank has gained capital. Leaving concurs")
					rs.concursService.GoIntoNormal(ctx)
					span.End()
					continue
				}

				rs.logger.Info("Bank doesn't need any kind of rescue")
				span.End()
				continue
			}

			if rs.concursService.IsConcurs(ctx) && activelySkipRescueIfAlreadyInConcurs {
				rs.logger.Info("Skipping rescue process", zap.Bool("isConcurs", true), zap.Bool("activelySkipRescueIfAlreadyInConcurs", true))
				span.End()
				continue
			}

			rs.logger.Info("Starting bank rescue process")
			rs.rescueInProcess.Store(true)

			for len(data) > 0 {
				<-data
			}

			ids, r_id, err := rs.rescueBank(ctx)
			if err != nil {
				rs.logger.Error("Failed to rescue bank. Going to concurs mode", zap.Error(err))
				rs.concursService.GoIntoConcurs(ctx)

				span.End()
				continue
			}

			// Distributed rescue
			if r_id != "" {

				reader := rs.kafkaClient.NewReader(types.KAFKA_RESCUE_TOPIC, fmt.Sprintf("rescue_initiator_%s_%s", rs.ownName, r_id))
				var op types.CoordinatedRescueOperation
				for {
					ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
					msg, err := reader.ReadMessage(ctx)
					cancel()

					if err == context.DeadlineExceeded {
						continue
					}

					if err == io.EOF {
						rs.logger.Error("Kafka reader was closed", zap.Error(err))
						return
					}

					if err != nil {
						rs.logger.Error("Failed to read message from kafka", zap.Error(err))
						continue
					}

					ctx, span2 := kafka.UnpackOTELHeader(context.Background(), msg, internal.Tracer(), "RescueService/rescueWatcher/rescue response")

					val := kafka.GetHeader(msg, types.CoordinatedRescueOperationContentTypeKey)
					if string(val) != types.CoordinatedRescueOperationContentTypeRescueOperation {
						rs.logger.Debug("Detected not expected content type for this operation. But due to messaging it may not be an error.", zap.String("expected", string(types.CoordinatedRescueOperationContentTypeRescueOperation)), zap.String("actual", string(val)))
						span2.End()
						continue
					}

					err = json.Unmarshal(msg.Value, &op)
					if err != nil {
						rs.logger.Error("Failed to unmarshal message", zap.Error(err))
						span2.End()
						continue
					}
					if op.ID == r_id &&
						(op.State == types.CoordinatedRescueOperationStateDone ||
							op.State == types.CoordinatedRescueOperationStateFailed ||
							op.State == types.CoordinatedRescueOperationStateError) {
						span2.End()
						break
					}
				}
				reader.Close()

				if op.State == types.CoordinatedRescueOperationStateDone {
					rs.logger.Info("Rescue operation was successful. Transfers should arrive in the next few seconds")
					time.Sleep(30 * time.Second)
				}

				if op.State == types.CoordinatedRescueOperationStateFailed {
					rs.logger.Error("Rescue operation failed. Failed to rescue bank. Going to concurs mode", zap.String("message", op.Message))
					rs.concursService.GoIntoConcurs(ctx)
				}

				if op.State == types.CoordinatedRescueOperationStateError {
					rs.logger.Error("Rescue operation errored. Failed to rescue bank. Going to concurs mode", zap.String("message", op.Message))
					rs.concursService.GoIntoConcurs(ctx)
				}

				span.End()
				rs.rescueInProcess.Store(false)
				continue
			}

			wg := sync.WaitGroup{}
			wg.Add(len(ids))

			// Wait for all transactions to be done
			for _, id := range ids {
				doneCB := rs.transferService.RegisterTransactionDoneCallback(ctx, id)
				go func(cb TransactionDoneCallback) {
					_, span := internal.Tracer().Start(ctx, "RescueService/callback wait helper routine", trace.WithAttributes(attribute.String("id", id)))
					defer span.End()

					defer wg.Done()
					success := <-cb

					if !success {
						rs.logger.Warn("A rescue transfer was finished unsuccessful. Take care now", zap.String("tid", id))
					} else {
						rs.logger.Debug("Handled cb", zap.String("id", id), zap.Bool("success", success))
					}
				}(doneCB)
			}

			span.AddEvent("Start waiting for callbacks", trace.WithTimestamp(time.Now()))
			wg.Wait()
			span.AddEvent("Done waiting for callbacks", trace.WithTimestamp(time.Now()))

			rs.logger.Info("Bank was rescued with the help of transfers")
			span.End()
		}
	}
}

func (rs *RescueService) rescueCoordinatorWatcher(ctx context.Context) {
	const processTimerInSeconds = 2
	const rescueOffset = 500
	const activelySkipRescueIfAlreadyInConcurs = true

	ticker := time.NewTicker(processTimerInSeconds * time.Second)
	rs.logger.Info("Start rescue coordinator watcher", zap.Int("intervalSeconds", processTimerInSeconds), zap.Int("rescueOffset", rescueOffset))

	var readerController chan struct{} = make(chan struct{})
	var isOpen atomic.Bool

	for {
		select {
		case <-ticker.C:
			_, span := internal.Tracer().Start(ctx, "RescueService/check for rescue coordination leader")
			node := rs.raft.Node()
			self := node.Whoami()
			leader := node.Leader()

			type Member struct {
				ID      uint64 `json:"id"`
				Address string `json:"address"`
				Active  bool   `json:"active"`
			}

			memberAddresses := lo.Map(node.Members(), func(item raftlib.Member, index int) Member {
				return Member{
					Address: item.Address(),
					ID:      item.ID(),
					Active:  item.IsActive(),
				}
			})

			leaderMember, _ := node.GetMemebr(leader)
			selfMember, _ := node.GetMemebr(self)

			if (leaderMember == nil || selfMember == nil) || (!selfMember.IsActive() || !leaderMember.IsActive()) {
				rs.logger.Debug("Leader or self member is not active or even set. Stopping or skipping rescue coordination")
				continue
			}

			rs.logger.Debug("Coordinator values", zap.Uint64("self", self), zap.Uint64("leader", leader), zap.Any("selfMember", selfMember.Address()), zap.Any("leaderMember", leaderMember.Address()), zap.Any("members", memberAddresses))

			if self != leader || (self == raftlib.None || leader == raftlib.None) {
				rs.logger.Info("Not leader. Stopping or skipping rescue coordination")
				span.End()
				close(readerController)
				continue
			}

			if isOpen.Load() {
				rs.logger.Debug("Leader coordinator goroutine already running skipping starting a new one")
				span.End()
				continue
			}

			isOpen.Store(true)
			rs.logger.Info("Leader: Starting rescue coordination goroutine")
			go func() {
				conn, err := rs.kafkaClient.ConnectToLeaderFromAny()
				if err != nil {
					rs.logger.Error("Failed to connect to kafka leader", zap.Error(err))
					return
				}

				err = conn.CreateTopics(
					kafka_go.TopicConfig{
						Topic:             types.KAFKA_RESCUE_COORDINATION_TOPIC,
						NumPartitions:     1,
						ReplicationFactor: 1,
					},
					kafka_go.TopicConfig{
						Topic:             types.KAFKA_RESCUE_TOPIC,
						NumPartitions:     1,
						ReplicationFactor: 1,
					},
				)

				if err != nil {
					rs.logger.Error("Failed to create topics", zap.Error(err))
					return
				}

				reader := rs.kafkaClient.NewReader(types.KAFKA_RESCUE_TOPIC, "rescue-coordinator")
				defer func() {
					rs.logger.Debug("Leader: Cleanup rescue coordination goroutine")

					reader.Close()
					readerController = make(chan struct{})
					isOpen.Store(false)
				}()

				for {
					select {
					case <-readerController:
						rs.logger.Info("Leader: Exiting rescue coordination goroutine")
						return
					default:
						ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
						msg, err := reader.ReadMessage(ctx)
						cancel()

						if err == context.DeadlineExceeded || err == context.Canceled {
							continue
						}

						if err == io.EOF {
							rs.logger.Error("Kafka reader was closed", zap.Error(err))
							return
						}

						if err != nil {
							rs.logger.Error("Failed to read message from kafka", zap.Error(err))
							continue
						}

						ctx, span := kafka.UnpackOTELHeader(context.Background(), msg, internal.Tracer(), "RescueService/rescueCoordinatorWatcher/coordinate")

						val := kafka.GetHeader(msg, types.CoordinatedRescueOperationContentTypeKey)
						if string(val) != types.CoordinatedRescueOperationContentTypeRescueOperation {
							rs.logger.Debug("Detected not expected content type for this operation. But due to messaging it may not be an error.", zap.String("expected", string(types.CoordinatedRescueOperationContentTypeRescueOperation)), zap.String("actual", string(val)))
							span.End()
							continue
						}

						var op types.CoordinatedRescueOperation
						err = json.Unmarshal(msg.Value, &op)
						if err != nil {
							rs.logger.Error("Failed to unmarshal message", zap.Error(err))
							span.RecordError(err)
							span.SetStatus(codes.Error, "Failed to unmarshal message")
							span.End()
							continue
						}

						op, err = rs.startCoordinateNewRescueOperation(ctx, op)
						if err != nil {
							span.RecordError(err)
							span.SetStatus(codes.Error, "Failed to start rescue operation coordination")
							rs.logger.Error("Failed to start rescue operation coordination", zap.Error(err))

							op.State = types.CoordinatedRescueOperationStateError
							op.Message = err.Error()

							header, err := kafka.PackOTELHeader(ctx)
							if err != nil {
								rs.logger.Error("Failed to pack OTEL header", zap.Error(err))
								span.RecordError(err)
							}

							writer := rs.kafkaClient.NewWriter(types.KAFKA_RESCUE_TOPIC)
							// Optimistic try setting state to error
							err = writer.WriteMessages(
								ctx,
								kafka_go.Message{
									Key:     []byte(op.ID),
									Value:   op.ToJSON(),
									Headers: []kafka_go.Header{header, protocol.Header{Key: types.CoordinatedRescueOperationContentTypeKey, Value: []byte(types.CoordinatedRescueOperationContentTypeRescueOperation)}},
								},
							)
							writer.Close()
							if err != nil {
								rs.logger.Error("Failed to set op message to error", zap.Error(err))
								span.RecordError(err)
							}

							span.End()
							continue
						}
						span.End()
					}
				}
			}()

			span.End()
		case <-ctx.Done():
			return
		}
	}
}

func (rs *RescueService) startCoordinateNewRescueOperation(ctx context.Context, op types.CoordinatedRescueOperation) (types.CoordinatedRescueOperation, error) {
	baseCtx, span := internal.Tracer().Start(ctx, "RescueService/startCoordinateNewRescueOperation")
	defer span.End()
	rs.logger.Debug("Got rescue op message", zap.Any("op", op))
	span.SetAttributes(attribute.String("op.state", string(op.State)), attribute.String("rescue.id", op.ID))
	if op.State != types.CoordinatedRescueOperationStatePending {
		return op, nil
	}

	writer := rs.kafkaClient.NewWriter(types.KAFKA_RESCUE_TOPIC)

	op.State = types.CoordinatedRescueOperationStateCoordinating

	ctx, cancel := context.WithTimeout(baseCtx, 30*time.Second)
	defer cancel()

	header, err := kafka.PackOTELHeader(baseCtx)
	if err != nil {
		rs.logger.Error("Failed to pack OTEL header", zap.Error(err))
		span.RecordError(err)
	}

	err = writer.WriteMessages(
		ctx,
		kafka_go.Message{
			Key:     []byte(op.ID),
			Value:   op.ToJSON(),
			Headers: []kafka_go.Header{header, protocol.Header{Key: types.CoordinatedRescueOperationContentTypeKey, Value: []byte(types.CoordinatedRescueOperationContentTypeRescueOperation)}},
		},
	)
	if err != nil {
		return op, err
	}
	writer.Close()

	writer = rs.kafkaClient.NewWriter(types.KAFKA_RESCUE_COORDINATION_TOPIC)
	defer writer.Close()

	ctx, cancel = context.WithTimeout(baseCtx, 30*time.Second)
	defer cancel()
	err = writer.WriteMessages(
		ctx,
		kafka_go.Message{
			Key: []byte(op.ID),
			Value: types.CoordinatedRescueOrganization{
				ID:                 op.ID,
				From:               op.Req.GetBankName(),
				Amount:             op.Req.GetAmount(),
				MustParticipate:    op.Req.GetDefinitivRescue(),
				MustNotParticipate: op.Req.GetDefinitivFail(),
			}.ToJSON(),
			Headers: []kafka_go.Header{header, protocol.Header{Key: types.CoordinatedRescueOperationContentTypeKey, Value: []byte(types.CoordinatedRescueOperationContentTypeRescueOrganization)}},
		},
	)
	if err != nil {
		return op, err
	}

	go rs.rescueCoordinationProcess(baseCtx, op)

	return op, nil
}

var (
	ErrNotEnoughParticipants          = fmt.Errorf("not enough participants")
	ErrNotEnoughValueFromParticipants = fmt.Errorf("not enough value from all participants")
)

func (rs *RescueService) rescueCoordinationProcess(ctx context.Context, req types.CoordinatedRescueOperation) {
	defer func() {
		rs.logger.Debug("Rescue coordination process ended", zap.String("id", req.ID))
	}()

	baseCtx, span := internal.Tracer().Start(ctx, "RescueService/rescueCoordinationProcess")
	defer span.End()

	reader := rs.kafkaClient.NewReader(types.KAFKA_RESCUE_COORDINATION_TOPIC, "rescue-coordinator_"+req.ID)
	defer reader.Close()

	participants := make([]types.CoordinatedRescueParticipation, 0)
	commitParticipants := make([]types.CoordinatedRescueOrganization, 0)

	ticker := time.NewTicker(5 * time.Second)

	header, err := kafka.PackOTELHeader(baseCtx)
	if err != nil {
		rs.logger.Error("Failed to pack OTEL header", zap.Error(err))
	}

	for {
		select {
		case <-ticker.C:
			amountParts, err := partitionNumber(ctx, req.Req.GetAmount(), participants)
			if err != nil {
				writer := rs.kafkaClient.NewWriter(types.KAFKA_RESCUE_TOPIC)
				defer writer.Close()
				ctx, cancel := context.WithTimeout(baseCtx, 5*time.Second)
				req.State = types.CoordinatedRescueOperationStateFailed
				req.Message = err.Error()
				err := writer.WriteMessages(ctx, kafka_go.Message{Key: []byte(req.ID), Value: req.ToJSON(), Headers: []kafka_go.Header{header, protocol.Header{Key: types.CoordinatedRescueOperationContentTypeKey, Value: []byte(types.CoordinatedRescueOperationContentTypeRescueOperation)}}})
				cancel()
				if err != nil {
					rs.logger.Error("Failed to write messages to kafka", zap.Error(err))
					span.RecordError(err)
					span.SetStatus(codes.Error, err.Error())
				}
				return
			}

			for i, p := range participants {
				commitParticipants = append(commitParticipants, types.CoordinatedRescueOrganization{
					ID:          req.ID,
					From:        req.Req.GetBankName(),
					Participant: p.Participant,
					Amount:      amountParts[i],
					Commit:      true,
				})
			}

			// Committing
			go func(ctx context.Context, req types.CoordinatedRescueOperation) {
				ctx, span := internal.Tracer().Start(ctx, "RescueService/rescueCoordinationProcess/committing")
				defer span.End()

				header, err := kafka.PackOTELHeader(ctx)
				if err != nil {
					rs.logger.Error("Failed to pack OTEL header", zap.Error(err))
				}

				writer := rs.kafkaClient.NewWriter(types.KAFKA_RESCUE_TOPIC)
				defer writer.Close()

				ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
				req.State = types.CoordinatedRescueOperationStateCommitting
				err = writer.WriteMessages(ctx, kafka_go.Message{Key: []byte(req.ID), Value: req.ToJSON(), Headers: []kafka_go.Header{header, protocol.Header{Key: types.CoordinatedRescueOperationContentTypeKey, Value: []byte(types.CoordinatedRescueOperationContentTypeRescueOperation)}}})
				cancel()
				if err != nil {
					rs.logger.Error("Failed to write messages to kafka", zap.Error(err))
				}
			}(baseCtx, req)

			rs.logger.Debug("Collected participants which want to commit to another bank", zap.Any("participants", commitParticipants))

			msgs := make([]kafka_go.Message, 0)
			for _, p := range commitParticipants {
				msgs = append(msgs, kafka_go.Message{Key: []byte(p.ID), Value: p.ToJSON(), Headers: []kafka_go.Header{header, protocol.Header{Key: types.CoordinatedRescueOperationContentTypeKey, Value: []byte(types.CoordinatedRescueOperationContentTypeRescueOrganization)}}})
			}

			intWriter := rs.kafkaClient.NewWriter(types.KAFKA_RESCUE_COORDINATION_TOPIC)
			defer intWriter.Close()

			ctx, cancel := context.WithTimeout(baseCtx, 5*time.Second)
			err = intWriter.WriteMessages(ctx, msgs...)
			cancel()
			if err != nil {
				rs.logger.Error("Failed to write messages to kafka", zap.Error(err))
				span.RecordError(err)
				span.SetStatus(codes.Error, "Failed to write messages to kafka")
			}

			// Done
			go func(ctx context.Context, req types.CoordinatedRescueOperation) {
				ctx, span := internal.Tracer().Start(ctx, "RescueService/rescueCoordinationProcess/done")
				defer span.End()

				header, err := kafka.PackOTELHeader(ctx)
				if err != nil {
					rs.logger.Error("Failed to pack OTEL header", zap.Error(err))
				}

				writer := rs.kafkaClient.NewWriter(types.KAFKA_RESCUE_TOPIC)
				defer writer.Close()

				ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
				req.State = types.CoordinatedRescueOperationStateDone
				err = writer.WriteMessages(ctx, kafka_go.Message{Key: []byte(req.ID), Value: req.ToJSON(), Headers: []kafka_go.Header{header, protocol.Header{Key: types.CoordinatedRescueOperationContentTypeKey, Value: []byte(types.CoordinatedRescueOperationContentTypeRescueOperation)}}})
				cancel()
				if err != nil {
					rs.logger.Error("Failed to write messages to kafka", zap.Error(err))
				}
			}(baseCtx, req)

			span.AddEvent("Exiting coordination routine")

			return
		default:
			ctx, cancel := context.WithTimeout(baseCtx, 2*time.Second)
			msg, err := reader.ReadMessage(ctx)
			cancel()

			if err == context.DeadlineExceeded || err == context.Canceled {
				continue
			}

			if err == io.EOF {
				rs.logger.Error("Kafka reader was closed", zap.Error(err))
				return
			}

			if err != nil {
				rs.logger.Error("Failed to read message from kafka", zap.Error(err))
				continue
			}

			ctx, span := kafka.UnpackOTELHeader(baseCtx, msg, internal.Tracer(), "RescueService/rescueCoordinationProcess/process participant")

			val := kafka.GetHeader(msg, types.CoordinatedRescueOperationContentTypeKey)
			if string(val) != types.CoordinatedRescueOperationContentTypeRescueParticipant {
				rs.logger.Debug("Detected not expected content type for this operation. But due to messaging it may not be an error.", zap.String("expected", string(types.CoordinatedRescueOperationContentTypeRescueParticipant)), zap.String("actual", string(val)))
				span.End()
				continue
			}

			var op types.CoordinatedRescueParticipation
			err = json.Unmarshal(msg.Value, &op)
			if err != nil {
				rs.logger.Error("Failed to unmarshal message", zap.Error(err))
				span.End()
				continue
			}

			if op.Participant == "" || op.ID != req.ID {
				span.AddEvent("Got empty participant or for other rescue process")
				span.SetStatus(codes.Unset, "Empty participant or for other rescue process")
				span.End()
				continue
			}

			span.AddEvent("Add participant")
			span.SetStatus(codes.Ok, "Participant added")
			participants = append(participants, op)
			span.End()
		}
	}
}

func partitionNumber(ctx context.Context, requestedValue float64, participants []types.CoordinatedRescueParticipation) ([]float64, error) {
	ctx, span := internal.Tracer().Start(ctx, "RescueService/partitionNumber")
	defer span.End()

	if participants == nil || len(participants) == 0 {
		return nil, ErrNotEnoughParticipants
	}

	participantsValueCombined := 0.0
	for _, p := range participants {
		participantsValueCombined += p.Amount
	}
	if participantsValueCombined < requestedValue {
		return nil, ErrNotEnoughValueFromParticipants
	}

	partitioned := make([]float64, len(participants))

	for i := range partitioned {
		partitioned[i] = participants[i].Amount / participantsValueCombined * requestedValue
	}

	return partitioned, nil
}

func (rs *RescueService) rescueRequestWatcher(ctx context.Context) {
	const RESCUE_AMOUNT_PERCENTAGE = 0.3

	conn, err := rs.kafkaClient.ConnectToLeaderFromAny()
	if err != nil {
		rs.logger.Error("Failed to connect to kafka leader", zap.Error(err))
		return
	}

	err = conn.CreateTopics(
		kafka_go.TopicConfig{
			Topic:             types.KAFKA_RESCUE_COORDINATION_TOPIC,
			NumPartitions:     1,
			ReplicationFactor: 1,
		},
	)

	if err != nil {
		rs.logger.Error("Failed to create topic", zap.Error(err))
		return
	}

	reader := rs.kafkaClient.NewReader(types.KAFKA_RESCUE_COORDINATION_TOPIC, fmt.Sprintf("rescue_participant_%s", rs.ownName))
	defer reader.Close()
	writer := rs.kafkaClient.NewWriter(types.KAFKA_RESCUE_COORDINATION_TOPIC)
	defer writer.Close()

	rs.logger.Info("Started rescue request watcher")

	for {
		select {
		case <-ctx.Done():
			return
		default:
			ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
			msg, err := reader.ReadMessage(ctx)
			cancel()

			if err == context.DeadlineExceeded || err == context.Canceled {
				continue
			}

			if err == io.EOF {
				rs.logger.Error("Kafka reader was closed", zap.Error(err))
				return
			}

			if err != nil {
				rs.logger.Error("Failed to read message from kafka", zap.Error(err))
				continue
			}

			ctx, span := kafka.UnpackOTELHeader(context.Background(), msg, internal.Tracer(), "RescueService/rescueRequestWatcher/fetch message")

			val := kafka.GetHeader(msg, types.CoordinatedRescueOperationContentTypeKey)
			if string(val) != types.CoordinatedRescueOperationContentTypeRescueOrganization {
				rs.logger.Debug("Detected not expected content type for this operation. But due to messaging it may not be an error.", zap.String("expected", string(types.CoordinatedRescueOperationContentTypeRescueOrganization)), zap.String("actual", string(val)))
				span.End()
				continue
			}

			var op types.CoordinatedRescueOrganization
			err = json.Unmarshal(msg.Value, &op)
			if err != nil {
				rs.logger.Error("Failed to unmarshal message", zap.Error(err))
				span.End()
				continue
			}

			if op.From == "" {
				span.End()
				continue
			}

			rs.logger.Debug("DEBUG", zap.Any("op", op), zap.Any("n", rs.ownName))

			if op.From == rs.ownName {
				rs.logger.Debug("Skipping rescue request, because it is from myself", zap.Any("op", op))
				span.SetAttributes(attribute.Bool("isSelf", true))
				span.AddEvent("Skipping self rescue request")
				span.End()
				continue
			} else {
				span.SetAttributes(attribute.Bool("isSelf", false))
			}

			span.SetAttributes(attribute.Bool("isParticipant", op.Participant == rs.ownName))

			if op.Commit && op.Participant == rs.ownName {
				t_id := rs.transferService.CreateTransfer(ctx, TransferCustomer{CustomerID: util.GenerateNanoID(), Bank: rs.ownName}, TransferCustomer{Bank: op.From, CustomerID: util.GenerateNanoID()}, op.Amount, "")
				rs.logger.Debug("Commit phase. Initiating transaction", zap.Any("op", op), zap.String("t_id", t_id))
				span.AddEvent("Commit phase. Initiating transaction", trace.WithAttributes(attribute.String("t_id", t_id)))
				span.End()
				continue
			} else if op.Commit {
				rs.logger.Debug("Commit phase. Skipping, because not the corresponding participant", zap.Any("op", op))
				span.AddEvent("Commit phase. Skipping, because not the corresponding participant")
				span.End()
				continue
			}

			shouldRescue := util.RandBool()

			userDriven := false
			if op.MustParticipate {
				shouldRescue = true
				userDriven = true
			} else if op.MustNotParticipate {
				shouldRescue = false
				userDriven = true
			}

			span.SetAttributes(attribute.Bool("shouldRescue", shouldRescue), attribute.Bool("userDriven", userDriven))

			if !shouldRescue {
				rs.logger.Debug("Bank decided not to participate in the rescue", zap.Any("op", op), zap.Bool("userDriven", userDriven))
				span.End()
				continue
			}

			rs.logger.Debug("Bank decided to participate in the rescue", zap.Any("op", op), zap.Bool("userDriven", userDriven))
			rescueAmount := rs.balanceService.GetBalance(context.Background()) * RESCUE_AMOUNT_PERCENTAGE

			span.SetAttributes(attribute.Float64("rescueAmount", rescueAmount))

			header, err := kafka.PackOTELHeader(ctx)
			if err != nil {
				rs.logger.Error("Failed to pack OTEL header", zap.Error(err))
				span.RecordError(err)
			}

			ctx, cancel = context.WithTimeout(ctx, 5*time.Second)
			err = writer.WriteMessages(context.Background(), kafka_go.Message{
				Key: []byte(op.ID),
				Value: types.CoordinatedRescueParticipation{
					ID:          op.ID,
					Participant: rs.ownName,
					Amount:      rescueAmount,
				}.ToJSON(),
				Headers: []kafka_go.Header{header, protocol.Header{Key: types.CoordinatedRescueOperationContentTypeKey, Value: []byte(types.CoordinatedRescueOperationContentTypeRescueParticipant)}},
			})
			cancel()
			if err != nil {
				rs.logger.Error("Failed to send rescue participation response", zap.Error(err), zap.Any("op", op))
				span.RecordError(err)
				span.SetStatus(codes.Error, "Failed to send rescue participation response")
			}

			span.End()
		}
	}
}

func (rs *RescueService) rescueBank(ctx context.Context) ([]string, string, error) {
	ctx, span := internal.Tracer().Start(ctx, "RescueService/rescueBank")
	defer span.End()

	const RESCUE_REQUEST_OFFSET = 2_500

	rescueAmount := (math.Abs(rs.balanceService.GetBalance(ctx)) + RESCUE_REQUEST_OFFSET + rs.creditService.GetFullCreditValue(ctx))
	span.SetAttributes(attribute.Float64("requestedValue", rescueAmount))

	ids := make([]string, 0, 1)

	banks, err := rs.consulClient.GetBanks()
	if err != nil {
		return nil, "", err
	}

	if len(banks) == 0 {
		return nil, "", fmt.Errorf("no banks available")
	}

	banks = lo.Filter(banks, func(x *api.CatalogService, index int) bool {
		return x.ServiceName != rs.ownName
	})

	banks = lo.Shuffle(banks)

	var rescueResponse *pb.RequestSingleRescueResponse
	for _, bank := range banks {
		address := fmt.Sprintf("%s:%d", bank.ServiceAddress, bank.ServicePort)
		span.AddEvent("Contacting other bank for rescue", trace.WithAttributes(attribute.String("address", address), attribute.String("name", bank.ServiceName)), trace.WithTimestamp(time.Now()))
		rs.logger.Info("Contacting other bank for rescue", zap.String("address", address), zap.String("name", bank.ServiceName))

		client, conn, err := grpc_client.NewGRPCBankClient(address)
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, "Failed to rescue bank")
			return nil, "", err
		}

		rescueResponse, err = client.RequestSingleRescue(ctx, &pb.RequestSingleRescueRequest{
			DefinitivFail:   rs.GetDefinitivFail(),
			DefinitivRescue: rs.GetDefinitivRescue(),
			BankName:        rs.ownName,
			Amount:          rescueAmount,
		})
		conn.Close()
		if err != nil {
			if strings.Contains(err.Error(), ErrRescueNotEnoughResources.Error()) {
				span.AddEvent("Got not enough resources response", trace.WithAttributes(attribute.String("address", address)))
				continue
			}

			span.RecordError(err)
			span.SetStatus(codes.Error, "Failed to rescue bank")
			return nil, "", err
		}

		break

	}

	// Is a distributed rescue
	if rescueResponse.GetRescueId() != "" {
		return nil, rescueResponse.GetRescueId(), nil
	}

	if rescueResponse.GetTransferId() == "" {
		return nil, "", fmt.Errorf("failed to rescue bank: all banks returned: %w", ErrRescueNotEnoughResources)
	}

	ids = append(ids, rescueResponse.GetTransferId())

	return ids, "", nil
}

func (rs *RescueService) Shutdown() {
	rs.cancelWatcher(fmt.Errorf("Service was shutdown"))
}
