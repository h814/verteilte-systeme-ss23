package service

import (
	"context"
	"sync"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/kafka"
	kafka_go "github.com/segmentio/kafka-go"
	"go.uber.org/zap"
)

type BalanceService struct {
	ownName string

	logger      *zap.Logger
	balanceLock sync.RWMutex
	balance     float64

	portfolioService *PortfolioService
	creditService    *CreditService

	kafkaClient *kafka.Kafka

	cancelBalancePublisher context.CancelFunc
}

var _ Service = &BalanceService{}

func NewBalanceService(logger *zap.Logger, name string, creditService *CreditService, portfolioService *PortfolioService, kafkaClient *kafka.Kafka) *BalanceService {
	bs := &BalanceService{
		logger:           logger.Named("balance-service"),
		creditService:    creditService,
		portfolioService: portfolioService,
		kafkaClient:      kafkaClient,
		ownName:          name,
	}

	ctx, cancel := context.WithCancel(context.Background())
	bs.cancelBalancePublisher = cancel

	go bs.publisher(ctx)

	return bs
}

func (bs *BalanceService) GetBalance(ctx context.Context) float64 {
	ctx, span := internal.Tracer().Start(ctx, "BalanceService/GetBalance")
	defer span.End()

	bs.balanceLock.RLock()
	defer bs.balanceLock.RUnlock()
	return bs.balance
}

func (bs *BalanceService) GetCombinedBalance(ctx context.Context) float64 {
	ctx, span := internal.Tracer().Start(ctx, "BalanceService/GetCombinedBalance")
	defer span.End()

	return bs.GetBalance(ctx) - bs.creditService.GetFullCreditValue(ctx) + bs.portfolioService.GetValue(ctx)
}

func (bs *BalanceService) ChangeBalance(ctx context.Context, val float64) float64 {
	ctx, span := internal.Tracer().Start(ctx, "BalanceService/ChangeBalance")
	defer span.End()

	bs.balanceLock.Lock()
	defer bs.balanceLock.Unlock()

	bs.balance += val
	bs.logger.Debug("Balance changed", zap.Float64("balance", bs.balance))
	return bs.balance
}

func (bs *BalanceService) Shutdown() {
	bs.cancelBalancePublisher()
}

func (bs *BalanceService) publisher(ctx context.Context) {
	ticker := time.NewTicker(3 * time.Second)

	writer := bs.kafkaClient.NewWriter(types.KAFKA_BALANCE_PUBLISHER_TOPIC)
	defer writer.Close()

	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			balance := bs.GetCombinedBalance(context.Background())

			err := writer.WriteMessages(
				ctx,
				kafka_go.Message{
					Value: types.BalancePublisherBalance{
						Balance: balance,
						Bank:    bs.ownName,
					}.ToJSON(),
				},
			)
			if err != nil {
				bs.logger.Error("Error publishing balance", zap.Error(err))
			}
		}
	}
}
