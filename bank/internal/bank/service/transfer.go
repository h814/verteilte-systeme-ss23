package service

import (
	"context"
	"fmt"
	"sync"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/grpc_client"
	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/consul"
	"github.com/jaevor/go-nanoid"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

var (
	ErrTransferAlreadyProcessed = fmt.Errorf("This transfer was already processed")
	ErrTransferNotFound         = fmt.Errorf("This transfer was not found")
)

var _ Service = &TransferService{}

type TransactionDoneCallback chan bool

type TransferService struct {
	logger    *zap.Logger
	idFactory func() string

	cancelTransferProcessor context.CancelFunc

	// Transfers not yet processed
	pendingTransfersLock sync.RWMutex
	pendingTransfers     map[string]Transfer

	// Transfers already processed
	processedTransfersLock sync.RWMutex
	processedTransfers     map[string]Transfer

	balanceService *BalanceService

	ownName string

	consulClient *consul.Consul

	callbackLock sync.RWMutex
	callbacks    map[string][]TransactionDoneCallback
}

type Transfer struct {
	ID         string
	From       TransferCustomer `json:"from"`
	To         TransferCustomer `json:"to"`
	Value      float64          `json:"value"`
	IsACK      bool
	IsDeclined bool
	Processed  bool
}

type TransferCustomer struct {
	CustomerID string `json:"customerId"`
	Bank       string `json:"bank"`
}

func NewTransferService(logger *zap.Logger, bankName string, bs *BalanceService, consulClient *consul.Consul) *TransferService {
	ts := &TransferService{
		logger:             logger.Named("transfer-service"),
		balanceService:     bs,
		pendingTransfers:   make(map[string]Transfer, 0),
		processedTransfers: make(map[string]Transfer, 0),
		ownName:            bankName,
		consulClient:       consulClient,
		callbacks:          make(map[string][]TransactionDoneCallback, 0),
	}

	idFactory, _ := nanoid.Standard(21)
	ts.idFactory = idFactory

	ctx, cancel := context.WithCancel(context.Background())
	ts.cancelTransferProcessor = cancel
	go ts.transferProcessor(ctx)

	return ts
}

func (ts *TransferService) CreateTransfer(ctx context.Context, fromCustomer TransferCustomer, toCustomer TransferCustomer, value float64, customID string) string {
	ctx, span := internal.Tracer().Start(ctx, "TransferService/CreateTransfer")
	defer span.End()

	tID := customID

	if tID == "" {
		tID = ts.idFactory()
	}

	transfer := Transfer{
		ID:         tID,
		From:       fromCustomer,
		To:         toCustomer,
		Value:      value,
		IsACK:      false,
		Processed:  false,
		IsDeclined: false,
	}

	ts.pendingTransfersLock.Lock()
	ts.pendingTransfers[tID] = transfer
	ts.pendingTransfersLock.Unlock()

	ts.logger.Info("Enqueued new transfer", zap.String("id", tID), zap.Any("transfer", transfer))

	return tID
}

func (ts *TransferService) GetAllTransfers(ctx context.Context) []Transfer {
	ctx, span := internal.Tracer().Start(ctx, "TransferService/GetAllTransfers")
	defer span.End()

	ts.pendingTransfersLock.RLock()
	span.AddEvent("RLocked pending")
	ts.processedTransfersLock.RLock()
	span.AddEvent("RLocked processed")

	mergedTransfers := make([]Transfer, 0, len(ts.pendingTransfers)+len(ts.processedTransfers))

	for _, transfer := range ts.pendingTransfers {
		mergedTransfers = append(mergedTransfers, transfer)
	}

	for _, transfer := range ts.processedTransfers {
		mergedTransfers = append(mergedTransfers, transfer)
	}

	ts.processedTransfersLock.RUnlock()
	span.AddEvent("RUnlocked processed")
	ts.pendingTransfersLock.RUnlock()
	span.AddEvent("RUnlocked pending")

	return mergedTransfers
}

func (ts *TransferService) GetTransferById(ctx context.Context, id string) Transfer {
	ctx, span := internal.Tracer().Start(ctx, "TransferService/GetTransferById")
	defer span.End()

	ts.pendingTransfersLock.RLock()
	ts.processedTransfersLock.RLock()
	defer ts.processedTransfersLock.RUnlock()
	defer ts.pendingTransfersLock.RUnlock()

	for tId, transfer := range ts.pendingTransfers {
		if tId == id {
			return transfer
		}
	}

	for tId, transfer := range ts.processedTransfers {
		if tId == id {
			return transfer
		}
	}

	return Transfer{}
}

func (ts *TransferService) CancelTransfer(ctx context.Context, id string) error {
	ctx, span := internal.Tracer().Start(ctx, "TransferService/CancelTransfer")
	defer span.End()

	ts.pendingTransfersLock.Lock()
	defer ts.pendingTransfersLock.Unlock()

	_, ok := ts.pendingTransfers[id]

	if !ok {
		ts.processedTransfersLock.RLock()
		_, ok := ts.processedTransfers[id]
		ts.processedTransfersLock.RUnlock()

		if ok {
			return ErrTransferAlreadyProcessed
		}

		return ErrTransferNotFound
	}

	delete(ts.pendingTransfers, id)

	ts.logger.Info("Canceled pending transfer", zap.String("id", id))

	return nil
}

func (ts *TransferService) Shutdown() {
	ts.cancelTransferProcessor()
}

func (ts *TransferService) transferProcessor(ctx context.Context) {
	const processTimerInSeconds = 5
	ticker := time.NewTicker(processTimerInSeconds * time.Second)
	ts.logger.Info("Start transfer processor", zap.Int("intervalSeconds", processTimerInSeconds))

	for {
		select {
		case <-ticker.C:
			ctx, span := internal.Tracer().Start(ctx, "TransferService/process pending transfers")
			ts.logger.Debug("Processing transfers")

			ts.pendingTransfersLock.Lock()
			span.AddEvent("Locked pending")
			ts.processedTransfersLock.Lock()
			span.AddEvent("Locked processed")

			// Process pending transfer
			ids := make([]string, 0)
			for id, transfer := range ts.pendingTransfers {
				isAlsoACK, err := ts.processTransfer(ctx, id, transfer)
				if err != nil {
					ts.logger.Error("Failed to process transfer", zap.Error(err), zap.String("transferID", id), zap.Any("transfer", transfer))
					continue
				}

				ids = append(ids, id)
				transfer.Processed = true
				transfer.IsACK = isAlsoACK
				ts.processedTransfers[id] = transfer
			}

			for _, id := range ids {
				delete(ts.pendingTransfers, id)
			}

			ts.processedTransfersLock.Unlock()
			span.AddEvent("Unlocked processed")
			ts.pendingTransfersLock.Unlock()
			span.AddEvent("Unlocked pending")

			ticker.Reset(processTimerInSeconds * time.Second)

			span.End()
		case <-ctx.Done():
			return
		}
	}
}

func (ts *TransferService) ACKTransfer(ctx context.Context, id string) error {
	ctx, span := internal.Tracer().Start(ctx, "TransferService/ACKTransfer")
	defer span.End()

	span.SetAttributes(attribute.String("id", id))

	ts.processedTransfersLock.Lock()
	defer ts.processedTransfersLock.Unlock()
	transfer, ok := ts.processedTransfers[id]

	if !ok {
		span.RecordError(ErrTransferNotFound)
		span.SetStatus(codes.Error, "Transfer not found")
		return ErrTransferNotFound
	}

	transfer.IsACK = true
	ts.processedTransfers[id] = transfer
	value := ts.balanceService.ChangeBalance(ctx, transfer.Value)
	ts.logger.Debug("Changed balance due to ACKed transfer", zap.String("type", "ack-receiver"), zap.Float64("newValue", value), zap.String("id", id))

	ts.HandleCallbacksForID(ctx, id, true)

	return nil
}

func (ts *TransferService) DeclineTransfer(ctx context.Context, id string, reason string) error {
	ctx, span := internal.Tracer().Start(ctx, "TransferService/DeclineTransfer")
	defer span.End()

	ts.processedTransfersLock.Lock()
	defer ts.processedTransfersLock.Unlock()
	transfer, ok := ts.processedTransfers[id]

	if !ok {
		span.RecordError(ErrTransferNotFound)
		span.SetStatus(codes.Error, "Transfer not found")
		return ErrTransferNotFound
	}

	span.SetAttributes(attribute.String("id", id), attribute.String("reason", reason))
	span.SetStatus(codes.Error, "A transfer was declined")

	transfer.IsDeclined = true
	ts.processedTransfers[id] = transfer
	ts.logger.Debug("Transfer was declined", zap.String("id", id), zap.String("reason", reason))

	ts.HandleCallbacksForID(ctx, id, false)

	return nil
}

func (ts *TransferService) getBankAddress(ctx context.Context, name string) (string, error) {
	ctx, span := internal.Tracer().Start(ctx, "TransferService/getBankAddress")
	defer span.End()
	span.SetAttributes(attribute.String("name", name))

	banks, err := ts.consulClient.GetBanks()
	ts.logger.Debug("Got banks from consul", zap.Any("banks", banks))
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "Failed to get bank address")
		return "", err
	}

	span.SetAttributes(attribute.Int("length", len(banks)))

	for _, bank := range banks {
		if bank.ServiceName == name {
			span.SetAttributes(attribute.String("address", bank.ServiceAddress), attribute.Int("port", bank.ServicePort))
			span.SetStatus(codes.Ok, "Bank address found")
			return fmt.Sprintf("%s:%d", bank.Address, bank.ServicePort), nil
		}
	}

	err = fmt.Errorf("No bank with name (%s) found", name)

	span.RecordError(err)
	span.SetStatus(codes.Error, "Failed to get bank address")
	return "", err
}

func (ts *TransferService) processTransfer(ctx context.Context, id string, transfer Transfer) (bool, error) {
	ctx, span := internal.Tracer().Start(ctx, "processTransfer/internal")
	defer span.End()

	if transfer.From.Bank == transfer.To.Bank {
		// Same bank means no real change in balance, because the money stays
		return true, nil
	}

	var address string
	var err error

	if transfer.From.Bank == ts.ownName {
		// Need to reach destination bank
		address, err = ts.getBankAddress(ctx, transfer.To.Bank)
	} else {
		// Need to ACK origin bank
		address, err = ts.getBankAddress(ctx, transfer.From.Bank)
	}

	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "Failed to process transfer")
		return false, err
	}

	client, conn, err := grpc_client.NewGRPCBankClient(address)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, "Failed to process transfer")
		return false, err
	}

	defer conn.Close()

	if transfer.From.Bank == ts.ownName {
		ts.logger.Debug("Outgoing transfer from this -> to external. Prepare for ACK or Decline")
		// INFO Outgoing transfer from this -> to external

		/*
			1. Get address of other bank
			2. Contact Bank and call RPC with transfer info
			3. Finish request
			4. Need to wait for ACK of external bank
		*/

		span.AddEvent("Send transfer request to remote bank")

		reply, err := client.RequestTransfer(
			ctx,
			&pb.RequestTransferRequest{
				Id:     transfer.ID,
				Amount: transfer.Value,
				From:   &pb.TransferCustomer{CustomerId: transfer.From.CustomerID, Bank: transfer.From.Bank},
				To:     &pb.TransferCustomer{CustomerId: transfer.To.CustomerID, Bank: transfer.To.Bank},
			},
		)
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, "Failed to process transfer")
			return false, err
		}

		ts.logger.Debug("Sent transfer to other bank", zap.String("id", reply.GetId()))
	} else {
		ts.logger.Debug("Incoming transfer from external -> to this. Handle ACK and balance adjustment")
		// INFO Incoming transfer from external -> to this

		/*
			1. Send ACK to other bank
			2. Process transfer (change value)
		*/

		span.AddEvent("Send transfer ACK to requesting bank")

		reply, err := client.ACKTransfer(ctx, &pb.IDRequest{Id: transfer.ID})
		if err != nil {
			span.RecordError(err)
			span.SetStatus(codes.Error, "Failed to process transfer")
			return false, err
		}

		span.AddEvent("Changing balance")
		value := ts.balanceService.ChangeBalance(ctx, transfer.Value)
		ts.logger.Debug("Changed balance due to ACKed transfer", zap.String("type", "ack-sender"), zap.Float64("newValue", value), zap.String("id", reply.GetId()))

		ts.HandleCallbacksForID(ctx, transfer.ID, true)

		return true, nil
	}

	return false, nil
}

func (ts *TransferService) HandleCallbacksForID(ctx context.Context, id string, success bool) {
	ctx, span := internal.Tracer().Start(ctx, "TransferService/HandleCallbacksForID")
	defer span.End()

	ts.callbackLock.Lock()
	data, ok := ts.callbacks[id]
	delete(ts.callbacks, id)
	ts.callbackLock.Unlock()

	if !ok {
		// No callbacks registered
		return
	}

	for _, d := range data {
		d <- success
		close(d)
	}
}

func (ts *TransferService) RegisterTransactionDoneCallback(ctx context.Context, id string) TransactionDoneCallback {
	ctx, span := internal.Tracer().Start(ctx, "TransferService/RegisterTransactionDoneCallback", trace.WithAttributes(attribute.String("id", id)))
	defer span.End()

	retChan := make(TransactionDoneCallback, 0)

	ts.callbackLock.Lock()
	data, ok := ts.callbacks[id]

	if !ok {
		data = make([]TransactionDoneCallback, 0, 1)
	}

	data = append(data, retChan)

	ts.callbacks[id] = data

	ts.callbackLock.Unlock()

	go ts.checkForTransactionDoneCallbackAlreadyDone(ctx, id, retChan)
	return retChan
}

func (ts *TransferService) checkForTransactionDoneCallbackAlreadyDone(ctx context.Context, id string, cb TransactionDoneCallback) {
	_, span := internal.Tracer().Start(ctx, "TransferService/checkForTransactionDoneCallbackAlreadyDone", trace.WithAttributes(attribute.String("id", id)))
	defer span.End()

	ts.processedTransfersLock.RLock()
	defer ts.processedTransfersLock.RUnlock()

	if data, ok := ts.processedTransfers[id]; ok {
		if data.IsACK {
			cb <- true
			span.SetAttributes(attribute.Bool("status", true), attribute.Bool("alreadyProcessed", true))
		} else if data.IsDeclined {
			cb <- false
			span.SetAttributes(attribute.Bool("status", false), attribute.Bool("alreadyProcessed", true))
		}

		if data.IsACK || data.IsDeclined {
			close(cb)
			ts.callbackLock.Lock()
			delete(ts.callbacks, id)
			ts.callbackLock.Unlock()
		}

		return
	}

	span.SetAttributes(attribute.Bool("alreadyProcessed", false))
}
