package service

import (
	"context"
	"fmt"
	"sync"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/types"
	exchange_types "code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"github.com/jaevor/go-nanoid"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

var _ Service = &PortfolioService{}

type PortfolioService struct {
	logger *zap.Logger

	cancelOrderProcessor context.CancelFunc

	// Orders not yet processed
	pendingOrderLock sync.RWMutex
	pendingOrders    map[string]types.Order

	// All processed orders
	historyOrderLock sync.RWMutex
	historyOrders    map[string]types.Order

	// <Code>: <amount>
	ownedSharesLock sync.RWMutex
	ownedShares     map[string]int

	Exchange exchange_types.Exchange

	idFactory func() string
}

func NewPortfolioService(logger *zap.Logger) *PortfolioService {
	ps := &PortfolioService{
		logger: logger.Named("portfolio-service"),
		Exchange: exchange_types.Exchange{
			Shares: util.NewConcurrentMap[string, exchange_types.Share](),
		},
		pendingOrders: make(map[string]types.Order, 0),
		historyOrders: make(map[string]types.Order, 0),
		ownedShares:   make(map[string]int, 0),
	}

	ps.Exchange.Shares.SetOverride(true)

	// TODO remove
	ps.ownedShares["LSFT"] = 5
	ps.ownedShares["MWG"] = 10

	idFactory, err := nanoid.Standard(21)
	if err != nil {
		return nil
	}
	ps.idFactory = idFactory

	ctx, cancel := context.WithCancel(context.Background())
	ps.cancelOrderProcessor = cancel

	go ps.orderProcessor(ctx)

	return ps
}

func (ps *PortfolioService) Shutdown() {
	ps.cancelOrderProcessor()
}

func (ps *PortfolioService) orderProcessor(ctx context.Context) {
	ticker := time.NewTicker(5 * time.Second)
	for {
		select {
		case <-ticker.C:
			ctx, span := internal.Tracer().Start(ctx, "PortfolioService/process pending orders")

			ps.pendingOrderLock.Lock()
			ps.historyOrderLock.Lock()

			for id, order := range ps.pendingOrders {
				newOrder, err := ps.processOrder(ctx, id, order)
				if err != nil {
					ps.logger.Error("Failed to process order", zap.Error(err), zap.String("orderID", id), zap.Any("order", order))
					continue
				}

				delete(ps.pendingOrders, id)
				ps.historyOrders[id] = newOrder
			}

			ps.historyOrderLock.Unlock()
			ps.pendingOrderLock.Unlock()
			span.End()
		case <-ctx.Done():
			return
		}
	}
}

func (ps *PortfolioService) processOrder(ctx context.Context, id string, order types.Order) (types.Order, error) {
	ctx, span := internal.Tracer().Start(ctx, "PortfolioService/processOrder")
	defer span.End()

	span.AddEvent("Processing order", trace.WithAttributes(attribute.String("orderID", id), attribute.String("state", string(order.State))))
	if order.State != types.ORDER_PENDING {
		return types.Order{}, fmt.Errorf("This order is not pending but: %s", order.State)
	}

	ps.ownedSharesLock.Lock()

	if order.Operation == types.ORDER_BUY {
		span.SetAttributes(attribute.String("operation", "BUY"))
		ps.logger.Info("Buying some stocks", zap.Any("order", order))
		ps.ownedShares[order.Code] += int(order.Count)
	} else if order.Operation == types.ORDER_SELL {
		span.SetAttributes(attribute.String("operation", "SELL"))
		ps.logger.Info("Selling some stocks", zap.Any("order", order))
		ps.ownedShares[order.Code] -= int(order.Count)

		if ps.ownedShares[order.Code] < 0 {
			ps.ownedShares[order.Code] = 0
		}

	}

	ps.ownedSharesLock.Unlock()

	order.State = types.ORDER_SUCCESS
	return order, nil
}

func (ps *PortfolioService) EnqueueOrder(ctx context.Context, order types.Order) string {
	ctx, span := internal.Tracer().Start(ctx, "PortfolioService/EnqueueOrder")
	defer span.End()

	order.State = types.ORDER_PENDING
	id := ps.idFactory()
	ps.pendingOrderLock.Lock()
	ps.pendingOrders[id] = order
	ps.pendingOrderLock.Unlock()
	return id
}

type SharePortfolioMix struct {
	Share exchange_types.Share `json:"share"`
	Count int                  `json:"count"`
	Value float64              `json:"value"`
}

func (ps *PortfolioService) GetOrderById(ctx context.Context, id string) (types.Order, error) {
	ctx, span := internal.Tracer().Start(ctx, "PortfolioService/EnqueueOrder")
	defer span.End()

	order, ok := ps.GetAllOrders(ctx)[id]

	if !ok {
		return types.Order{}, fmt.Errorf("Not existing")
	}

	return order, nil
}

func (ps *PortfolioService) GetAllOrders(ctx context.Context) map[string]types.Order {
	ctx, span := internal.Tracer().Start(ctx, "PortfolioService/GetAllOrders")
	defer span.End()

	ps.historyOrderLock.RLock()
	ps.pendingOrderLock.RLock()
	mapCopy := make(map[string]types.Order, len(ps.historyOrders)+len(ps.pendingOrders))

	for k, v := range ps.historyOrders {
		mapCopy[k] = v
	}

	for k, v := range ps.pendingOrders {
		mapCopy[k] = v
	}

	ps.pendingOrderLock.RUnlock()
	ps.historyOrderLock.RUnlock()

	return mapCopy
}

func (ps *PortfolioService) GetPendingOrders(ctx context.Context) map[string]types.Order {
	ctx, span := internal.Tracer().Start(ctx, "PortfolioService/GetPendingOrders")
	defer span.End()

	ps.pendingOrderLock.RLock()
	mapCopy := make(map[string]types.Order, len(ps.pendingOrders))

	for k, v := range ps.pendingOrders {
		mapCopy[k] = v
	}

	ps.pendingOrderLock.RUnlock()

	return mapCopy
}

func (ps *PortfolioService) GetOrderHistory(ctx context.Context) map[string]types.Order {
	ctx, span := internal.Tracer().Start(ctx, "PortfolioService/GetOrderHistory")
	defer span.End()

	ps.historyOrderLock.RLock()
	mapCopy := make(map[string]types.Order, len(ps.historyOrders))

	for k, v := range ps.historyOrders {
		mapCopy[k] = v
	}

	ps.historyOrderLock.RUnlock()

	return mapCopy
}

func (ps *PortfolioService) GetPortfolio(ctx context.Context) []SharePortfolioMix {
	ctx, span := internal.Tracer().Start(ctx, "PortfolioService/GetPortfolio")
	defer span.End()

	mix := make([]SharePortfolioMix, 0)

	ps.ownedSharesLock.RLock()
	defer ps.ownedSharesLock.RUnlock()

	if len(ps.ownedShares) == 0 {
		ps.logger.Info("No shares in portfolio yet")
		return mix
	}

	for k, v := range ps.ownedShares {
		share := ps.Exchange.Shares.Get(k)

		val := SharePortfolioMix{
			Count: v,
			Value: float64(v) * share.Value,
			Share: share,
		}

		if share.Value == 0 {
			val.Value = -1
		}

		mix = append(mix, val)
	}

	return mix
}

func (ps *PortfolioService) GetPortfolioEntryByCode(ctx context.Context, code string) SharePortfolioMix {
	ctx, span := internal.Tracer().Start(ctx, "PortfolioService/GetPortfolioEntryByCode")
	defer span.End()

	ps.ownedSharesLock.RLock()
	defer ps.ownedSharesLock.RUnlock()

	if len(ps.ownedShares) == 0 {
		ps.logger.Info("No shares in portfolio yet")
		return SharePortfolioMix{}
	}

	count, ok := ps.ownedShares[code]

	if !ok {
		return SharePortfolioMix{}
	}

	share := ps.Exchange.Shares.Get(code)

	val := SharePortfolioMix{
		Count: count,
		Value: float64(count) * share.Value,
		Share: share,
	}

	if share.Value == 0 {
		val.Value = -1
	}

	return val
}

func (ps *PortfolioService) GetValue(ctx context.Context) float64 {
	ctx, span := internal.Tracer().Start(ctx, "PortfolioService/GetValue")
	defer span.End()

	val := 0.0

	ps.ownedSharesLock.RLock()
	defer ps.ownedSharesLock.RUnlock()

	for k, v := range ps.ownedShares {
		share := ps.Exchange.Shares.Get(k)
		val += float64(v) * share.Value
	}

	return val
}
