package bank

import (
	"context"
	"fmt"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/kafka"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util/connector"

	"go.opentelemetry.io/otel/codes"
	"go.uber.org/zap"
)

var _ connector.Connector = &KafkaConnector{}

type KafkaConnector struct {
	connector.BaseConnector
	logger      *zap.Logger
	kafkaClient *kafka.Kafka
}

func NewKafkaConnector(logger *zap.Logger, kafkaClient *kafka.Kafka) *KafkaConnector {
	conn := &KafkaConnector{
		logger:        logger.Named("kafka-connector"),
		kafkaClient:   kafkaClient,
		BaseConnector: connector.BaseConnector{},
	}

	conn.BaseConnector.State.Store(uint32(connector.RETRYING))

	return conn
}

func (c *KafkaConnector) Connect(ctx context.Context) error {
	err := JoinKafkaCluster(ctx, c.logger, c.kafkaClient)
	if err != nil {
		return err
	}

	c.BaseConnector.State.Store(uint32(connector.CONNECTED))

	return nil
}

func (c *KafkaConnector) Disconnect(ctx context.Context) error {
	c.BaseConnector.State.Store(uint32(connector.DISCONNECTED))

	return nil
}

func (c *KafkaConnector) Check(ctx context.Context) error {
	err := CheckKafkaCluster(ctx, c.logger, c.kafkaClient)
	if err != nil {
		return err
	}

	c.BaseConnector.State.Store(uint32(connector.RETRYING))

	return nil
}

func (c *KafkaConnector) State(ctx context.Context) connector.ConnectorState {
	return connector.ConnectorState(c.BaseConnector.State.Load())
}

func JoinKafkaCluster(ctx context.Context, logger *zap.Logger, client *kafka.Kafka) error {
	ctx, span := internal.Tracer().Start(ctx, "JoinKafkaCluster")
	defer span.End()

	conn, err := client.ConnectToLeaderFromAny()
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	if conn == nil {
		return fmt.Errorf("conn is nil")
	}

	brokers, err := conn.Brokers()

	logger.Info("Joined kafka cluster", zap.Any("brokers", brokers))
	return nil
}

func CheckKafkaCluster(ctx context.Context, logger *zap.Logger, client *kafka.Kafka) error {
	ctx, span := internal.Tracer().Start(ctx, "CheckKafkaCluster")
	defer span.End()

	conn, err := client.ConnectToLeaderFromAny()
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	if conn == nil {
		return fmt.Errorf("conn is nil")
	}

	brokers, err := conn.Brokers()

	logger.Info("Checked kafka cluster", zap.Any("brokers", brokers))
	return nil
}
