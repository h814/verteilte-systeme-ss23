package bank

import (
	"context"
	"net"
	"strings"
	"sync"
	"testing"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/log"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
)

var discardLogger *zap.Logger = log.DiscardLogger()

func TestExchangeConnectorConnect(t *testing.T) {
	assert := assert.New(t)

	udpPort := 54032

	mockServer, err := net.ListenUDP("udp", &net.UDPAddr{IP: nil, Port: udpPort})
	assert.Nil(err)
	defer mockServer.Close()

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		mockServer.SetReadDeadline(time.Now().Add(4 * time.Second))
		for {
			messageRaw := make([]byte, 512)
			readBytes, remote, err := mockServer.ReadFromUDP(messageRaw)
			assert.Nil(err, "Read error: %v", err)

			if err != nil {
				break
			}

			msg := strings.TrimSpace(string(messageRaw[:readBytes]))
			assert.True(strings.HasPrefix(msg, "JOIN_EX;"), "Not a join message")
			mockServer.WriteToUDP([]byte("SUCCESS"), &net.UDPAddr{IP: remote.IP, Port: remote.Port})
			break
		}
	}()

	connector := NewExchangeConnector(discardLogger, types.ExchangeConfig{RemoteConfig: types.RemoteConfig{Host: "127.0.0.1", Port: uint16(udpPort)}}, net.IPv4(127, 0, 0, 1), uint16(3400))
	err = connector.Connect(context.Background())
	assert.Nil(err, "Connect error: %v", err)
	wg.Wait()
}

func TestExchangeConnectorDisconnect(t *testing.T) {
	assert := assert.New(t)

	udpPort := 54032

	mockServer, err := net.ListenUDP("udp", &net.UDPAddr{IP: nil, Port: udpPort})
	assert.Nil(err)
	defer mockServer.Close()

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		mockServer.SetReadDeadline(time.Now().Add(4 * time.Second))
		for {
			messageRaw := make([]byte, 512)
			readBytes, remote, err := mockServer.ReadFromUDP(messageRaw)
			assert.Nil(err)

			if err != nil {
				break
			}

			msg := strings.TrimSpace(string(messageRaw[:readBytes]))
			assert.True(strings.HasPrefix(msg, "QUIT_EX;"), "Not a quit message")
			mockServer.WriteToUDP([]byte("SUCCESS"), &net.UDPAddr{IP: remote.IP, Port: remote.Port})
			break
		}
	}()

	connector := NewExchangeConnector(discardLogger, types.ExchangeConfig{RemoteConfig: types.RemoteConfig{Host: "127.0.0.1", Port: uint16(udpPort)}}, net.IPv4(127, 0, 0, 1), uint16(3400))
	err = connector.Disconnect(context.Background())
	assert.Nil(err)
	wg.Wait()
}

func TestExchangeConnectorCheck(t *testing.T) {
	assert := assert.New(t)

	udpPort := 54032

	mockServer, err := net.ListenUDP("udp", &net.UDPAddr{IP: nil, Port: udpPort})
	assert.Nil(err)
	defer mockServer.Close()

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		mockServer.SetReadDeadline(time.Now().Add(4 * time.Second))
		for {
			messageRaw := make([]byte, 512)
			readBytes, remote, err := mockServer.ReadFromUDP(messageRaw)
			assert.Nil(err)

			if err != nil {
				break
			}

			msg := strings.TrimSpace(string(messageRaw[:readBytes]))
			assert.True(strings.HasPrefix(msg, "PING_EX;"), "Not a ping message")
			mockServer.WriteToUDP([]byte("PONG"), &net.UDPAddr{IP: remote.IP, Port: remote.Port})
			break
		}
	}()

	connector := NewExchangeConnector(discardLogger, types.ExchangeConfig{RemoteConfig: types.RemoteConfig{Host: "127.0.0.1", Port: uint16(udpPort)}}, net.IPv4(127, 0, 0, 1), uint16(3400))
	err = connector.Check(context.Background())
	assert.Nil(err)
	wg.Wait()
}

func TestHandleExchangeOperation(t *testing.T) {
	assert := assert.New(t)

	udpPort := 54032

	mockServer, err := net.ListenUDP("udp", &net.UDPAddr{IP: nil, Port: udpPort})
	assert.Nil(err)
	defer mockServer.Close()

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		mockServer.SetReadDeadline(time.Now().Add(4 * time.Second))
		for {
			messageRaw := make([]byte, 512)
			readBytes, remote, err := mockServer.ReadFromUDP(messageRaw)
			assert.Nil(err)

			if err != nil {
				break
			}

			msg := strings.TrimSpace(string(messageRaw[:readBytes]))
			assert.True(strings.HasPrefix(msg, "TEST_EX;"), "Not a test message")
			mockServer.WriteToUDP([]byte("SUCCESS"), &net.UDPAddr{IP: remote.IP, Port: remote.Port})
			break
		}
	}()

	conn, err := net.DialUDP("udp", nil, &net.UDPAddr{IP: net.IPv4(127, 0, 0, 1), Port: udpPort})
	assert.Nil(err)

	err = handleExchangeOperation(context.Background(), discardLogger, conn, "TEST_EX;", net.IPv4(127, 0, 0, 1), uint16(udpPort))
	assert.Nil(err)

	conn.Close()
	wg.Wait()
}

func TestConnectToUDPRemote(t *testing.T) {
	assert := assert.New(t)

	udpPort := 54032

	mockServer, err := net.ListenUDP("udp", &net.UDPAddr{IP: nil, Port: udpPort})
	assert.Nil(err)
	defer mockServer.Close()

	wg := sync.WaitGroup{}
	wg.Add(1)

	go func() {
		defer wg.Done()
		mockServer.SetReadDeadline(time.Now().Add(4 * time.Second))
		for {
			messageRaw := make([]byte, 512)
			readBytes, err := mockServer.Read(messageRaw)
			assert.Nil(err)

			if err != nil {
				break
			}

			msg := strings.TrimSpace(string(messageRaw[:readBytes]))
			assert.Equal("Hello", msg, "Unexpected message")
			break
		}
	}()

	conn, err := connectToUDPRemote(context.Background(), types.RemoteConfig{Host: "127.0.0.1", Port: uint16(udpPort)})
	assert.Nil(err)
	assert.NotNil(conn)

	i, err := conn.Write([]byte("Hello"))
	assert.Nil(err)
	assert.Equal(len("Hello"), i)

	conn.Close()
	wg.Wait()
}
