package grpchandler

import (
	"context"
	"errors"

	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
	"github.com/samber/lo"
)

var ErrGRPCMock = errors.New("GRPC Mock Error")

type GRPCHandler_Mock struct {
	WithError bool
	WithId    string
}

func (handler *GRPCHandler_Mock) RequestTransfer(context.Context, *pb.RequestTransferRequest) (*pb.IDReply, error) {
	if handler.WithError {
		return nil, ErrGRPCMock
	}

	return &pb.IDReply{Id: lo.Ternary(handler.WithId == "", "1", handler.WithId)}, nil
}

func (handler *GRPCHandler_Mock) CancelTransfer(context.Context, *pb.IDRequest) (*pb.IDReply, error) {
	if handler.WithError {
		return nil, ErrGRPCMock
	}

	return &pb.IDReply{Id: lo.Ternary(handler.WithId == "", "1", handler.WithId)}, nil
}

func (handler *GRPCHandler_Mock) ACKTransfer(context.Context, *pb.IDRequest) (*pb.IDReply, error) {
	if handler.WithError {
		return nil, ErrGRPCMock
	}

	return &pb.IDReply{Id: lo.Ternary(handler.WithId == "", "1", handler.WithId)}, nil
}

func (handler *GRPCHandler_Mock) DeclineTransfer(context.Context, *pb.TransferDeclineRequest) (*pb.IDReply, error) {
	if handler.WithError {
		return nil, ErrGRPCMock
	}

	return &pb.IDReply{Id: lo.Ternary(handler.WithId == "", "1", handler.WithId)}, nil
}

func (handler *GRPCHandler_Mock) RequestCredit(context.Context, *pb.RequestCreditRequest) (*pb.RequestCreditReply, error) {
	if handler.WithError {
		return nil, ErrGRPCMock
	}

	return &pb.RequestCreditReply{Id: lo.Ternary(handler.WithId == "", "1", handler.WithId), Amount: 1.0}, nil
}

func (handler *GRPCHandler_Mock) PayCredit(context.Context, *pb.IDRequest) (*pb.IDReply, error) {
	if handler.WithError {
		return nil, ErrGRPCMock
	}

	return &pb.IDReply{Id: lo.Ternary(handler.WithId == "", "1", handler.WithId)}, nil
}

func (handler *GRPCHandler_Mock) RequestSingleRescue(context.Context, *pb.RequestSingleRescueRequest) (*pb.RequestSingleRescueResponse, error) {
	if handler.WithError {
		return nil, ErrGRPCMock
	}

	return &pb.RequestSingleRescueResponse{TransferId: lo.Ternary(handler.WithId == "", "1", handler.WithId)}, nil
}
