package grpchandler

import (
	"context"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
)

func (handler *grpcHandler) RequestTransfer(ctx context.Context, req *pb.RequestTransferRequest) (*pb.IDReply, error) {
	transferID := handler.transferService.CreateTransfer(
		ctx,
		service.TransferCustomer{
			CustomerID: req.GetFrom().GetCustomerId(),
			Bank:       req.GetFrom().GetBank(),
		},
		service.TransferCustomer{
			CustomerID: req.GetTo().GetCustomerId(),
			Bank:       req.GetTo().GetBank(),
		},
		req.GetAmount(),
		req.Id,
	)

	return &pb.IDReply{Id: transferID}, nil
}

func (handler *grpcHandler) CancelTransfer(ctx context.Context, req *pb.IDRequest) (*pb.IDReply, error) {
	handler.transferService.CancelTransfer(ctx, req.GetId())
	return &pb.IDReply{Id: req.GetId()}, nil
}

func (handler *grpcHandler) ACKTransfer(ctx context.Context, req *pb.IDRequest) (*pb.IDReply, error) {
	err := handler.transferService.ACKTransfer(ctx, req.GetId())
	if err != nil {
		return nil, err
	}
	return &pb.IDReply{Id: req.GetId()}, nil
}

func (handler *grpcHandler) DeclineTransfer(ctx context.Context, req *pb.TransferDeclineRequest) (*pb.IDReply, error) {
	err := handler.transferService.DeclineTransfer(ctx, req.GetId().GetId(), req.GetReason())
	if err != nil {
		return nil, err
	}
	return &pb.IDReply{Id: req.GetId().GetId()}, nil
}
