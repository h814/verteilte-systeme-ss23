package grpchandler

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/glob"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/types"
	kafkalib "code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/kafka"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	kafka "github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/protocol"
	"go.uber.org/zap"
)

const RESCUE_PADDING_AMOUNT = 3_000

func (handler *grpcHandler) RequestSingleRescue(ctx context.Context, req *pb.RequestSingleRescueRequest) (*pb.RequestSingleRescueResponse, error) {
	if glob.MOM_ENABLED {
		return handler.RequestMultipleRescue(ctx, req)
	}

	ctx, span := internal.Tracer().Start(ctx, "gRPC-Handler/RequestSingleRescue")
	defer span.End()

	shouldRescue := util.RandBool()

	userDriven := false
	if req.GetDefinitivFail() {
		shouldRescue = false
		userDriven = true
	}

	if req.GetDefinitivRescue() {
		shouldRescue = true
		userDriven = true
	}

	handler.logger.Info("Made single rescue decision", zap.Bool("userDriven", userDriven), zap.Bool("shouldRescue", shouldRescue))

	if !shouldRescue {
		return nil, fmt.Errorf("The bank decided to decline the rescue request: userDriven: %t", userDriven)
	}

	if req.GetAmount() >= handler.balanceService.GetCombinedBalance(ctx)-RESCUE_PADDING_AMOUNT {
		if !userDriven {
			return nil, service.ErrRescueNotEnoughResources
		}

		val := handler.balanceService.ChangeBalance(ctx, req.GetAmount())
		handler.logger.Info("Faking balance for simulation. The rescue request was user driven", zap.Float64("newValue", val))
	}

	id := handler.transferService.CreateTransfer(ctx, service.TransferCustomer{CustomerID: util.GenerateNanoID(), Bank: handler.name}, service.TransferCustomer{Bank: req.GetBankName(), CustomerID: util.GenerateNanoID()}, req.GetAmount(), "")

	return &pb.RequestSingleRescueResponse{TransferId: id}, nil
}

func (handler *grpcHandler) RequestMultipleRescue(ctx context.Context, req *pb.RequestSingleRescueRequest) (*pb.RequestSingleRescueResponse, error) {
	ctx, span := internal.Tracer().Start(ctx, "gRPC-Handler/RequestMultipleRescue")
	defer span.End()

	id := util.GenerateNanoID()

	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	op := types.CoordinatedRescueOperation{
		Req:   req,
		ID:    id,
		State: types.CoordinatedRescueOperationStatePending,
	}

	bytes, err := json.Marshal(op)
	if err != nil {
		return nil, err
	}

	header, err := kafkalib.PackOTELHeader(ctx)
	if err != nil {
		handler.logger.Error("Failed to pack OTEL header", zap.Error(err))
	}

	writer := handler.kafka.NewWriter(types.KAFKA_RESCUE_TOPIC)
	defer writer.Close()

	ctx, cancel = context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	err = writer.WriteMessages(
		ctx,
		kafka.Message{
			Key:   []byte(id),
			Value: bytes,
			Headers: []kafka.Header{
				header,
				protocol.Header{Key: types.CoordinatedRescueOperationContentTypeKey, Value: []byte(types.CoordinatedRescueOperationContentTypeRescueOperation)},
			},
		},
	)
	if err != nil {
		return nil, err
	}

	return &pb.RequestSingleRescueResponse{TransferId: "", RescueId: id}, nil
}
