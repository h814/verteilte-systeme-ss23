package grpchandler

import (
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/kafka"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/raft"
	"go.uber.org/zap"
)

var _ pb.BankServiceServer = &grpcHandler{}

type grpcHandler struct {
	name             string
	logger           *zap.Logger
	portfolioService *service.PortfolioService
	balanceService   *service.BalanceService
	transferService  *service.TransferService
	creditService    *service.CreditService
	rescueService    *service.RescueService
	raft             *raft.Raft
	kafka            *kafka.Kafka
	pb.UnimplementedBankServiceServer
}

func NewGrpcHandler(logger *zap.Logger, name string, portfolioService *service.PortfolioService, balanceService *service.BalanceService, creditService *service.CreditService, transferService *service.TransferService, rescueService *service.RescueService, kafka *kafka.Kafka, raft *raft.Raft) *grpcHandler {
	handler := grpcHandler{
		logger:           logger.Named("grpc-handler"),
		name:             name,
		portfolioService: portfolioService,
		balanceService:   balanceService,
		transferService:  transferService,
		creditService:    creditService,
		rescueService:    rescueService,
		raft:             raft,
		kafka:            kafka,
	}

	return &handler
}
