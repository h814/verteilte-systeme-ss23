package grpchandler

import (
	"context"

	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
)

func (handler *grpcHandler) RequestCredit(ctx context.Context, req *pb.RequestCreditRequest) (*pb.RequestCreditReply, error) {
	creditID := handler.creditService.GetNewCredit(ctx, req.GetCustomer(), req.GetAmount())

	return &pb.RequestCreditReply{Id: creditID, Amount: req.GetAmount()}, nil
}

func (handler *grpcHandler) PayCredit(ctx context.Context, req *pb.IDRequest) (*pb.IDReply, error) {
	err := handler.creditService.PayCredit(ctx, req.GetId())
	if err != nil {
		return nil, err
	}

	return &pb.IDReply{Id: req.GetId()}, nil
}
