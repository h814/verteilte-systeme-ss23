package bank

import (
	"context"
	"fmt"
	"net"
	"strings"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util/connector"

	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/codes"
	"go.uber.org/zap"
)

var _ connector.Connector = &ExchangeConnector{}

type ExchangeConnector struct {
	connector.BaseConnector
	logger        *zap.Logger
	localIP       net.IP
	config        types.ExchangeConfig
	udpServerPort uint16
}

func NewExchangeConnector(logger *zap.Logger, config types.ExchangeConfig, localIP net.IP, udpServerPort uint16) *ExchangeConnector {
	conn := &ExchangeConnector{
		logger:        logger.Named("ex-connector"),
		localIP:       localIP,
		config:        config,
		udpServerPort: udpServerPort,
		BaseConnector: connector.BaseConnector{},
	}

	conn.BaseConnector.State.Store(uint32(connector.RETRYING))

	return conn
}

func (c *ExchangeConnector) Connect(ctx context.Context) error {
	err := JoinExchangeServer(ctx, c.logger, c.config, c.localIP, c.udpServerPort)
	if err != nil {
		return err
	}

	c.BaseConnector.State.Store(uint32(connector.CONNECTED))

	return nil
}

func (c *ExchangeConnector) Disconnect(ctx context.Context) error {
	err := QuitExchangeServer(ctx, c.logger, c.config, c.localIP, c.udpServerPort)
	if err != nil {
		return err
	}

	c.BaseConnector.State.Store(uint32(connector.DISCONNECTED))

	return nil
}

func (c *ExchangeConnector) Check(ctx context.Context) error {
	err := CheckExchangeServer(ctx, c.logger, c.config, c.localIP, c.udpServerPort)
	if err != nil {
		return err
	}

	c.BaseConnector.State.Store(uint32(connector.RETRYING))

	return nil
}

func (c *ExchangeConnector) State(ctx context.Context) connector.ConnectorState {
	return connector.ConnectorState(c.BaseConnector.State.Load())
}

func JoinExchangeServer(ctx context.Context, logger *zap.Logger, config types.ExchangeConfig, localIP net.IP, udpServerPort uint16) error {
	ctx, span := internal.Tracer().Start(ctx, "JoinExchangeServer")
	defer span.End()

	conn, err := connectToUDPRemote(ctx, config.RemoteConfig)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	err = handleExchangeOperation(ctx, logger, conn, JOIN_PREFIX, localIP, udpServerPort)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	logger.Info("Joined exchange server", zap.String("host", config.Host), zap.Uint16("port", config.Port))
	return nil
}

func CheckExchangeServer(ctx context.Context, logger *zap.Logger, config types.ExchangeConfig, localIP net.IP, udpServerPort uint16) error {
	ctx, span := internal.Tracer().Start(ctx, "CheckExchangeServer")
	defer span.End()

	conn, err := connectToUDPRemote(ctx, config.RemoteConfig)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	err = handleExchangeOperation(ctx, logger, conn, PING_PREFIX, localIP, udpServerPort)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	logger.Info("Checked exchange server", zap.String("host", config.Host), zap.Uint16("port", config.Port))
	return nil
}

func QuitExchangeServer(ctx context.Context, logger *zap.Logger, config types.ExchangeConfig, localIP net.IP, udpServerPort uint16) error {
	ctx, span := internal.Tracer().Start(ctx, "QuitExchangeServer")
	defer span.End()

	conn, err := connectToUDPRemote(ctx, config.RemoteConfig)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	err = handleExchangeOperation(ctx, logger, conn, QUIT_PREFIX, localIP, udpServerPort)
	if err != nil {
		span.RecordError(err)
		span.SetStatus(codes.Error, err.Error())
		return err
	}

	logger.Info("Quit exchange server", zap.String("host", config.Host), zap.Uint16("port", config.Port))
	return nil
}

func handleExchangeOperation(ctx context.Context, logger *zap.Logger, conn *net.UDPConn, prefix string, localIP net.IP, udpServerPort uint16) error {
	ctx, span := internal.Tracer().Start(ctx, "handleExchangeOperation")
	defer span.End()

	defer conn.Close()

	propagator := otel.GetTextMapPropagator()
	udpCarrier := telemetry.NewUDPCarrier()
	propagator.Inject(ctx, udpCarrier)

	data, err := udpCarrier.Pack()
	if err != nil {
		logger.Error("Failed to pack udp carrier", zap.Error(err))
	}

	_, err = conn.Write([]byte(fmt.Sprintf("%s%s%s;%s%d;%s", prefix, HOST_PREFIX, localIP.String(), PORT_PREFIX, udpServerPort, string(data))))
	if err != nil {
		return err
	}

	conn.SetReadDeadline(time.Now().Add(15 * time.Second))

	for {
		message := make([]byte, 32)
		n, err := conn.Read(message)
		if err != nil {
			return err
		}

		if (prefix != PING_PREFIX && strings.TrimSpace(string(message[:n])) == "SUCCESS") || (prefix == PING_PREFIX && strings.TrimSpace(string(message[:n])) == "PONG") {
			logger.Debug("Operation successful", zap.String("prefix", prefix))
			break
		}

	}

	return nil
}

func connectToUDPRemote(ctx context.Context, config types.RemoteConfig) (*net.UDPConn, error) {
	ctx, span := internal.Tracer().Start(ctx, "connectToUDPRemote")
	defer span.End()

	addr, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%s:%d", config.Host, config.Port))
	if err != nil {
		return nil, err
	}

	conn, err := net.DialUDP("udp", nil, addr)
	if err != nil {
		return nil, err
	}

	return conn, err
}
