package bank

import (
	"context"
	"fmt"
	"net"
	"strings"
	"sync"
	"testing"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	exchange_types "code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util/connector"
	"github.com/stretchr/testify/assert"
	"go.opentelemetry.io/otel"
)

type MockConnector struct {
	connector.BaseConnector
	produceError bool
}

var _ connector.Connector = &MockConnector{}

func (mc *MockConnector) Connect(context.Context) error {
	if mc.produceError {
		return fmt.Errorf("Encountered error")
	}

	mc.BaseConnector.State.Store(uint32(connector.CONNECTED))

	return nil
}

func (mc *MockConnector) Check(context.Context) error {
	if mc.produceError {
		return fmt.Errorf("Encountered error")
	}
	mc.BaseConnector.State.Store(uint32(connector.NOOP))
	return nil
}

func (mc *MockConnector) State(context.Context) connector.ConnectorState {
	return connector.ConnectorState(mc.BaseConnector.State.Load())
}

func (mc *MockConnector) Disconnect(context.Context) error {
	if mc.produceError {
		return fmt.Errorf("Encountered error")
	}

	mc.BaseConnector.State.Store(uint32(connector.QUIT))

	return nil
}

func GetUdpOTELData(t *testing.T) []byte {
	propagator := otel.GetTextMapPropagator()
	udpCarrier := telemetry.NewUDPCarrier()
	udpCarrier.Set("traceparent", "700-87be396ee69480f3ae6ee99724ac96ed-e08b3cdd7a25ce04-01")
	propagator.Inject(context.Background(), udpCarrier)
	udpCarrierData, err := udpCarrier.Pack()
	assert.Nil(t, err, "Failed to pack udp carrier dumm data")

	return udpCarrierData
}

func TestInitConnector(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	bankServer := &BankServer{
		logger: discardLogger,
	}

	t.Run("Should error if initial connection fails", func(t *testing.T) {
		t.Parallel()
		bankServer := bankServer
		bankServer.connector = &MockConnector{produceError: true}
		err := bankServer.initConnector(2, 1.01, 500*time.Millisecond, 2*time.Second)
		defer bankServer.cancelConnector()
		assert.NotNil(err, "Connector init returned no error")
		assert.Contains(err.Error(), "Encountered error")
	})

	t.Run("Should succeed if no error is encountered on initial connection", func(t *testing.T) {
		t.Parallel()
		bankServer := bankServer
		bankServer.connector = &MockConnector{}
		err := bankServer.initConnector(2, 1.01, 500*time.Millisecond, 2*time.Second)
		defer bankServer.cancelConnector()
		assert.Nil(err, "Connector init returned an error: %v", err)
	})
}

func TestHandleShareChange(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	bankServer := &BankServer{
		logger: discardLogger,
	}

	otelData := GetUdpOTELData(t)

	t.Run("Should have name if 4 params are set", func(t *testing.T) {
		t.Parallel()
		udpPort := 54932
		mockServerConn, err := net.ListenUDP("udp", &net.UDPAddr{IP: nil, Port: udpPort})
		assert.Nil(err)
		defer mockServerConn.Close()

		bankServer := &BankServer{
			logger:          bankServer.logger,
			updateShareChan: make(chan ShareUpdate, 0),
		}

		wg := sync.WaitGroup{}
		wg.Add(2)

		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
		defer cancel()

		go func() {
			defer wg.Done()
			for {
				select {
				case <-ctx.Done():
					assert.Fail("Share timeout exceeded")
					return
				case share := <-bankServer.updateShareChan:
					assert.Equal("TST1", share.Share.Code)
					assert.Equal("Test", share.Share.Name)
					assert.Equal(float64(23.23), share.Share.Value)
					return
				default:
				}
			}
		}()

		go func() {
			defer wg.Done()
			mockServerConn.SetReadDeadline(time.Now().Add(4 * time.Second))
			for {
				select {
				case <-ctx.Done():
					assert.Fail("Share timeout exceeded")
					return
				default:
					udpMessageRaw := make([]byte, 512)
					readBytes, err := mockServerConn.Read(udpMessageRaw)
					assert.Nil(err, "Failed to read from udp: %v", err)

					if err != nil {
						return
					}

					if readBytes <= 0 {
						continue
					}

					assert.Equal("ACK", strings.TrimSpace(string(udpMessageRaw[:readBytes])), "Wrong answer received")
					return
				}
			}
		}()

		// time.Sleep(100 * time.Millisecond)

		code := bankServer.handleShareChange(context.Background(), "8;TST1;23.23;Test;"+string(otelData), mockServerConn, &net.UDPAddr{IP: nil, Port: udpPort})
		assert.Equal(0, code, "Wrong return code")
		wg.Wait()
	})

	t.Run("Should have no name if 3 params are set", func(t *testing.T) {
		t.Parallel()
		udpPort := 54531
		mockServerConn, err := net.ListenUDP("udp", &net.UDPAddr{IP: nil, Port: udpPort})
		assert.Nil(err)
		defer mockServerConn.Close()

		bankServer := &BankServer{
			logger:          bankServer.logger,
			updateShareChan: make(chan ShareUpdate, 0),
		}

		wg := sync.WaitGroup{}
		wg.Add(2)

		ctx, cancel := context.WithTimeout(context.Background(), 4*time.Second)
		defer cancel()

		go func() {
			defer wg.Done()
			for {
				select {
				case <-ctx.Done():
					assert.Fail("Share timeout exceeded")
					return
				case share := <-bankServer.updateShareChan:
					assert.Equal("TST1", share.Share.Code)
					assert.Equal("", share.Share.Name)
					assert.Equal(float64(23.23), share.Share.Value)
					return
				default:
				}
			}
		}()

		go func() {
			defer wg.Done()
			mockServerConn.SetReadDeadline(time.Now().Add(4 * time.Second))
			for {
				select {
				case <-ctx.Done():
					assert.Fail("Share timeout exceeded")
					return
				default:
					udpMessageRaw := make([]byte, 512)
					readBytes, err := mockServerConn.Read(udpMessageRaw)
					assert.Nil(err, "Failed to read from udp: %v", err)

					if err != nil {
						return
					}

					if readBytes <= 0 {
						continue
					}

					assert.Equal("ACK", strings.TrimSpace(string(udpMessageRaw[:readBytes])), "Wrong answer received")
					return
				}
			}
		}()

		// time.Sleep(100 * time.Millisecond)

		code := bankServer.handleShareChange(context.Background(), "0;TST1;23.23;"+string(otelData), mockServerConn, &net.UDPAddr{IP: nil, Port: udpPort})
		assert.Equal(0, code, "Wrong return code")
		wg.Wait()
	})

	t.Run("Should return error if number is no float64", func(t *testing.T) {
		t.Parallel()
		bankServer := bankServer
		code := bankServer.handleShareChange(context.Background(), "TST1;TE23;"+string(otelData), nil, nil)
		assert.Equal(1, code, "Wrong return code")
	})
}

func TestMiddleman(t *testing.T) {
	t.Parallel()
	assert := assert.New(t)

	bankServer := &BankServer{
		logger:           discardLogger,
		PortfolioService: service.NewPortfolioService(discardLogger),
		updateShareChan:  make(chan ShareUpdate, 0),
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	go bankServer.middleman(ctx)

	testValues := []ShareUpdate{
		{Ctx: context.Background(), Share: exchange_types.Share{Name: "Test", Code: "TST", Value: 23.0}},
		{Ctx: context.Background(), Share: exchange_types.Share{Name: "Test 1", Code: "TST1", Value: 28.23}},
		{Ctx: context.Background(), Share: exchange_types.Share{Name: "Test 2", Code: "TST2", Value: 21233.0}},
		{Ctx: context.Background(), Share: exchange_types.Share{Name: "Test 3", Code: "TST3", Value: 424.2343}},
	}

	for _, v := range testValues {
		bankServer.updateShareChan <- v
		time.Sleep(1 * time.Second)
		assert.Equal(v.Share.Value, bankServer.PortfolioService.Exchange.Shares.Get(v.Share.Code).Value)
	}
}
