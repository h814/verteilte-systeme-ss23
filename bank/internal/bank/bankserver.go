package bank

import (
	"context"
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"strconv"
	"strings"
	"time"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	grpchandler "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/grpc_handler"
	httphandler "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/http_handler"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/types"
	exchange_types "code.fbi.h-da.de/mue-rue/vs-praktikum/exchange/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/consul"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/grpc"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver/httptracer"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver/router"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/kafka"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/probes"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/raft"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util/connector"
	"go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

const (
	SHARE_VALUE_PREFIX = "SHARE_VAL;"
	JOIN_PREFIX        = "JOIN_EX;"
	PING_PREFIX        = "PING_EX;"
	QUIT_PREFIX        = "QUIT_EX;"
	HOST_PREFIX        = "HOST:"
	PORT_PREFIX        = "PORT:"
	CONNECTOR_TIMEOUT  = 5
	chanSize           = 10
)

type ShareUpdate struct {
	Share exchange_types.Share
	Ctx   context.Context
}

type BankServer struct {
	name                              string
	updateShareChan                   chan ShareUpdate
	logger                            *zap.Logger
	logShares                         bool
	conn                              *net.UDPConn
	cancelMiddleman                   context.CancelFunc
	cancelConnectorEventListener      context.CancelFunc
	cancelConnector                   context.CancelFunc
	cancelKafkaConnector              context.CancelFunc
	cancelKafkaConnectorEventListener context.CancelFunc
	exchangeConfig                    types.ExchangeConfig
	server                            *httpserver.HTTPServer
	router                            *router.Router
	grpc                              *grpc.Grpc
	connector                         connector.Connector
	kafkaConnector                    connector.Connector
	probeServer                       *probes.Probes
	PortfolioService                  *service.PortfolioService
	BalanceService                    *service.BalanceService
	TransferService                   *service.TransferService
	CreditService                     *service.CreditService
	RescueService                     *service.RescueService
	ConcursService                    *service.ConcursService
	consulClient                      *consul.Consul
	kafkaClient                       *kafka.Kafka
	raft                              *raft.Raft
}

func New(name string, logger *zap.Logger, config types.ServerConfig, exCfg types.ExchangeConfig, kafkaCfg kafka.Config, raft *raft.Raft, logShares bool, allowExplicitRescueOperations bool, probeServer *probes.Probes, consulClient *consul.Consul) (*BankServer, context.CancelFunc, error) {
	localIP, err := util.GetOutboundIP()
	if err != nil {
		return nil, nil, fmt.Errorf("failed to get local ip: %w", err)
	}

	server := &BankServer{
		name:            name,
		logger:          logger.Named("bank-server"),
		updateShareChan: make(chan ShareUpdate, chanSize),
		logShares:       logShares,
		probeServer:     probeServer,
		exchangeConfig:  exCfg,
		connector:       NewExchangeConnector(logger, exCfg, localIP, config.UDP.Port),
		kafkaClient:     kafka.New(logger, kafkaCfg),
		consulClient:    consulClient,
		raft:            raft,
	}

	server.kafkaConnector = NewKafkaConnector(logger, server.kafkaClient)

	err = server.initConnector(5, 2, 1*time.Second, 60*time.Second)
	if err != nil {
		return nil, nil, err
	}
	err = server.initKafka(10, 2, 1*time.Second, 120*time.Second)
	if err != nil {
		return nil, nil, err
	}

	server.PortfolioService = service.NewPortfolioService(server.logger)
	server.CreditService = service.NewCreditService(server.logger)
	server.ConcursService = service.NewConcursService(server.logger)
	server.BalanceService = service.NewBalanceService(server.logger, server.name, server.CreditService, server.PortfolioService, server.kafkaClient)
	server.TransferService = service.NewTransferService(server.logger, name, server.BalanceService, server.consulClient)
	server.RescueService = service.NewRescueService(server.logger, server.name, server.BalanceService, server.ConcursService, server.TransferService, server.CreditService, server.consulClient, server.kafkaClient, server.raft, allowExplicitRescueOperations)

	if config.Port == 0 {
		return nil, nil, fmt.Errorf("The provided http server port is not possible or was not set")
	}

	if config.UDP.Port == 0 {
		return nil, nil, fmt.Errorf("The provided udp server port is not possible or was not set")
	}

	ctx, cancel := context.WithCancel(context.Background())
	server.cancelMiddleman = cancel

	server.initHttpServer(config)
	server.initUDPServer(config.UDP)
	server.initGRPCServer(config.GRPCServer)

	for i := 0; i < chanSize; i++ {
		go server.middleman(ctx)
	}

	return server, cancel, nil
}

func (bs *BankServer) initGRPCServer(config grpc.GRPCServerConfig) {
	bs.logger.Info("Starting GRPC server")
	bs.grpc = grpc.New(bs.logger, config)
	grpcHandler := grpchandler.NewGrpcHandler(bs.logger, bs.name, bs.PortfolioService, bs.BalanceService, bs.CreditService, bs.TransferService, bs.RescueService, bs.kafkaClient, bs.raft)
	grpc.RegisterGrpc[pb.BankServiceServer](bs.grpc, pb.RegisterBankServiceServer, grpcHandler)
}

func (bs *BankServer) initConnector(retries int, multiplier float64, initialBackoff time.Duration, maxBackoff time.Duration) error {
	establisher := &connector.ConnectionEstablisher{
		Connector:         bs.connector,
		MaxRetries:        retries,
		InitialBackoff:    initialBackoff,
		MaxBackoff:        maxBackoff,
		BackoffMultiplier: multiplier,
		Logger:            bs.logger.Named("establisher"),
	}

	ctx, cancel := context.WithCancel(context.Background())
	bs.cancelConnector = cancel
	establisherChan := establisher.EstablishConnection(ctx, CONNECTOR_TIMEOUT)

	initialConn := <-establisherChan

	if initialConn.Err != nil || initialConn.State != connector.CONNECTED {
		return fmt.Errorf("failed to connect to initial stock exchange (state: %d): %w", initialConn.State, initialConn.Err)
	}

	ctx, cancel = context.WithCancel(context.Background())
	bs.cancelConnectorEventListener = cancel
	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				return
			case conn := <-establisherChan:
				if conn.State == connector.CONNECTED {
					bs.probeServer.SetLiveOrReady(true)
				} else if conn.State != connector.NOOP {
					bs.probeServer.SetLiveOrReady(false)
				}

				if conn.State == connector.CONNECTED || conn.State == connector.RETRYING || conn.State == connector.NOOP {
					continue
				}

				if conn.Err != nil {
					bs.logger.Fatal("Failed to connect to stock exchange", zap.Error(conn.Err), zap.Int("state", int(conn.State)))
				}
			}
		}
	}(ctx)

	return nil
}

func (bs *BankServer) initKafka(retries int, multiplier float64, initialBackoff time.Duration, maxBackoff time.Duration) error {
	establisher := &connector.ConnectionEstablisher{
		Connector:         bs.kafkaConnector,
		MaxRetries:        retries,
		InitialBackoff:    initialBackoff,
		MaxBackoff:        maxBackoff,
		BackoffMultiplier: multiplier,
		Logger:            bs.logger.Named("kafka-establisher"),
	}

	ctx, cancel := context.WithCancel(context.Background())
	bs.cancelKafkaConnector = cancel
	establisherChan := establisher.EstablishConnection(ctx, CONNECTOR_TIMEOUT)

	initialConn := <-establisherChan

	if initialConn.Err != nil || initialConn.State != connector.CONNECTED {
		return fmt.Errorf("failed to connect to initial kafka (state: %d): %w", initialConn.State, initialConn.Err)
	}

	ctx, cancel = context.WithCancel(context.Background())
	bs.cancelKafkaConnectorEventListener = cancel
	go func(ctx context.Context) {
		for {
			select {
			case <-ctx.Done():
				return
			case conn := <-establisherChan:
				if conn.State == connector.CONNECTED {
					bs.logger.Fatal("Connected to kafka cluster", zap.Error(conn.Err), zap.Int("state", int(conn.State)))
				} else if conn.State != connector.NOOP {
					// NOOP
				}

				if conn.State == connector.CONNECTED || conn.State == connector.RETRYING || conn.State == connector.NOOP {
					continue
				}

				if conn.Err != nil {
					bs.logger.Fatal("Failed to connect to kafka cluster", zap.Error(conn.Err), zap.Int("state", int(conn.State)))
				}
			}
		}
	}(ctx)

	return nil
}

func (bs *BankServer) initHttpServer(config types.ServerConfig) {
	router := router.New(bs.logger)

	httphandler.SetupHandlers(bs.name, bs.logger, router, bs.PortfolioService, bs.BalanceService, bs.CreditService, bs.TransferService, bs.RescueService, bs.ConcursService)

	bs.server = &httpserver.HTTPServer{
		Addr:    fmt.Sprintf("%s:%d", config.BindAddr, config.Port),
		Handler: httptracer.NewHandler(bs.logger, router),
		Logger:  bs.logger.Named("http-server"),
	}
}

func (bs *BankServer) initUDPServer(config types.UDPServerConfig) {
	bs.logger.Info("Starting udp server", zap.String("bindAddr", config.BindAddr), zap.Uint16("port", config.Port))
	conn, err := net.ListenUDP("udp", &net.UDPAddr{IP: net.ParseIP(config.BindAddr), Port: int(config.Port)})
	if err != nil {
		bs.logger.Fatal("UDP server failed to listen", zap.Error(err))
	}
	bs.conn = conn

	go func() {
		for {
			udpMessageRaw := make([]byte, 128)
			readBytes, remote, err := conn.ReadFromUDP(udpMessageRaw)
			if err != nil {
				bs.logger.Error("Failed to read UDP message", zap.Error(err))
				continue
			}
			bs.logger.Debug("New UDP message for bank server", zap.Any("remote", remote), zap.ByteString("data", udpMessageRaw[:readBytes]))

			if readBytes == 0 {
				continue
			}

			data := strings.TrimSpace(string(udpMessageRaw[:readBytes]))

			if strings.HasPrefix(data, SHARE_VALUE_PREFIX) {
				go bs.handleShareChange(context.Background(), strings.TrimSpace(strings.TrimPrefix(data, SHARE_VALUE_PREFIX)), conn, remote)
			}

		}
	}()
}

func (bs *BankServer) handleShareChange(ctx context.Context, data string, conn *net.UDPConn, remote *net.UDPAddr) int {
	propagator := otel.GetTextMapPropagator()
	dataS := strings.Split(data, ";")[1:]
	code := dataS[0]
	value, err := strconv.ParseFloat(dataS[1], 64)
	if err != nil {
		bs.logger.Error("Failed to parse value field to float", zap.Error(err), zap.String("value", dataS[1]))
		return 1
	}

	carrierIndex := 2
	shareName := ""
	if len(dataS) == 4 {
		carrierIndex = 3
		shareName = dataS[2]
	}

	udpCarrier := telemetry.NewUDPCarrier()
	err = udpCarrier.Unpack([]byte(dataS[carrierIndex]))
	if err != nil {
		bs.logger.Error("Failed to unpack udp carrier", zap.Error(err))
		return 2
	}
	ctx = propagator.Extract(ctx, udpCarrier)
	ctx, span := internal.Tracer().Start(ctx, "enqueue shareChange", trace.WithSpanKind(trace.SpanKindConsumer))
	defer span.End()

	bs.updateShareChan <- ShareUpdate{
		Share: exchange_types.Share{
			Code:  code,
			Value: value,
			Name:  shareName,
		},
		Ctx: ctx,
	}

	span.AddEvent("Sending ack")
	conn.WriteToUDP([]byte("ACK"), remote)
	span.AddEvent("Sent ack")

	return 0
}

func (bs *BankServer) ListenAndServe() {
	host, port, err := net.SplitHostPort(bs.server.Addr)
	if err != nil {
		bs.logger.Error("Failed to split host and port", zap.Error(err))
	}

	go func() {
		bs.logger.Info("Starting bank http server", zap.String("port", port), zap.String("host", host))
		err := bs.server.ListenAndServe()
		if err != nil {
			bs.logger.Fatal("Failed to start server")
		}
	}()

	go bs.grpc.Run()
}

func (bs *BankServer) Shutdown(ctx context.Context) error {
	err := bs.connector.Disconnect(context.Background())
	if err != nil {
		bs.logger.Error("Failed to disconnect exchange server. Maybe it's already down.")
	}

	bs.kafkaConnector.Disconnect(context.Background())

	bs.cancelMiddleman()
	bs.cancelConnectorEventListener()
	bs.cancelConnector()
	bs.cancelKafkaConnectorEventListener()
	bs.cancelKafkaConnector()
	return bs.server.Shutdown(ctx)
}

func (bs *BankServer) middleman(mmCtx context.Context) {
	client := http.Client{Transport: otelhttp.NewTransport(http.DefaultTransport)}
	for {
		select {
		case shareUpdate := <-bs.updateShareChan:
			share := shareUpdate.Share
			ctx := shareUpdate.Ctx

			if share.Code == "" {
				continue
			}

			ctx, span := internal.Tracer().Start(ctx, "process share change")
			span.SetAttributes(attribute.String("share-code", share.Code))
			span.SetAttributes(attribute.Float64("share-value", share.Value))

			if bs.logShares {
				bs.logger.Debug("Received updated share", zap.String("code", share.Code), zap.Float64("value", share.Value))
			}

			oldShare := bs.PortfolioService.Exchange.Shares.Get(share.Code)

			if share.Name == "" && oldShare.Code == "" {
				bs.logger.Warn("Tried to change an not existing share. Fetching info from api", zap.String("code", share.Code))
				span.AddEvent("Query share info via HTTP API")

				req, _ := http.NewRequestWithContext(ctx, "GET", fmt.Sprintf("http://%s:%d/api/share?code=%s", bs.exchangeConfig.Api.Host, bs.exchangeConfig.Api.Port, share.Code), nil)
				res, err := client.Do(req)
				if err != nil {
					bs.logger.Error("Failed to query missing share", zap.Error(err))
					span.SetStatus(codes.Error, "Failed to query missing share")
					span.RecordError(err)
					span.End()
					continue
				}

				var resp exchange_types.ApiShareResponse

				dec := json.NewDecoder(res.Body)
				err = dec.Decode(&resp)
				if err != nil {
					bs.logger.Error("Failed to parse share query result", zap.Error(err))
					span.SetStatus(codes.Error, "Failed to parse share query result")
					span.RecordError(err)
					span.End()
					continue
				}

				oldShare = resp.Data[0]
			}

			if share.Name == "" {
				share.Name = oldShare.Name
			}

			bs.PortfolioService.Exchange.Shares.Set(share.Code, share)

			bs.printPortfolioValue(ctx)
			span.End()
		case <-mmCtx.Done():
			return
		}
	}
}

type PortfolioValue struct {
	Count int
	Value float64
	Name  string
}

func (bs *BankServer) GetBankValue(ctx context.Context) float64 {
	ctx, span := internal.Tracer().Start(ctx, "GetBankValue")
	defer span.End()

	values := bs.PortfolioService.GetPortfolio(ctx)

	if len(values) == 0 {
		bs.logger.Info("No shares in portfolio yet")
		return -1
	}

	valueCum := 0.0

	for _, v := range values {
		valueCum += v.Value
	}

	return valueCum
}

func (bs *BankServer) printPortfolioValue(ctx context.Context) {
	ctx, span := internal.Tracer().Start(ctx, "printPortfolioValue")
	defer span.End()

	bs.logger.Info("New portfolio value", zap.Float64("value", bs.PortfolioService.GetValue(ctx)), zap.Any("portfolio", bs.PortfolioService.GetPortfolio(ctx)))
}
