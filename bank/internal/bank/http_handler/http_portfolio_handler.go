package httphandler

import (
	"fmt"
	"net/http"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/types"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
)

type GetAllSharesResponse struct {
	Shares []service.SharePortfolioMix `json:"shares"`
}

func (h *httpHandler) GetAllShares(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetAllShares")
	defer span.End()

	portfolio := h.portfolioService.GetPortfolio(ctx)

	response := GetAllSharesResponse{
		Shares: portfolio,
	}

	res.Json(response)
}

type GetShareByCodeResponse struct {
	service.SharePortfolioMix
}

func (h *httpHandler) GetShareByCode(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetShareByCode")
	defer span.End()

	mixedShare := h.portfolioService.GetPortfolioEntryByCode(ctx, req.Params.Get("code"))

	span.SetAttributes(attribute.String("shareCode", req.Params.Get("code")))

	if mixedShare.Share.Code == "" {
		res.WriteHeader(http.StatusNotFound)
		res.Write([]byte("No share with this code found! Code: " + mixedShare.Share.Code + " ;Param: " + req.Params.Get("code")))
		span.SetStatus(codes.Error, "No share with code found")
		return
	}

	response := GetShareByCodeResponse{
		SharePortfolioMix: mixedShare,
	}

	res.Json(response)
}

type ExchangeInfoResponse struct {
	IsConnected bool `json:"isConnected"`
}

func (h *httpHandler) GetExchangeInfo(req *httpserver.Request, res httpserver.Response) {
	_, span := internal.Tracer().Start(req.Context(), "HTTP/GetExchangeInfo")
	defer span.End()

	response := ExchangeInfoResponse{
		IsConnected: true,
	}

	res.Json(response)
}

type OrderIdMix struct {
	types.Order
	Id string `json:"id"`
}

type GetOrderInfoByIdResponse struct {
	Order OrderIdMix `json:"order"`
}

func (h *httpHandler) GetOrderInfoById(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetOrderInfoById")
	defer span.End()

	orderId := req.Params.Get("id")
	span.SetAttributes(attribute.String("orderId", orderId))

	if orderId == "" {
		res.WriteHeader(400)
		res.Write([]byte("No order id specified"))
		span.SetStatus(codes.Error, "No order id specified")
		return
	}

	order, err := h.portfolioService.GetOrderById(ctx, orderId)
	if err != nil {
		res.WriteHeader(http.StatusNotFound)
		res.Write([]byte("This order does not exist"))
		span.RecordError(err)
		span.SetStatus(codes.Error, "Get order info by id failed")
		return
	}

	response := GetOrderInfoByIdResponse{
		Order: OrderIdMix{
			Order: order,
			Id:    orderId,
		},
	}

	res.Json(response)
}

type GetAllOrdersResponse struct {
	Orders []OrderIdMix
}

func (h *httpHandler) GetAllOrders(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetAllOrders")
	defer span.End()

	orders := h.portfolioService.GetAllOrders(ctx)
	response := GetAllOrdersResponse{
		Orders: make([]OrderIdMix, 0),
	}

	for k, v := range orders {
		response.Orders = append(response.Orders, OrderIdMix{
			Order: v,
			Id:    k,
		})
	}

	res.Json(response)
}

type CreateOrderRequest struct {
	types.Order
}

func (h *httpHandler) CreateOrder(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/CreateOrder")
	defer span.End()

	order, ok := httpserver.DecodeJson[CreateOrderRequest](ctx, req, res)
	if !ok {
		span.SetStatus(codes.Error, "Failed to decode json")
		return
	}

	if order.Code == "" || h.portfolioService.Exchange.Shares.Get(order.Code).Code == "" {
		res.WriteHeader(400)
		res.Write([]byte("Error no or wrong code passed"))
		span.SetStatus(codes.Error, "No or wrong code passed")
		return
	}

	if order.Count < 1 {
		res.WriteHeader(400)
		res.Write([]byte("Error order value must be bigger or equal to one"))
		span.RecordError(fmt.Errorf("Wrong order count specified: %d", order.Count))
		span.SetStatus(codes.Error, "Failed to create order")
		return
	}

	if order.Operation != types.ORDER_SELL && order.Operation != types.ORDER_BUY {
		res.WriteHeader(400)
		res.Write([]byte("Wrong order operation specified. Use BUY or SELL instead"))
		span.RecordError(fmt.Errorf("Wrong order operation specified. Got: %s. Use BUY or SELL instead", order.Operation))
		span.SetStatus(codes.Error, "Failed to create order")
		return
	}

	id := h.portfolioService.EnqueueOrder(ctx, order.Order)

	res.WriteHeader(http.StatusCreated)
	res.Write([]byte(id))
}
