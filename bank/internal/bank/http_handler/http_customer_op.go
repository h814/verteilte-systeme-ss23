package httphandler

import (
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
	"go.opentelemetry.io/otel/codes"
)

type CustomerOPRequest struct {
	Value float64 `json:"value"`
}

func (h *httpHandler) CreateCustomerOPDeposit(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/CreateCustomerOPDeposit")
	defer span.End()

	opReq, ok := httpserver.DecodeJson[CustomerOPRequest](ctx, req, res)
	if !ok {
		span.SetStatus(codes.Error, "Failed to decode json")
		return
	}

	h.balanceService.ChangeBalance(ctx, opReq.Value)
}

func (h *httpHandler) CreateCustomerOPWithdraw(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/CreateCustomerOPWithdraw")
	defer span.End()

	opReq, ok := httpserver.DecodeJson[CustomerOPRequest](ctx, req, res)
	if !ok {
		span.SetStatus(codes.Error, "Failed to decode json")
		return
	}

	h.balanceService.ChangeBalance(ctx, -opReq.Value)
}
