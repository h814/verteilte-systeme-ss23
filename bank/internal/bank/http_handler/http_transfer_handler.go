package httphandler

import (
	"net/http"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
)

type GetTransfersResponse struct {
	Transfers []service.Transfer `json:"transfers"`
}

func (h *httpHandler) GetTransfers(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetTransfers")
	defer span.End()

	transfers := h.transferService.GetAllTransfers(ctx)

	response := GetTransfersResponse{
		Transfers: transfers,
	}

	res.Json(response)
}

type GetTransferByIdResponse struct {
	Transfer service.Transfer `json:"transfer"`
}

func (h *httpHandler) GetTransferById(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetTransferById")
	defer span.End()

	transferId := req.Params.Get("id")
	span.SetAttributes(attribute.String("transferID", transferId))

	if transferId == "" {
		res.WriteHeader(400)
		res.Write([]byte("No transfer id specified"))
		span.SetStatus(codes.Error, "No transfer id specified")
		return
	}

	transfer := h.transferService.GetTransferById(ctx, transferId)

	if transfer.ID == "" {
		res.WriteHeader(http.StatusNotFound)
		span.SetStatus(codes.Error, "Not found")
		return
	}

	response := GetTransferByIdResponse{
		Transfer: transfer,
	}

	res.Json(response)
}

type CreateTransferRequest struct {
	service.Transfer
}

func (h *httpHandler) CreateTransfer(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/CreateTransfer")
	defer span.End()

	transferReq, ok := httpserver.DecodeJson[CreateTransferRequest](ctx, req, res)
	if !ok {
		span.SetStatus(codes.Error, "Failed to decode json")
		return
	}

	// Force request which are created at a bank to be from a this very bank
	transferReq.From.Bank = h.name

	id := h.transferService.CreateTransfer(ctx, transferReq.From, transferReq.To, transferReq.Value, "")

	res.WriteHeader(http.StatusCreated)
	res.Write([]byte(id))
}

func (h *httpHandler) DeleteTransfer(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/CreateTransfer")
	defer span.End()

	transferId := req.Params.Get("id")
	span.SetAttributes(attribute.String("transferID", transferId))

	if transferId == "" {
		res.WriteHeader(400)
		res.Write([]byte("No transfer id specified"))
		span.SetStatus(codes.Error, "No transfer id specified")
		return
	}

	err := h.transferService.CancelTransfer(ctx, transferId)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(err.Error()))
		span.RecordError(err)
		span.SetStatus(codes.Error, "Transfer cancel failed")
		return
	}

	res.WriteHeader(http.StatusOK)
	res.Write([]byte(transferId))
}
