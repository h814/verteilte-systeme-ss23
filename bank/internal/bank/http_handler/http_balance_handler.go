package httphandler

import (
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
)

type GetBalanceResponse struct {
	Balance float64 `json:"balance"`
}

func (h httpHandler) GetBalance(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetBalance")
	defer span.End()

	response := GetBalanceResponse{
		Balance: h.balanceService.GetBalance(ctx),
	}

	res.Json(response)
}
