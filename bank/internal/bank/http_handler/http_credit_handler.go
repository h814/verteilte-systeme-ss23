package httphandler

import (
	"net/http"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
)

type GetCreditsResponse struct {
	Credits []service.Credit `json:"credits"`
}

func (h *httpHandler) GetCredits(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetCredits")
	defer span.End()

	credits := h.creditService.GetAllCredits(ctx)

	response := GetCreditsResponse{
		Credits: credits,
	}

	res.Json(response)
}

type GetCreditByIdResponse struct {
	Credit service.Credit `json:"credit"`
}

func (h *httpHandler) GetCreditById(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetCreditById")
	defer span.End()

	creditId := req.Params.Get("id")
	span.SetAttributes(attribute.String("creditId", creditId))

	if creditId == "" {
		res.WriteHeader(400)
		res.Write([]byte("No credit id specified"))
		span.SetStatus(codes.Error, "No credit id specified")
		return
	}

	credit := h.creditService.GetCreditById(ctx, creditId)

	if credit.ID == "" {
		res.WriteHeader(http.StatusNotFound)
		span.SetStatus(codes.Error, "No credit with this id found")
		return
	}

	response := GetCreditByIdResponse{
		Credit: credit,
	}

	res.Json(response)
}

type CreateCreditRequest struct {
	CustomerID string  `json:"customerID"`
	Amount     float64 `json:"amount"`
}

func (h *httpHandler) CreateCredit(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/CreateCredit")
	defer span.End()

	creditRes, ok := httpserver.DecodeJson[CreateCreditRequest](ctx, req, res)
	if !ok {
		span.SetStatus(codes.Error, "Failed to decode json")
		return
	}

	id := h.creditService.GetNewCredit(ctx, creditRes.CustomerID, creditRes.Amount)

	res.WriteHeader(http.StatusCreated)
	res.Write([]byte(id))
}
