package httphandler

import (
	"fmt"
	"net/http"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/codes"
)

type DrainBankRequest struct {
	Sure            bool    `json:"sure"`
	DefinitivFail   *bool   `json:"definitivFail,omitempty"`
	DefinitivRescue *bool   `json:"definitivRescue,omitempty"`
	CustomOffset    float64 `json:"customOffset"`
}

func (h *httpHandler) DrainBank(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/DrainBank")
	defer span.End()

	drainReq, ok := httpserver.DecodeJson[DrainBankRequest](ctx, req, res)
	if !ok {
		span.SetStatus(codes.Error, "Failed to decode json")
		return
	}

	if !drainReq.Sure {
		span.SetStatus(codes.Error, "User was not sure")
		res.WriteHeader(http.StatusBadRequest)
		res.Write([]byte("Please make sure you are sure to drain this bank"))
		return
	}

	if drainReq.DefinitivFail != nil {
		h.rescueService.SetDefinitivFail(*drainReq.DefinitivFail)
	}

	if drainReq.DefinitivRescue != nil {
		h.rescueService.SetDefinitivRescue(*drainReq.DefinitivRescue)
	}

	portfolioValue := h.portfolioService.GetValue(ctx)
	loanedMoneyValue := h.creditService.GetFullCreditValue(ctx)
	currentCashBalanceValue := h.balanceService.GetBalance(ctx)

	// Calculate the value the bank should be drained for test purposes
	// For the bank to go bankrupt it is required to loose the current cash plus the values the bank holds in form of shares
	// In addition a random offset is also removed
	drainValue := portfolioValue - loanedMoneyValue + currentCashBalanceValue + util.RandFloat(1500, 10250) + drainReq.CustomOffset

	span.SetAttributes(attribute.Float64("drainValue", drainValue))

	value := h.balanceService.ChangeBalance(ctx, -drainValue)

	res.WriteHeader(http.StatusOK)
	res.Write([]byte(fmt.Sprintf("The bank was successfully drained with an amount of: %f. The new value is: %f", drainValue, value)))
}
