package httphandler

import (
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver"
)

type BankInfoResponse struct {
	Name            string                      `json:"name"`
	Balance         float64                     `json:"value"`
	Portfolio       []service.SharePortfolioMix `json:"portfolio"`
	FullCreditValue float64                     `json:"fullCreditValue"`
	OpenCredits     []service.Credit            `json:"openCredits"`
	CombinedBalance float64                     `json:"combinedBalance"`
	IsConcurs       bool                        `json:"isConcurs"`
}

func (h *httpHandler) GetBankInfo(req *httpserver.Request, res httpserver.Response) {
	ctx, span := internal.Tracer().Start(req.Context(), "HTTP/GetBankInfo")
	defer span.End()

	response := BankInfoResponse{
		Name:            h.name,
		Balance:         h.balanceService.GetBalance(ctx),
		Portfolio:       h.portfolioService.GetPortfolio(ctx),
		OpenCredits:     h.creditService.GetAllOpenCredits(ctx),
		FullCreditValue: h.creditService.GetFullCreditValue(ctx),
		CombinedBalance: h.balanceService.GetCombinedBalance(ctx),
		IsConcurs:       h.concursService.IsConcurs(ctx),
	}

	res.Json(response)
}
