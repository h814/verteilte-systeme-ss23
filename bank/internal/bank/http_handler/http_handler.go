package httphandler

import (
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/service"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/httpserver/router"
	"go.uber.org/zap"
)

type httpHandler struct {
	name             string
	logger           *zap.Logger
	router           *router.Router
	portfolioService *service.PortfolioService
	balanceService   *service.BalanceService
	transferService  *service.TransferService
	creditService    *service.CreditService
	rescueService    *service.RescueService
	concursService   *service.ConcursService
}

func SetupHandlers(name string, logger *zap.Logger, router *router.Router, portfolioService *service.PortfolioService, balanceService *service.BalanceService, creditService *service.CreditService, transferService *service.TransferService, rescueService *service.RescueService, concursService *service.ConcursService) {
	handler := httpHandler{
		logger:           logger.Named("http-handler"),
		router:           router,
		portfolioService: portfolioService,
		balanceService:   balanceService,
		transferService:  transferService,
		creditService:    creditService,
		rescueService:    rescueService,
		concursService:   concursService,
		name:             name,
	}

	router.GET("/api/share", handler.GetAllShares)
	router.GET("/api/share/{code}", handler.GetShareByCode)

	router.GET("/api/bank/info", handler.GetBankInfo)

	router.GET("/api/exchange/info", handler.GetExchangeInfo)

	router.GET("/api/order/{id}", handler.GetOrderInfoById)
	router.POST("/api/order", handler.CreateOrder)
	router.GET("/api/order", handler.GetAllOrders)

	router.POST("/api/account/{customer_id}/withdraw", handler.CreateCustomerOPWithdraw)
	router.POST("/api/account/{customer_id}/deposit", handler.CreateCustomerOPDeposit)

	router.GET("/api/transfer/{id}", handler.GetTransferById)
	router.POST("/api/transfer", handler.CreateTransfer)
	router.GET("/api/transfer", handler.GetTransfers)
	router.DELETE("/api/transfer/{id}", handler.DeleteTransfer)

	router.GET("/api/credit/{id}", handler.GetCreditById)
	router.POST("/api/credit", handler.CreateCredit)
	router.GET("/api/credit", handler.GetCredits)

	router.GET("/api/balance", handler.GetBalance)

	router.POST("/api/bank/drain", handler.DrainBank)
}
