package grpc_client_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	grpchandler "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/bank/grpc_handler"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/bank/internal/grpc_client"
	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"

	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/grpc"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/log"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/util"
)

type grpcMock struct {
	Server  *grpc.Grpc
	Handler *grpchandler.GRPCHandler_Mock
}

func (g *grpcMock) StartTestServer(port uint16) {
	g.Server = grpc.New(log.DiscardLogger(), grpc.GRPCServerConfig{BindAddr: "localhost", Port: port})
	g.Handler = &grpchandler.GRPCHandler_Mock{}
	grpc.RegisterGrpc[pb.BankServiceServer](g.Server, pb.RegisterBankServiceServer, g.Handler)
	go g.Server.Run()
}

func TestNewGRPCBankClient(t *testing.T) {
	freePort, err := util.GetFreePort()
	require.Nil(t, err, "Could not get free port")
	grpcMock := &grpcMock{}
	grpcMock.StartTestServer(uint16(freePort))

	t.Parallel()

	t.Run("Should get error response", func(t *testing.T) {
		grpcMock.Handler.WithError = true

		client, conn, err := grpc_client.NewGRPCBankClient(fmt.Sprintf("localhost:%d", freePort))
		require.Nil(t, err)
		defer conn.Close()
		_, err = client.RequestTransfer(context.Background(), &pb.RequestTransferRequest{})
		require.NotNil(t, err, "Should get error response")
		assert.ErrorContains(t, err, grpchandler.ErrGRPCMock.Error(), "Wrong error type")
	})

	t.Run("Should get valid response", func(t *testing.T) {
		grpcMock.Handler.WithError = false
		grpcMock.Handler.WithId = "1"

		client, conn, err := grpc_client.NewGRPCBankClient(fmt.Sprintf("localhost:%d", freePort))
		require.Nil(t, err)
		defer conn.Close()
		res, err := client.RequestTransfer(context.Background(), &pb.RequestTransferRequest{})
		require.Nil(t, err)
		assert.Equal(t, "1", res.Id, "Should get valid response")
	})
}
