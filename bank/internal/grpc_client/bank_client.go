package grpc_client

import (
	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// NOTE: Use *grpc.ClientConn.Close() after finishing the client request
func NewGRPCBankClient(address string) (pb.BankServiceClient, *grpc.ClientConn, error) {
	var opts []grpc.DialOption

	opts = append(opts, grpc.WithTransportCredentials(insecure.NewCredentials()))

	unary, streamI := telemetry.GetGRPCClientInterceptors()
	opts = append(opts, grpc.WithUnaryInterceptor(unary), grpc.WithStreamInterceptor(streamI))

	conn, err := grpc.Dial(address, opts...)
	if err != nil {
		return nil, nil, err
	}

	client := pb.NewBankServiceClient(conn)

	return client, conn, nil
}
