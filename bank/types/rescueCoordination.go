package types

import (
	"encoding/json"

	pb "code.fbi.h-da.de/mue-rue/vs-praktikum/bank/proto"
)

const CoordinatedRescueOperationContentTypeKey = "X-Content-Type"

type CoordinatedRescueOperationContentType string

const (
	CoordinatedRescueOperationContentTypeRescueOperation    = "jm-vs/CoordinatedRescueOperation"
	CoordinatedRescueOperationContentTypeRescueOrganization = "jm-vs/CoordinatedRescueOrganization"
	CoordinatedRescueOperationContentTypeRescueParticipant  = "jm-vs/CoordinatedRescueParticipation"
)

type CoordinatedRescueOperationState string

const (
	CoordinatedRescueOperationStatePending      CoordinatedRescueOperationState = "pending"
	CoordinatedRescueOperationStateCoordinating CoordinatedRescueOperationState = "coordinating"
	CoordinatedRescueOperationStateCommitting   CoordinatedRescueOperationState = "committing"
	CoordinatedRescueOperationStateDone         CoordinatedRescueOperationState = "done"
	CoordinatedRescueOperationStateFailed       CoordinatedRescueOperationState = "failed"
	CoordinatedRescueOperationStateError        CoordinatedRescueOperationState = "error"
)

const (
	KAFKA_RESCUE_TOPIC              = "rescue-request"
	KAFKA_RESCUE_COORDINATION_TOPIC = "rescue-coordination"
)

type CoordinatedRescueOperation struct {
	Req     *pb.RequestSingleRescueRequest  `json:"req"`
	ID      string                          `json:"id"`
	State   CoordinatedRescueOperationState `json:"state"`
	Message string                          `json:"message"`
}

func (coord CoordinatedRescueOperation) ToJSON() []byte {
	bytes, _ := json.Marshal(coord)

	return bytes
}

type CoordinatedRescueOrganization struct {
	ID                 string  `json:"id"`
	From               string  `json:"from"`
	Amount             float64 `json:"amount"`
	Participant        string  `json:"participant"`
	Commit             bool    `json:"commit"`
	MustParticipate    bool    `json:"mustParticipate"`
	MustNotParticipate bool    `json:"mustNotParticipate"`
}

func (coord CoordinatedRescueOrganization) ToJSON() []byte {
	bytes, _ := json.Marshal(coord)

	return bytes
}

type CoordinatedRescueParticipation struct {
	ID          string  `json:"id"`
	Participant string  `json:"participant"`
	Amount      float64 `json:"amount"`
}

func (part CoordinatedRescueParticipation) ToJSON() []byte {
	bytes, _ := json.Marshal(part)

	return bytes
}
