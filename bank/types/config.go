package types

import (
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/consul"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/grpc"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/kafka"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/log"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/probes"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/telemetry"
	"code.fbi.h-da.de/mue-rue/vs-praktikum/lib/go/raft"
)

type Config struct {
	Name                          string           `yaml:"name" toml:"name"`
	AllowExplicitRescueOperations bool             `yaml:"allowExplicitRescueOperations" toml:"allowExplicitRescueOperations"`
	LogShares                     bool             `yaml:"logShares" toml:"log_shares"`
	Kafka                         kafka.Config     `yaml:"kafka" toml:"kafka"`
	ExchangeConfig                ExchangeConfig   `yaml:"exchangeServer" toml:"exchange_server"`
	Server                        ServerConfig     `yaml:"server" toml:"server"`
	Logger                        log.Config       `yaml:"logger" toml:"logger"`
	Probes                        probes.Config    `yaml:"probes" toml:"probes"`
	Consul                        consul.Config    `yaml:"consul" toml:"consul"`
	Telemetry                     telemetry.Config `yaml:"telemetry" toml:"telemetry"`
	Raft                          raft.Config      `yaml:"raft" toml:"raft"`
}

type ServerConfig struct {
	BindAddr   string                `yaml:"bindAddr" toml:"bindAddr"`
	Port       uint16                `yaml:"port" toml:"port"`
	UDP        UDPServerConfig       `yaml:"udp" toml:"udp"`
	GRPCServer grpc.GRPCServerConfig `yaml:"grpc" toml:"grpc"`
}

type UDPServerConfig struct {
	BindAddr string `yaml:"bindAddr" toml:"bindAddr"`
	Port     uint16 `yaml:"port" toml:"port"`
}

type ExchangeConfig struct {
	RemoteConfig
	Api RemoteConfig `yaml:"api" toml:"api"`
}

type RemoteConfig struct {
	Host string `yaml:"host" toml:"host"`
	Port uint16 `yaml:"port" toml:"port"`
}
