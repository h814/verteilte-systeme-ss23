package types

type (
	OrderState     string
	OrderOperation string
)

const (
	ORDER_PENDING OrderState = "PENDING"
	ORDER_SUCCESS OrderState = "SUCCESS"
	ORDER_FAIL    OrderState = "FAIL"
)

const (
	ORDER_BUY  OrderOperation = "BUY"
	ORDER_SELL OrderOperation = "SELL"
)

type Order struct {
	State     OrderState     `json:"state"`
	Operation OrderOperation `json:"operation"`
	Code      string         `json:"code"`
	Count     uint32         `json:"count"`
}
