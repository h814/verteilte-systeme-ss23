package types

import "encoding/json"

const KAFKA_BALANCE_PUBLISHER_TOPIC = "balance"

type BalancePublisherBalance struct {
	Balance float64 `json:"balance"`
	Bank    string  `json:"bank"`
}

func (b BalancePublisherBalance) ToJSON() []byte {
	bytes, _ := json.Marshal(b)
	return bytes
}
